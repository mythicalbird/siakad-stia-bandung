<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RefController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/ref/get_count", name="feeder_ref_get_count")
     * @Method({"POST"})
     */
    public function refGetCountAction(Request $request) 
    {
        $response = new JsonResponse();
        if ( !empty($request->get('table')) ) {

          $table = $request->get('table');
          if ( $table == 'propinsi' || $table == 'kota' || $table == 'kecamatan' ) {

            if ( $table == 'propinsi' ) {
              $data = $this->feeder->ws( 'GetCountRecordset', array('table' => 'wilayah', "filter" => "id_negara='ID' AND id_level_wil=1") );
            } elseif ( $table == 'kota' ) {
              $data = $this->feeder->ws( 'GetCountRecordset', array('table' => 'wilayah', "filter" => "id_negara='ID' AND id_level_wil=2") );
            } elseif ( $table == 'kecamatan' ) {
              $data = $this->feeder->ws( 'GetCountRecordset', array('table' => 'wilayah', "filter" => "id_negara='ID' AND id_level_wil=3") );
            }

          } else {
            $data = $this->feeder->ws( 'GetCountRecordset', array('table' => $table) );
          } 

          $result_total = ( isset($data['result']) ) ? (int)$data['result'] : 0;

          $response->setData($result_total);

        }
        return $response;
    }


    /**
     * @Route("/feeder/ref/get_data", name="feeder_ref_get_data")
     * @Method({"POST"})
     */
    public function refGetDataAction(Request $request) 
    {
        $response = new JsonResponse();
        if ( !empty($request->get('table')) ) {

          $table = $request->get('table');

          if ( $table == 'propinsi' || $table == 'kota' || $table == 'kecamatan' ) {

            $params = array(
              'table'   => 'wilayah',
              'limit'   => $request->get('limit'),
              'offset'  => $request->get('offset')
            );
            if ( $table == 'propinsi' ) {
              $params['filter'] = "id_negara='ID' AND id_level_wil=1";
            } elseif ( $table == 'kota' ) {
              $params['filter'] = "id_negara='ID' AND id_level_wil=2";
            } elseif ( $table == 'kecamatan' ) {
              $params['filter'] = "id_negara='ID' AND id_level_wil=3";
            }

          } else {

            $params = array(
              'table'   => $table,
              'limit'   => $request->get('limit'),
              'offset'  => $request->get('offset')
            );
            if ( $table == 'wilayah' ) {
              $params['filter'] = "id_negara='ID' AND id_level_wil=2";
            }

          }
          $data = $this->feeder->ws( 'GetRecordset', $params );
          if ( isset($data['result']) && count($data['result']) > 0 ) {
            $response->setData($data['result']);
          }

        }
        return $response;
    } 

    /**
     * @Route("/feeder/ref/import", name="feeder_ref_import")
     * @Method({"POST"})
     */
    public function refImportAction(Request $request) 
    {
        $response = new JsonResponse();
        $ret = array();
        $now = new\DateTime();
        $em = $this->getDoctrine()->getManager();


        $ref = $request->get('table');
        $result = $request->get('result');

        $raw = json_encode($result);
        $ket = '';

        if ($ref == 'semester') {

          
            // $kode = trim($result['id_smt']);
            // $smtr = $em->getRepository('AppBundle:Semester')
            //     ->findOneByKode($kode);
            // if ( ! $smtr ) {
            //   $smtr = new Semester();
            // }
            // $smtr->setKode($kode);
            // $smtr->setNama($result['nm_smt']);
            // $smtr->setSemester($result['smt']);
            // $smtr->setAktif($result['a_periode_aktif']);
            // $smtr->setTglMulai(new\DateTime($result['tgl_mulai']));
            // $smtr->setTglSelesai(new\DateTime($result['tgl_selesai']));
            // if (isset($result['id_thn_ajaran'])) {
            //     $ta = $em->getRepository('AppBundle:TahunAkademik')
            //         ->findOneByKode($result['id_thn_ajaran']);
            //     if ( $ta ) {
            //       $smtr->setTahunAkademik($ta);
            //     }
            // }
            // $smtr->setRaw($raw);

            // $em->persist($smtr);

        } 
        elseif ( $ref == 'tahun_ajaran' ) {

            $kode = $result['id_thn_ajaran'];
            $tglMulai = new\DateTime($result['tgl_mulai']);
            $tglSelesai = new\DateTime($result['tgl_selesai']);
            $rangeTgl = $tglMulai->format('d-m-Y') . ' s/d ' . $tglSelesai->format('d-m-Y');

            $ta = $em->getRepository('AppBundle:TahunAkademik')
                ->findOneByKode($kode);

            if (!$ta) {
              $ta = new TahunAkademik();
              $ta->setTahun($kode);
              $ta->setAktif($result['a_periode_aktif']);
              $ta->setBatasKrs($rangeTgl);
              $ta->setBatasKrsOnline($rangeTgl);
              $ta->setBatasUbahKrs($rangeTgl);
              $ta->setBatasCetakKss($rangeTgl);
              $ta->setBatasPembayaran($rangeTgl);
              $ta->setPerkuliahan($rangeTgl);
              $ta->setBatasUts($rangeTgl);
              $ta->setBatasUas($rangeTgl);
            }

            $ta->setKode($kode);
            $ta->setNama($result['nm_thn_ajaran']);
            $ta->setRaw($raw);

            $em->persist($ta);

        } 
        elseif ( $ref == 'propinsi' || $ref == 'kota' || $ref == 'kecamatan' ) {

            $kode = trim($result['id_wil']);

            $wil = $em->getRepository('AppBundle:Wilayah')
                ->findOneByKode($kode);
            if ( ! $wil ) {
              $wil = new \AppBundle\Entity\Wilayah();
              $wil->setKode($kode);
              $negara = $em->getRepository('AppBundle:Master')
                  ->findOneBy(array(
                  'type'  => 'negara',
                  'kode'  => trim($result['id_negara']),
              ));
              if ( $negara ) {
                $wil->setNegara($negara);
              }
              $level = $em->getRepository('AppBundle:Master')
                  ->findOneBy(array(
                  'type'  => 'level_wilayah',
                  'kode'  => trim($result['id_level_wil']),
              ));
              if ( $level ) {
                $wil->setLevel($level);
              }
            }

            $wil->setNama( trim($result['nm_wil']) );
            $wil->setAsal( str_replace("NULL", "", trim($result['asal_wil'])) );
            $wil->setKodeBps( str_replace("NULL", "", trim($result['kode_bps'])) );
            $wil->setKodeDagri( str_replace("NULL", "", trim($result['kode_dagri'])) );
            $wil->setKodeKeu( str_replace("NULL", "", trim($result['kode_keu'])) );
            
            $parent = $em->getRepository('AppBundle:Wilayah')
                ->findOneByKode(trim($result['id_induk_wilayah']));
            if ( $parent ) {
              $wil->setParent($parent);
            }

            $em->persist($wil);

        } 
        else {

            if ($ref == 'agama') {
              $kode = $result['id_agama'];
              $nama = $result['nm_agama'];
            } 
            elseif ($ref == 'status_mahasiswa') {
              $kode = $result['id_stat_mhs'];
              $nama = $result['nm_stat_mhs'];
              $ket = $result['ket_stat_mhs'];
            } 
            elseif ($ref == 'bentuk_pendidikan') {
              $kode = $result['id_bp'];
              $nama = $result['nm_bp'];
              $custom = array($result['a_jenj_paud'], $result['a_jenj_tk'], $result['a_jenj_sd'], $result['a_jenj_smp'], $result['a_jenj_sma'], $result['a_jenj_tinggi'], $result['dir_bina'], $result['a_aktif']);
            } 
            elseif ($ref == 'ikatan_kerja_sdm') {
              $kode = $result['id_ikatan_kerja'];
              $nama = $result['nm_ikatan_kerja'];
              $ket = $result['ket_ikatan_kerja'];
            } 
            elseif ($ref == 'jabfung') {
              $kode = $result['id_jabfung'];
              $nama = $result['nm_jabfung'];
              $custom = array($result['id_kel_prof'], $result['angka_kredit']);
            } 
            elseif ($ref == 'jalur_masuk') {
              $kode = $result['id_jalur_masuk'];
              $nama = $result['nm_jalur_masuk'];
              $ket = $result['ket_jalur_masuk'];
            } 
            elseif ($ref == 'jenis_evaluasi') {
              $kode = $result['id_jns_eval'];
              $nama = $result['nm_jns_eval'];
              $ket = $result['ket_jns_eval'];
            } 
            elseif ($ref == 'jenis_keluar') {
              $kode = $result['id_jns_keluar'];
              $nama = $result['ket_keluar'];
            } 
            elseif ($ref == 'jenis_pendaftaran') {
              $kode = $result['id_jns_daftar'];
              $nama = $result['nm_jns_daftar'];
            } 
            elseif ($ref == 'jenis_sert') {
              $kode = $result['id_jns_sert'];
              $nama = $result['nm_jns_sert'];
            } 
            elseif ($ref == 'jenis_sms') {
              $kode = $result['id_jns_sms'];
              $nama = $result['nm_jns_sms'];
            } 
            elseif ($ref == 'jenis_subst') {
              $kode = $result['id_jns_subst'];
              $nama = $result['nm_jns_subst'];
            } 
            elseif ($ref == 'jenis_tinggal') {
              $kode = $result['id_jns_tinggal'];
              $nama = $result['nm_jns_tinggal'];
            } 
            elseif ($ref == 'jenjang_pendidikan') {
              $kode = $result['id_jenj_didik'];
              $nama = $result['nm_jenj_didik'];
              $custom = array($result['u_jenj_lemb'], $result['u_jenj_org']);
            } 
            elseif ($ref == 'jurusan') {
              // $kode = $result['id_jenj_didik'];
              // $nama = $result['nm_jenj_didik'];
            } 
            elseif ($ref == 'kebutuhan_khusus') {
              $kode = $result['id_kk'];
              $nama = $result['nm_kk'];
            } 
            elseif ($ref == 'lembaga_pengangkat') {
              $kode = $result['id_lemb_angkat'];
              $nama = $result['nm_lemb_angkat'];
            } 
            elseif ($ref == 'level_wilayah') {
              $kode = $result['id_level_wil'];
              $nama = $result['nm_level_wilayah'];
            } 
            elseif ($ref == 'negara') {
              $kode = $result['id_negara'];
              $nama = $result['nm_negara'];
            } 
            elseif ($ref == 'pangkat_golongan') {
              $kode = $result['id_pangkat_gol'];
              $nama = $result['kode_gol'] . ' - ' . $result['nm_pangkat'];
            } 
            elseif ($ref == 'pekerjaan') {
              $kode = $result['id_pekerjaan'];
              $nama = $result['nm_pekerjaan'];
            } 
            elseif ($ref == 'penghasilan') {
              $kode = $result['id_penghasilan'];
              $nama = $result['nm_penghasilan'];
            } 
            elseif ($ref == 'status_keaktifan_pegawai') {
              $kode = $result['id_stat_aktif'];
              $nama = $result['nm_stat_aktif'];
            } 
            elseif ($ref == 'status_kepegawaian') {
              $kode = $result['id_stat_pegawai'];
              $nama = $result['nm_stat_pegawai'];
            } 
            elseif ($ref == 'alat_transport') {
              $kode = $result['id_alat_transport'];
              $nama = $result['nm_alat_transport'];
            } 
            elseif ($ref == 'pembiayaan') {
              $kode = $result['id_pembiayaan'];
              $nama = $result['nm_pembiayaan'];
            } 
            elseif ($ref == 'jenis_prestasi') {
              $kode = $result['id_jns_prestasi'];
              $nama = $result['nm_jns_prestasi'];
            } 
            elseif ($ref == 'tingkat_prestasi') {
              $kode = $result['id_tkt_prestasi'];
              $nama = $result['nm_tkt_prestasi'];
            } 
            elseif ($ref == 'jenis_aktivitas_mahasiswa') {
              $kode = $result['id_jns_akt_mhs'];
              $nama = $result['nm_jns_akt_mhs'];
            } 
            elseif ($ref == 'kategori_kegiatan') {
              $kode = $result['id_katgiat'];
              $nama = $result['nm_kat'];
            }

            $master = $em->getRepository('AppBundle:Master')
                ->findOneBy(array(
                  'type'  => $ref,
                  'kode'  => $kode
                ));
            if (!$master) {
              $master = new \AppBundle\Entity\Master();
              $master->setType($ref);
            }
            $master->setKode($kode);
            $master->setNama($nama);
            $master->setKet($ket);
            if (isset($custom[0])) {
              $master->setCustom1($custom[0]);
            }
            if (isset($custom[1])) {
              $master->setCustom2($custom[1]);
            }
            if (isset($custom[2])) {
              $master->setCustom3($custom[2]);
            }
            if (isset($custom[3])) {
              $master->setCustom4($custom[3]);
            }
            if (isset($custom[4])) {
              $master->setCustom5($custom[4]);
            }
            if (isset($custom[5])) {
              $master->setCustom6($custom[5]);
            }
            if (isset($custom[6])) {
              $master->setCustom7($custom[6]);
            }
            if (isset($custom[7])) {
              $master->setCustom8($custom[7]);
            }
            if (isset($custom[8])) {
              $master->setCustom9($custom[8]);
            }
            if (isset($custom[9])) {
              $master->setCustom10($custom[9]);
            }
            // $master->setRaw($raw);
            $em->persist($master);

        }

        $em->flush();
        $response->setData($ret);
        sleep(1);
        return $response;
    } 

}
