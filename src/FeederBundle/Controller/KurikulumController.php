<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class KurikulumController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/feeder/kurikulum/ajax_import", name="feeder_kurikulum_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        $response = new JsonResponse();
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $request->get('kode_prodi') );
        $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
          'table'   => "kurikulum",
          'filter'  => "id_sms='".$prodi->getUuid()."'",
          'limit'   => 1,
          'offset'  => $request->get('offset')
        ) );
        if( isset($dataFeeder['result'][0]) ) {
          $data = $dataFeeder['result'][0];
          $this->insertOrUpdateAction($prodi, $data);
          $response->setData( array(
            'success'   => 1,
            'message'   => 'Kurikulum ' . $data['nm_kurikulum_sp'] . ' berhasil diimport...'
          ) );
        }
        sleep(1);
        return $response;
    }


    private function insertOrUpdateAction($prodi, $data = array()) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $now = new\DateTime();
      $id_smt = $data['id_smt'];
      $ta_thn = substr($id_smt, 0, 4);
      $ta_kode_smt = substr($id_smt, -1);
      $ta = $this->appService->getTa($id_smt, true);
      $kurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
        ->findOneByUuid( $data['id_kurikulum_sp'] );

      if ( !$kurikulum ) {
        $kurikulum = new \AppBundle\Entity\Kurikulum();
        $kurikulum->setNama( $data['nm_kurikulum_sp'] );
        $kurikulum->setSksWajib( $data['jml_sks_wajib'] );
        $kurikulum->setSksPilihan( $data['jml_sks_pilihan'] );
        $kurikulum->setJumlahSksLulus( $data['jml_sks_lulus'] );
        $kurikulum->setJumlahSksWajib( $data['jml_sks_wajib'] );
        $kurikulum->setJumlahSksPilihan( $data['jml_sks_pilihan'] );
        $kurikulum->setJumlahSemesterNormal( $data['jml_sem_normal'] );
        $kurikulum->setProdi( $prodi );
        $kurikulum->setTa( $ta );
      }
      $kurikulum->setKode( $id_smt );
      $kurikulum->setRaw( $data ); 
      $kurikulum->setUuid( $data['id_kurikulum_sp'] );
      $em->persist($kurikulum);
      $em->flush();

      return;

    }

    /**
     * @Route("/feeder/kurikulum/tester", name="tester_kurikulum_get_list")
     */
    public function feederGetListKurikulumSandbox()
    {
        $results = $this->getFeedData(63201);
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/kurikulum/tester2", name="tester_kurikulum_get_list2")
     */
    public function feederGetListKurikulumSandbox2()
    {
        $results = $this->getFeedData(63201);

        // $response = new JsonResponse();
        $ret = array();
        foreach ( $results as $data) {

          $kode_prodi = 63201;

          $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
            ->findOneByKodeProdi( $kode_prodi );

          if ( ! $prodi ) {
            $ret['success'] = 0;
            $ret['message'] = '...';
          } else {
            $this->insertOrUpdateAction($prodi, $data);
            $ret['success'] = 1;
            $ret['message'] = 'Kurikulum ' . $data['nm_kurikulum_sp'] . ' berhasil diimport';
          }

        }

        // $response->setData($ret);
        // sleep(1);
        // return $response;

        echo "<pre>";
        print_r($ret);
        echo "</pre>";
        exit;
    }


}
