<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MakulController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    // private function getFeedData($kode_prodi, $results = array()) 
    // {
    //     $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
    //       ->findOneByKodeProdi($kode_prodi);
    //     $dataMakul = $this->feeder->ws( 'GetListMataKuliah', array(
    //       'filter'  => "kode_prodi='".$prodi->getKodeProdi()."'",
    //       'limit'   => 1000
    //     ) );
    //     if ( count($dataMakul['result']) > 0 ) {
          
    //       $data = $dataMakul['result'];
          
    //       for ( $i = 0; $i < count($data); $i++ ) {

    //         $res = $data[$i];
    //         $res['kr_makul'] = array();

    //         $dataKurikulumMakulFeeder = $this->feeder->ws( 'GetRecordset', array(
    //           'table'   => 'mata_kuliah_kurikulum',
    //           'filter'  => "kode_mk='".trim($res['kode_mk'])."'",
    //           'limit'   => 1000
    //         ) );
    //         if ( isset($dataKurikulumMakulFeeder['result']) && count($dataKurikulumMakulFeeder['result']) > 0 ) {
    //           foreach ($dataKurikulumMakulFeeder['result'] as $kr_makul) {
    //             $res['kr_makul'][] = $kr_makul;
    //           }
    //         }

    //         $results[] = $res;

    //       }

    //     }

    //     return $results;
    // }

    public function getFeedData() {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( 63101 );
        $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
          'table'   => "mata_kuliah",
          'filter'  => "kode_prodi='".$prodi->getKodeProdi()."'",
          'limit'   => 1,
          'offset'  => 10
        ) );
        if( isset($dataFeeder['result'][0]) ) {
          $data = $dataFeeder['result'][0];

          $dataKurikulumMakulFeeder = $this->feeder->ws( 'GetRecordset', array(
            'table'   => 'mata_kuliah_kurikulum',
            'filter'  => "kode_mk='".trim($data['kode_mk'])."'",
            'limit'   => 1000
          ) );
          if ( isset($dataKurikulumMakulFeeder['result']) && count($dataKurikulumMakulFeeder['result']) > 0 ) {
            $data['kr_makul'] = $dataKurikulumMakulFeeder['result'];
            // foreach ($dataKurikulumMakulFeeder['result'] as $kr_makul) {
            //   foreach ($kr_makul as $key => $value) {
            //     $kr_makul[$key] = trim($value);
            //   }

            //   $data['kr_makul'][] = $kr_makul;

            //   // $dataKelasFeeder = $this->feeder->ws( 'GetListKelasKuliah', array(
            //   //   'filter'  => "kode_prodi='".$prodi->getKodeProdi()."' AND kode_mk='".$kr_makul['fk__id_mk']."'"
            //   // ) );
            //   // if ( isset($dataKelasFeeder['result']) && count($dataKelasFeeder['result']) > 0 ) {
            //   //   foreach ($dataKelasFeeder['result'] as $kelas_kuliah) {
            //   //     foreach ($kelas_kuliah as $key => $value) {
            //   //       $kelas_kuliah[$key] = trim($value);
            //   //     }
            //   //     // if ( $kelas_kuliah['id_smt'] == $kurikulum->getKode() ) {
            //   //     //   $kelas = $em->getRepository('AppBundle:Kelas')
            //   //     //     ->findOneBy(array(
            //   //     //       'prodi'   => $prodi,
            //   //     //       'nama'    => $kelas_kuliah['nm_kls']
            //   //     //     ));
            //   //     //   if ( !$kelas ) {
            //   //     //     $kelas = new \AppBundle\Entity\Kelas();
            //   //     //     $kelas->setProdi($prodi);
            //   //     //     $kelas->setNama($kelas_kuliah['nm_kls']);
            //   //     //     $kelas->setStatus('publish');
            //   //     //   }
            //   //     //   $kelas->setUuid($kelas_kuliah['id_kls']);
            //   //     //   $em->persist($kelas);
            //   //     //   $em->flush();
            //   //     //   /*---------*/
            //   //     //   $mk->setKelas($kelas);
            //   //     //   $em->persist($mk);
            //   //     //   $em->flush();
            //   //     //   /*---------*/
            //   //     //   if ( !empty($kelas_kuliah['dosen']) ) {
            //   //     //     $dosenPengampuList = explode("#", $kelas_kuliah['dosen']);
            //   //     //     foreach ($dosenPengampuList as $nama_dosen) {
            //   //     //       if ( !empty($nama_dosen) ) {
            //   //     //         $user = $em->getRepository('AppBundle:User')
            //   //     //           ->findOneBy(array(
            //   //     //             // 'prodi' => $prodi,
            //   //     //             'nama'      => trim($nama_dosen),
            //   //     //             'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 3)
            //   //     //           ));
            //   //     //         if ( $user ) {
            //   //     //           if ( null !== $user->getDataDosen() ) {
            //   //     //             $dosen = $user->getDataDosen();
            //   //     //             $dp = $em->getRepository('AppBundle:DosenPengampu')
            //   //     //               ->findOneBy(array(
            //   //     //                 'makul'     => $mk,
            //   //     //                 'dosen'     => $dosen
            //   //     //               ));
            //   //     //             if ( !$dp ) {
            //   //     //               $dp = new \AppBundle\Entity\DosenPengampu();
            //   //     //               $dp->setMakul($mk);
            //   //     //               $dp->setDosen($dosen);
            //   //     //               $dp->setStatus('publish');
            //   //     //               $em->persist($dp);
            //   //     //               $em->flush();
            //   //     //             }
            //   //     //           }
            //   //     //         }
            //   //     //       }
            //   //     //     }
            //   //     //   }

            //   //     //   // import KRS
            //   //     //   $dataNilaiKrsFeeder = $this->feeder->ws( 'GetRecordset', array(
            //   //     //     'table'   => "nilai",
            //   //     //     'filter'  => "nm_kls='".$kelas_kuliah['nm_kls']."' AND kode_mk='".$kelas_kuliah['kode_mk']."' AND id_smt='".$kelas_kuliah['id_smt']."'",
            //   //     //     'limit'   => 1000
            //   //     //   ) );
            //   //     //   if ( isset($dataNilaiKrsFeeder['result']) && count($dataNilaiKrsFeeder['result']) > 0 ) {
            //   //     //     foreach ($dataNilaiKrsFeeder['result'] as $nilai_krs) {
            //   //     //       foreach ($nilai_krs as $key => $value) {
            //   //     //         $nilai_krs[$key] = trim($value);
            //   //     //       }
            //   //     //       $mhsUser = $em->getRepository('AppBundle:User')
            //   //     //         ->findOneByUsername($nilai_krs['nipd']);
            //   //     //       if ( $mhsUser ) {
            //   //     //         if ( null !== $mhsUser->getDataMahasiswa() ) {
            //   //     //           $mhs = $mhsUser->getDataMahasiswa();
            //   //     //           $krs = $em->getRepository('AppBundle:Krs')
            //   //     //             ->findOneBy(array(
            //   //     //               'mahasiswa'   => $mhs,
            //   //     //               'makul'       => $mk,
            //   //     //             ));
            //   //     //           if ( !$krs ) {
            //   //     //             $krs = new \AppBundle\Entity\Krs();
            //   //     //             $krs->setMahasiswa($mhs);
            //   //     //             $krs->setMakul($mk);
            //   //     //           }
            //   //     //           $krs->setSemester($mk->getSemester());
            //   //     //           $krs->setStatus('diterima');
            //   //     //           $krs->setNilaiAngka($nilai_krs['nilai_indeks']);
            //   //     //           $krs->setNilaiHuruf($nilai_krs['nilai_huruf']);
            //   //     //           $krs->setNilaiAkhir($nilai_krs['nilai_angka']);
            //   //     //           $em->persist($krs);
            //   //     //           $em->flush();
            //   //     //         }
            //   //     //       }
            //   //     //       sleep(0.5);
            //   //     //     }
            //   //     //   }

            //   //     // }
            //   //   }
            //   // }
            // }
          }

          // $this->insertOrUpdateAction(trim($data['kode_mk']), $prodi, $data);
          // $response->setData(array(
          //   'success'     => 1,
          //   'message'     => 'Mata kuliah ' . $data['nm_mk'] . ' berhasil diimport'
          // ));
        }
        return $data;
    }


    /**
     * @Route("/feeder/mata_kuliah/ajax_import", name="feeder_mata_kuliah_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        $response = new JsonResponse();
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $request->get('kode_prodi') );
        $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
          'table'   => "mata_kuliah",
          'filter'  => "kode_prodi='".$prodi->getKodeProdi()."'",
          'limit'   => 1,
          'offset'  => $request->get('offset')
        ) );
        if( isset($dataFeeder['result'][0]) ) {
          $data = $dataFeeder['result'][0];

          $dataKurikulumMakulFeeder = $this->feeder->ws( 'GetRecordset', array(
            'table'   => 'mata_kuliah_kurikulum',
            'filter'  => "kode_mk='".trim($data['kode_mk'])."'",
            'limit'   => 1000
          ) );
          if ( isset($dataKurikulumMakulFeeder['result']) && count($dataKurikulumMakulFeeder['result']) > 0 ) {
            $data['kr_makul'] = $dataKurikulumMakulFeeder['result'];
          }

          $this->insertOrUpdateAction(trim($data['kode_mk']), $prodi, $data);
          $response->setData(array(
            'success'     => 1,
            'message'     => 'Mata kuliah ' . $data['nm_mk'] . ' berhasil diimport'
          ));
        }
        sleep(1);
        return $response;
    }



    private function insertOrUpdateAction($kode_mk, $prodi, $data = array()) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        if (!is_array($value)) {
          $data[$key] = trim($value);
        }
      }
      $now = new\DateTime();
      $makul = $em->getRepository('AppBundle:Makul')
        ->findOneByKode( $kode_mk );

      if ( !$makul ) {
        $makul = new \AppBundle\Entity\Makul();
        $makul->setKode( $kode_mk );
        $makul->setNama( $data['nm_mk'] );
        $makul->setNamaEng( '' );
        $makul->setStatus('publish');
        $makul->setSksTeori( $data['sks_tm'] );
        $makul->setSksPraktek( $data['sks_prak'] );
        $makul->setSksLapangan( $data['sks_prak_lap'] );
        $makul->setSilabus( $data['a_silabus'] );
        $makul->setSatuanAcara( $data['a_sap'] );
        $makul->setBahanAjar( $data['a_bahan_ajar'] );
        $makul->setDiktat( $data['a_diktat'] );
        $makul->setProdi( $prodi );
        $makul->setJenis( $this->appService->getMasterTermObject('jenis_makul', $data['jns_mk']) );
        $makul->setKelompok( $this->appService->getMasterTermObject('jenis_makul', $data['kel_mk']) );
      }
      $makul->setUuid( $data['id_mk'] );
      $makul->setRaw( $data );
      $em->persist($makul);
      $em->flush();


      // set kurikulum makul
      if ( isset($data['kr_makul']) && count($data['kr_makul']) > 0 ) {
        foreach ($data['kr_makul'] as $kr_mk) {
          foreach ($kr_mk as $key => $value) {
            $kr_mk[$key] = trim($value);
          }
          $kurikulum = $em->getRepository('AppBundle:Kurikulum')
            ->findOneByUuid($kr_mk['id_kurikulum_sp']);
          if ( $kurikulum ) {

            // ambil kelas dulu
            $dataKelasFeeder = $this->feeder->ws( 'GetListKelasKuliah', array(
              'filter'  => "kode_prodi='".$prodi->getKodeProdi()."' AND kode_mk='".$kode_mk."'"
            ) );
            if ( isset($dataKelasFeeder['result']) && count($dataKelasFeeder['result']) > 0 ) {

              foreach ($dataKelasFeeder['result'] as $kelas_kuliah) {
                foreach ($kelas_kuliah as $key => $value) {
                  $kelas_kuliah[$key] = trim($value);
                }
                if ( $kelas_kuliah['id_smt'] == $kurikulum->getKode() ) {

                  $kelas = $em->getRepository('AppBundle:Kelas')
                    ->findOneBy(array(
                      'prodi'   => $prodi,
                      'nama'    => $kelas_kuliah['nm_kls']
                    ));
                  if ( !$kelas ) {
                    $kelas = new \AppBundle\Entity\Kelas();
                    $kelas->setProdi($prodi);
                    $kelas->setNama($kelas_kuliah['nm_kls']);
                    $kelas->setStatus('publish');
                  }
                  $kelas->setUuid($kelas_kuliah['id_kls']);
                  $em->persist($kelas);
                  $em->flush();

                  $mk = $em->getRepository('AppBundle:KurikulumMakul')
                    ->findOneBy(array(
                      'kurikulum' => $kurikulum,
                      'makul'     => $makul,
                      'semester'  => $kr_mk['smt'],
                      'kelas'     => $kelas
                    ));
                  if ( !$mk ) {
                    $mk = new \AppBundle\Entity\KurikulumMakul();
                    $mk->setKurikulum($kurikulum);
                    $mk->setTa($kurikulum->getTa());
                    $mk->setMakul($makul);
                    $mk->setSemester($kr_mk['smt']);
                    $mk->setKelas($kelas);
                    $mk->setStatus('publish');
                  }
                  $mk->setProdi( ( null !== $kurikulum->getProdi() ) ? $kurikulum->getProdi() : $prodi );
                  $mk->setUuid($kr_mk['id_mk']);
                  $em->persist($mk);
                  $em->flush();
                  /*---------*/
                  if ( !empty($kelas_kuliah['dosen']) ) {
                    $dosenPengampuList = explode("#", $kelas_kuliah['dosen']);
                    foreach ($dosenPengampuList as $nama_dosen) {
                      if ( !empty($nama_dosen) ) {
                        $user = $em->getRepository('AppBundle:User')
                          ->findOneBy(array(
                            // 'prodi' => $prodi,
                            'nama'      => trim($nama_dosen),
                            'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 3)
                          ));
                        if ( $user ) {
                          if ( null !== $user->getDataDosen() ) {
                            $dosen = $user->getDataDosen();
                            $dp = $em->getRepository('AppBundle:DosenPengampu')
                              ->findOneBy(array(
                                'makul'     => $mk,
                                'dosen'     => $dosen
                              ));
                            if ( !$dp ) {
                              $dp = new \AppBundle\Entity\DosenPengampu();
                              $dp->setMakul($mk);
                              $dp->setDosen($dosen);
                              $dp->setStatus('publish');
                              $em->persist($dp);
                              $em->flush();
                            }
                          }
                        }
                      }
                    }
                  }

                  // import KRS
                  $dataNilaiKrsFeeder = $this->feeder->ws( 'GetRecordset', array(
                    'table'   => "nilai",
                    'filter'  => "nm_kls='".$kelas_kuliah['nm_kls']."' AND kode_mk='".$kelas_kuliah['kode_mk']."' AND id_smt='".$kelas_kuliah['id_smt']."'",
                    'limit'   => 1000
                  ) );
                  if ( isset($dataNilaiKrsFeeder['result']) && count($dataNilaiKrsFeeder['result']) > 0 ) {
                    foreach ($dataNilaiKrsFeeder['result'] as $nilai_krs) {
                      foreach ($nilai_krs as $key => $value) {
                        $nilai_krs[$key] = trim($value);
                      }
                      $mhsUser = $em->getRepository('AppBundle:User')
                        ->findOneByUsername($nilai_krs['nipd']);
                      if ( $mhsUser ) {
                        if ( null !== $mhsUser->getDataMahasiswa() ) {
                          $mhs = $mhsUser->getDataMahasiswa();
                          $krs = $em->getRepository('AppBundle:Krs')
                            ->findOneBy(array(
                              'mahasiswa'   => $mhs,
                              'makul'       => $mk,
                              'semester'    => $mk->getSemester()
                            ));
                          if ( !$krs ) {
                            $krs = new \AppBundle\Entity\Krs();
                            $krs->setMahasiswa($mhs);
                            $krs->setMakul($mk);
                            $krs->setSemester($mk->getSemester());
                          }
                          $krs->setStatus('diterima');
                          $krs->setNilaiAngka($nilai_krs['nilai_indeks']);
                          $krs->setNilaiHuruf($nilai_krs['nilai_huruf']);
                          $krs->setNilaiAkhir($nilai_krs['nilai_angka']);
                          $em->persist($krs);
                          $em->flush();
                        }
                      }
                      sleep(0.5);
                    }
                  }

                }
              }
            }

          }
        }
      }
      return;
    }

    /**
     * @Route("/feeder/mata_kuliah/tester", name="tester_mata_kuliah_get_list")
     */
    public function feederGetListMakulSandbox()
    {
        // $results = $this->getRecordDataAction(63201, 'mata_kuliah');
        $results = $this->getFeedData();
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/mata_kuliah/tester2", name="tester_mata_kuliah_get_list2")
     */
    public function feederGetListMakulSandbox2()
    {
        $count = $this->getRecordDataAction(63101, 'mata_kuliah');
        $ret = array();

        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( 63101 );

        for ($i=0; $i < $count; $i++) { 

          $dataFeeder = $this->feeder->ws( 'GetRecordset', array(
            'table'   => "mata_kuliah",
            'filter'  => "kode_prodi='".$prodi->getKodeProdi()."'",
            'limit'   => 1,
            'offset'  => $i
          ) );
          if( isset($dataFeeder['result'][0]) ) {
            $data = $dataFeeder['result'][0];

            $data['kr_makul'] = array();

            $dataKurikulumMakulFeeder = $this->feeder->ws( 'GetRecordset', array(
              'table'   => 'mata_kuliah_kurikulum',
              'filter'  => "kode_mk='".trim($data['kode_mk'])."'",
              'limit'   => 1000
            ) );
            if ( isset($dataKurikulumMakulFeeder['result']) && count($dataKurikulumMakulFeeder['result']) > 0 ) {
              foreach ($dataKurikulumMakulFeeder['result'] as $kr_makul) {
                $data['kr_makul'][] = $kr_makul;
              }
            }

            $this->insertOrUpdateAction(trim($data['kode_mk']), $prodi, $data);
            $ret[] = array(
              'success'     => 1,
              'message'     => 'Mata kuliah ' . $data['nm_mk'] . ' berhasil diimport'
            );
          }
        }

        echo "<pre>";
        print_r($ret);
        echo "</pre>";
        exit;
    }

    private function getRecordDataAction($kode_prodi, $table, $tahun = 0)
    {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $kode_prodi );
        $params = array( 'table' => $table );
        if( $table == 'mahasiswa_pt' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."' AND mulai_smt ilike '".$tahun."%'";
        } elseif ( $table == 'kurikulum' ) {
          $params['filter'] = "id_sms='".$prodi->getUuid()."'";
        } elseif ( $table == 'mata_kuliah' ) {
          $params['filter'] = "kode_prodi='".$prodi->getKodeProdi()."'";
        }
        $dataFeederCount = $this->feeder->ws( 'GetCountRecordset', $params);
        if ( !isset($dataFeederCount['result']) ) {
          return 0;
        }
        return $dataFeederCount['result'];
    }
}
