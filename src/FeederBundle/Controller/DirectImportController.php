<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;

class DirectImportController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }


    /**
     * @Route("/feeder/direct/wilayah", name="feeder_direct_wilayah")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dataCount = $this->feeder->ws('GetCountRecordset', array(
          'table'   => "wilayah"
        ));
        $limit = 500;
        $count = ( isset($dataCount['result']) ) ? $dataCount['result'] : 0;
        if ( $count > 0 ) {
          $total_pages = ceil($count/$limit);
          for ($i=0; $i <= $total_pages; $i++) { 
            $dataFeeder = $this->feeder->ws('GetRecordset', array(
              'table'   => "wilayah",
              'filter'  => "id_negara='ID'",
              'offset'  => ($i-1)*$limit,
              'limit'   => $limit,
            ));
            if ( isset($dataFeeder['result']) && count($dataFeeder['result']) > 0 ) {
              for ($j=0; $j < count($dataFeeder['result']); $j++) { 

                $data = array();
                foreach ($dataFeeder['result'][$j] as $key => $value) {
                  $data[$key] = trim($value);
                }

                /*-------------*/
                $kode = $data['id_wil'];
                $wilayah = $em->getRepository('AppBundle:Wilayah')
                  ->findOneByKode( $kode );
                if ( !$wilayah ) {
                  $wilayah = new \AppBundle\Entity\Wilayah();
                  $wilayah->setKode($kode);
                  $wilayah->setNama( $data['nm_wil'] );
                }
                $wilayah->setRaw($data);
                $wilayah->setLevel( $this->appService->getMasterTermObject('level_wilayah', $data['id_level_wil']) );
                $parent = $em->getRepository('AppBundle:Wilayah')
                  ->findOneByKode( $data['id_induk_wilayah'] );
                if ( !$parent ) {
                    $parentFeeder = $this->feeder->ws('GetRecord', array(
                      'table'   => "wilayah",
                      'filter'  => "id_wil='".$data['id_induk_wilayah']."'",
                    ));
                    if ( count($parentFeeder['result']) > 0 ) {
                      $dataParent = $parentFeeder['result'];
                      $parent = new \AppBundle\Entity\Wilayah();
                      $parent->setNama( trim($dataParent['nm_wil']) );
                      $parent->setKode( $data['id_induk_wilayah'] );
                      $parent->setLevel( $this->appService->getMasterTermObject('level_wilayah', trim($dataParent['id_level_wil'])) );
                      $parent->setRaw($dataParent);
                      $em->persist($parent);
                      $em->flush();
                    }
                }
                $wilayah->setParent($parent);
                $em->persist($wilayah);
                $em->flush();
                /*-------------*/

              }
            }
            sleep(1);
          }
        }

        return new Response('success');
    }

    /**
     * @Route("/feeder/direct/sp", name="feeder_direct_sp")
     */
    public function spAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dataCount = $this->feeder->ws('GetCountRecordset', array(
          'table'   => "satuan_pendidikan"
        ));
        $limit = 1000;
        $count = ( isset($dataCount['result']) ) ? $dataCount['result'] : 0;
        if ( $count > 0 ) {
          $total_pages = ceil($count/$limit);
          for ($i=0; $i <= $total_pages; $i++) { 
            $dataFeeder = $this->feeder->ws('GetRecordset', array(
              'table'   => "satuan_pendidikan",
              'offset'  => ($i-1)*$limit,
              'limit'   => $limit,
            ));
            if ( isset($dataFeeder['result']) && count($dataFeeder['result']) > 0 ) {
              for ($j=0; $j < count($dataFeeder['result']); $j++) { 

                $data = array();
                foreach ($dataFeeder['result'][$j] as $key => $value) {
                  $data[$key] = trim($value);
                }

                /*-------------*/
                $kode = $data['npsn'];
                $sp = $em->getRepository('AppBundle:SatuanPendidikan')
                  ->findOneByKode( $kode );
                if ( !$sp ) {
                  $sp = new \AppBundle\Entity\SatuanPendidikan();
                  $sp->setKode($kode);
                  $sp->setNama( $data['nm_lemb'] );
                  $sp->setSingkatan( $data['nm_singkat'] );
                  if ( !empty($data['id_bp']) && $data['id_bp'] > 18 && $data['id_bp'] < 24 ) {
                    $sp->setJenis('perguruan_tinggi');
                  } else {
                    $sp->setJenis('sekolah');
                  }
                  $bp = $em->getRepository('AppBundle:Master')
                    ->findOneByKode( $this->appService->getMasterTermObject('bentuk_pendidikan', $data['id_bp']) );
                  if ( $bp ) {
                    $sp->setBp($bp);
                  }
                  $sp->setAlamat($data['jln']);                  
                  $wilayah = $em->getRepository('AppBundle:Wilayah')
                    ->findOneByKode($data['id_wil']);
                  if ( $wilayah ) {
                    $sp->setWilayah($wilayah);
                  }
                  $sp->setPos($data['kode_pos']);
                  $sp->setTelp($data['no_tel']);
                  $sp->setFax($data['no_fax']);
                  $sp->setEmail($data['email']);
                  $sp->setWebsite($data['website']);
                  $sp->setUuid($data['id_sp']);
                  $sp->setRaw($data);
                  $em->persist($sp);
                  $em->flush();
                }
                /*-------------*/

              }
            }
            sleep(1);
          }
        }

        return new Response('success');
    }

}

