<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;

class ExportPerkuliahanController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/export/perkuliahan/get_data", name="feeder_export_perkuliahan_get_data")
     * @Method({"POST"})
     */
    public function exportGetDataAction(Request $request) 
    {

      $results = array();
      $response = new JsonResponse();

      $table = $request->get('table');
      $kode_prodi = $request->get('kode_prodi');
      $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
        ->findOneByKodeProdi( $kode_prodi );

      if ( ! $prodi ) {
        return $response;
      }

      if ( $table == 'kurikulum' ) {

        $dataKurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
          ->findByProdi($prodi);

        foreach ($dataKurikulum as $data) {
          $results[] = array(
            'id_siakad'         => $data->getId(),
            'id_kurikulum_sp'   => ( null !== $data->getUuid() ) ? $data->getUuid() : "",
            'id_sms'            => $prodi->getUuid(),
            'id_jenj_didik'     => (int)$prodi->getJenjang()->getKode(),
            'id_smt'            => (int)$data->getSemester()->getKode(),
            'nm_kurikulum_sp'   => $data->getNama(),
            'jml_sem_normal'    => (int)$data->getJumlahSemesterNormal(),
            'jml_sks_lulus'     => (int)$data->getJumlahSksLulus(),
            'jml_sks_wajib'     => (int)$data->getJumlahSksWajib(),
            'jml_sks_pilihan'   => (int)$data->getJumlahSksPilihan(),
          );
        }

      } 
      elseif ( $table == 'mata_kuliah' ) {

        $dataMakul = $this->getDoctrine()->getRepository('AppBundle:Makul')
          ->findAll();

        foreach ($dataMakul as $data) {
          if ( null !== $data->getKurikulum() ) {
            
            if ( null !== $data->getKurikulum()->getProdi() && $prodi == $data->getKurikulum()->getProdi() ) {

                $results[] = array(
                  'id_mk'             => ( null !== $data->getUuid() ) ? $data->getUuid() : "",
                  'id_sms'            => $prodi->getUuid(),
                  'id_jenj_didik'     => (int)$prodi->getJenjang()->getKode(),
                  'kode_mk'           => $data->getKode(),
                  'nm_mk'             => $data->getNama(),
                  'jns_mk'            => ( null !== $data->getJenis() ) ? $data->getJenis()->getKode() : null,
                  'kel_mk'            => ( null !== $data->getKelompok() ) ? $data->getKelompok()->getKode() : null,
                  // 'sks_mk'            => (int),
                  'sks_tm'            => (int) $data->getSksTeori(),
                  'sks_prak'          => (int) $data->getSksPraktek(),
                  'sks_prak_lap'      => (int) $data->getSksLapangan(),
                  // 'sks_sim'           => (int),
                  'metode_pelaksanaan_kuliah' => '',
                  'a_sap'             => (int) $data->getSatuanAcara(),
                  'a_silabus'         => (int) $data->getSilabus(),
                  'a_bahan_ajar'      => (int) $data->getBahanAjar(),
                  // 'acara_prak'        => (int) ,
                  'a_diktat'          => (int) $data->getDiktat(),
                  // 'tgl_mulai_efektif' => ,
                );

            }

          }

        }
        
      }

      $response->setData($results);

      return $response;

    }

    /**
     * @Route("/feeder/export/perkuliahan/mata_kuliah", name="feeder_export_perkuliahan_mata_kuliah")
     * @Method({"POST"})
     */
    public function exportMakulAction(Request $request) {

      $ret = array();
      $response = new JsonResponse();

      $ret['success'] = 0;
      $ret['message'] = 'gagal';

      $data = $request->get('data');

      if ( !empty($data['id_mk']) ) {

        $record = array(
          'key'     => array('id_mk' => $data['id_mk']),
          'data'    => $data
        );

        $feederKurikulum = $this->feeder->ws( 'GetRecord', array(
          'table'   => 'mata_kuliah',
          'filter'  => "id_mk='".$data['id_mk']."'",
        ) );

        if ( count($feederKurikulum['result']) > 0 ) {
          
          //mode update
          $result = $this->feeder->ws( 'UpdateRecord', array(
            'table' => 'mata_kuliah',
            'key'   => $data['id_mk'],
            'data'  => json_encode($data)
          ));

          $ret['success'] = 1;
          $ret['message'] = 'Mata kuliah ' . $data['nm_mk'] . ' berhasil diexport';

        } 


      }

      else {

        // mode insert
        unset($data['id_mk']);
        $result = $this->feeder->ws( 'InsertRecord', array(
          'table' => 'mata_kuliah',
          'data'  => json_encode($data)
        ));

        $ret['success'] = 1;
        $ret['message'] = 'Mata kuliah ' . $data['nm_mk'] . ' berhasil ditambah';

      }


      $response->setData($ret);
      sleep(1);
      return $response;

    }

    /**
     * @Route("/feeder/export/perkuliahan/kurikulum", name="feeder_export_perkuliahan_kurikulum")
     * @Method({"POST"})
     */
    public function exportKurikulumAction(Request $request) {

      $ret = array();
      $response = new JsonResponse();

      $ret['success'] = 0;
      $ret['message'] = 'gagal';

      $data = $request->get('data');

      // $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
      //   ->findOneByKodeProdi( $request->get('kode_prodi') );


      if ( !empty($data['id_kurikulum_sp']) ) {

        $record_data = array(
          'id_sms'            => $data['id_sms'],
          'id_jenj_didik'     => $data['id_jenj_didik'],
          'id_smt'            => $data['id_smt'],
          'nm_kurikulum_sp'   => $data['nm_kurikulum_sp'],
          'jml_sem_normal'    => $data['jml_sem_normal'],
          'jml_sks_lulus'     => $data['jml_sks_lulus'],
          'jml_sks_wajib'     => $data['jml_sks_wajib'],
          'jml_sks_pilihan'   => $data['jml_sks_pilihan'],
        );

        $record = array(
          'key'     => array('id_kurikulum_sp' => $data['id_kurikulum_sp']),
          'data'    => $record_data
        );

        $feederKurikulum = $this->feeder->ws( 'GetRecord', array(
          'table'   => 'kurikulum',
          'filter'  => "id_kurikulum_sp='".$data['id_kurikulum_sp']."'",
        ) );

        if ( count($feederKurikulum['result']) > 0 ) {
          
          //mode update
          $result = $this->feeder->ws( 'UpdateRecord', array(
            'table' => 'kurikulum',
            'data'  => json_encode($record)
          ));

          // if ( isset($result['result']) ) {
          //   $ret['success'] = 1;
          //   $ret['message'] = 'berhasil diimport';
          // } else {
          //   $ret['success'] = 0;
          //   $ret['message'] = '...';
          // }
          $ret['success'] = 1;
          $ret['message'] = 'Kurikulum ' . $data['nm_kurikulum_sp'] . ' berhasil diexport';

        } 


      }

      else {

        $record = array(
          'id_sms'            => $data['id_sms'],
          'id_jenj_didik'     => $data['id_jenj_didik'],
          'id_smt'            => $data['id_smt'],
          'nm_kurikulum_sp'   => $data['nm_kurikulum_sp'],
          'jml_sem_normal'    => $data['jml_sem_normal'],
          'jml_sks_lulus'     => $data['jml_sks_lulus'],
          'jml_sks_wajib'     => $data['jml_sks_wajib'],
          'jml_sks_pilihan'   => $data['jml_sks_pilihan'],
        );

        $result = $this->feeder->ws( 'InsertRecord', array(
          'table' => 'kurikulum',
          'data'  => json_encode($record)
        ));

        if ( isset($result['result']['id_kurikulum_sp']) ) {
          $kurikulum = $this->getDoctrine()->getRepository('AppBundle:Kurikulum')
            ->find( $data['id_siakad'] );
          if ( $kurikulum ) {
            $kurikulum->setUuid( $result['result']['id_kurikulum_sp'] );
            $em = $this->getDoctrine()->getManager();
            $em->persist($kurikulum);
            $em->flush();
          }
        }

        $ret['success'] = 1;
        $ret['message'] = 'Kurikulum ' . $data['nm_kurikulum_sp'] . ' berhasil ditambah';

      }


      $response->setData($ret);
      sleep(1);
      return $response;

    }

    /**
     * @Route("/feeder/export/tester/perkuliahan/kurikulum", name="feeder_export_tester_perkuliahan_kurikulum")
     */
    public function exportTesterKurikulumAction(Request $request) {

      $result = array();

      $kode_prodi = 63101;
      $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
        ->findOneByKodeProdi($kode_prodi);

      $record = array(
        'id_sms'            => $prodi->getUuid(),
        'id_jenj_didik'     => (int)$prodi->getJenjang()->getKode(),
        'id_smt'            => 20171,
        'nm_kurikulum_sp'   => 'Kurikulum 20181',
        'jml_sem_normal'    => 44,
        'jml_sks_lulus'     => 44,
        'jml_sks_wajib'     => 44,
        'jml_sks_pilihan'   => 44,
      );

      $result = $this->feeder->ws( 'InsertRecord', array(
        'table' => 'kurikulum',
        'data'  => json_encode($record)
      ));
      
      echo "<pre>";
      print_r($result);
      echo "</pre>";exit;

    }

}
