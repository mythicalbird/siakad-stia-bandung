<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TesterController extends Controller
{
    protected $appService;
    protected $feeder;

    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/testersss/index", name="feeder_tester_index")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
          ->add('method', ChoiceType::class, array(
              'choices' => array(
                  'ListTable'      => 'ListTable',
                  'GetToken'      => 'GetToken',
                  'GetRecord'     => 'GetRecord',
                  'GetRecordset'  => 'GetRecordset',
                  'GetDictionary'     => 'GetDictionary',
                  'GetListPrestasiMahasiswa'  => 'GetListPrestasiMahasiswa',
                  'GetListAktifitasMahasiswa'     => 'GetListAktifitasMahasiswa',
                  'GetListBimbinganMahasiswa'  => 'GetListBimbinganMahasiswa',
                  'GetListUjiMahasiswa'     => 'GetListUjiMahasiswa',
                  'GetPembiayaan'  => 'GetPembiayaan',
                  'GetJenisPrestasi'     => 'GetJenisPrestasi',
                  'GetTingkatPrestasi'  => 'GetTingkatPrestasi',
                  'GetJenisAktifitasMahasiswa'     => 'GetJenisAktifitasMahasiswa',
                  'GetKategoriKegiatan'  => 'GetKategoriKegiatan',
                  'GetListMahasiswa'     => 'GetListMahasiswa',
                  'GetListRiwayatPendidikanMahasiswa'  => 'GetListRiwayatPendidikanMahasiswa',
                  'GetNilaiTransferPendidikanMahasiswa'     => 'GetNilaiTransferPendidikanMahasiswa',
                  'GetKRSMahasiswa'  => 'GetKRSMahasiswa',
                  'GetRiwayatNilaiMahasiswa'     => 'GetRiwayatNilaiMahasiswa',
                  'GetAktivitasKuliahMahasiswa'  => 'GetAktivitasKuliahMahasiswa',
                  'GetAgama'     => 'GetAgama',
                  'GetBentukPendidikan'  => 'GetBentukPendidikan',
                  'GetIkatanKerjaSdm'     => 'GetIkatanKerjaSdm',
                  'GetJabfung'  => 'GetJabfung',
                  'GetJalurMasuk'     => 'GetJalurMasuk',
                  'GetJenisEvaluasi'  => 'GetJenisEvaluasi',
                  'GetJenisKeluar'     => 'GetJenisKeluar',
                  'GetJenisSertifikasi'  => 'GetJenisSertifikasi',
                  'GetJenisPendaftaran'     => 'GetJenisPendaftaran',
                  'GetJenisSMS'  => 'GetJenisSMS',
                  'GetJenisSubstansi'     => 'GetJenisSubstansi',
                  'GetJenisTinggal'  => 'GetJenisTinggal',
                  'GetJenjangPendidikan'     => 'GetJenjangPendidikan',
                  'GetJurusan'  => 'GetJurusan',
                  'GetKebutuhanKhusus'     => 'GetKebutuhanKhusus',
                  'GetLembagaPengangkat'  => 'GetLembagaPengangkat',
                  'GetLevelWilayah'     => 'GetLevelWilayah',
                  'GetNegara'  => 'GetNegara',
                  'GetPangkatGolongan'     => 'GetPangkatGolongan',
                  'GetPekerjaan'  => 'GetPekerjaan',
                  'GetPenghasilan'     => 'GetPenghasilan',
                  'GetSemester'  => 'GetSemester',
                  'GetStatusKeaktifanPegawai'     => 'GetStatusKeaktifanPegawai',
                  'GetStatusKepegawaian'  => 'GetStatusKepegawaian',
                  'GetStatusMahasiswa'     => 'GetStatusMahasiswa',
                  'GetTahunAjaran'  => 'GetTahunAjaran',
                  'GetWilayah'     => 'GetWilayah',
                  'GetListDosen'  => 'GetListDosen',
                  'DetailBiodataDosen'     => 'DetailBiodataDosen',
                  'GetListPenugasanDosen'     => 'GetListPenugasanDosen',
                  'GetAktivitasMengajarDosen'  => 'GetAktivitasMengajarDosen',
                  'GetRiwayatFungsionalDosen'     => 'GetRiwayatFungsionalDosen',
                  'GetRiwayatPangkatDosen'  => 'GetRiwayatPangkatDosen',
                  'GetRiwayatPendidikanDosen'     => 'GetRiwayatPendidikanDosen',
                  'GetRiwayatSertifikasiDosen'  => 'GetRiwayatSertifikasiDosen',
                  'GetRiwayatPenelitianDosen'     => 'GetRiwayatPenelitianDosen',
                  'GetMahasiswaBimbinganDosen'  => 'GetMahasiswaBimbinganDosen',
                  'GetListPenugasanSemuaDosen'     => 'GetListPenugasanSemuaDosen',
                  'GetDetailPenugasanDosen'  => 'GetDetailPenugasanDosen',
                  'GetListMataKuliah'     => 'GetListMataKuliah',
                  'GetDetailMataKuliah'  => 'GetDetailMataKuliah',
                  'GetListKurikulum'     => 'GetListKurikulum',
                  'GetDetailKurikulum'  => 'GetDetailKurikulum',
                  'GetListKelasKuliah'     => 'GetListKelasKuliah',
                  'GetDetailKelasKuliah'  => 'GetDetailKelasKuliah',
                  'GetDosenPengajarKelasKuliah'     => 'GetDosenPengajarKelasKuliah',
                  'GetPerhitunganSKS'  => 'GetPerhitunganSKS',
                  'GetPesertaKelasKuliah'     => 'GetPesertaKelasKuliah',
                  'GetListNilaiPerkuliahanKelas'  => 'GetListNilaiPerkuliahanKelas',
                  'GetListPerkuliahanMahasiswa'     => 'GetListPerkuliahanMahasiswa',
                  'GetDetailPerkuliahanMahasiswa'  => 'GetDetailPerkuliahanMahasiswa',
                  'GetListMahasiswaLulusDO'     => 'GetListMahasiswaLulusDO',
                  'GetDetailMahasiswaLulusDO'  => 'GetDetailMahasiswaLulusDO',
                  'GetListSkalaNilaiProdi'     => 'GetListSkalaNilaiProdi',
                  'GetDetailSkalaNilaiProdi'     => 'GetDetailSkalaNilaiProdi',
                  'GetListPeriodePerkuliahan'  => 'GetListPeriodePerkuliahan',
                  'GetDetailPeriodePerkuliahan'     => 'GetDetailPeriodePerkuliahan',
                  'GetRekapLaporan'  => 'GetRekapLaporan',
                  'GetRekapJumlahDosen'     => 'GetRekapJumlahDosen',
                  'GetRekapJumlahMahasiswa'  => 'GetRekapJumlahMahasiswa',
                  'GetRekapIPSMahasiswa'     => 'GetRekapIPSMahasiswa',
                  'GetRekapKRSMahasiswa'  => 'GetRekapKRSMahasiswa',
                  'GetRekapKHSMahasiswa'     => 'GetRekapKHSMahasiswa',
                  'GetPembiayaan'  => 'GetPembiayaan',
                  'GetJenisPrestasi'     => 'GetJenisPrestasi',
                  'GetTingkatPrestasi'  => 'GetTingkatPrestasi',
                  'GetJenisAktivitasMahasiswa'     => 'GetJenisAktivitasMahasiswa',
              )
          ))
          ->add('filter', null, array(
              'required'  => false
          ))
          ->add('order', null, array(
              'required'  => false
          ))
          ->add('limit', null, array(
              'required'  => false
          ))
          ->add('offset', null, array(
              'required'  => false
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted()) {
            $data = $form->getData();
            $response = new JsonResponse();
            $method = $data['method'];
            $args = array();
            $command = $this->feeder->ws( $method, $args );
            $response->setData( print_r($command) );
            return $response;
        }
        return $this->appService->load('FeederBundle:Default:tester_index.html.twig', array(
          'form'  => $form->createView(),
        ));
    }

    /**
     * @Route("/feeder/tester/{method}/{args}", name="feeder_tester")
     */
    public function testerAction(Request $request, $method, $args = '')
    {
        if ($args != '') {
          $args = json_decode($args, true);
        } else {
          $args = array();
        }
        $command = $this->feeder->ws( $method, $args );
        echo "<pre>";
        print_r($command);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/referensi/{ref}", name="feeder_referensi")
     */
    public function referensiAction(Request $request, $ref)
    {
        // $command = $this->feeder->ws( 'GetRecordset', array(
        //   'table'   => $ref,
        //   'filter'  => "id_level_wil=0"
        // ) );
        // if ( isset( $command['result'] ) ) {
        //   echo json_encode($command['result'], JSON_PRETTY_PRINT);
        // }
        // exit;
        $path = $this->get('kernel')->getProjectDir() . '/web';
        $filePath = $path . '/data/propinsi.json';
        return $filePath;
        $command = file_get_contents($filePath);
        echo "<pre>";
        print_r(json_decode($command, true));
        echo "</pre>";
        exit;
    }

}
