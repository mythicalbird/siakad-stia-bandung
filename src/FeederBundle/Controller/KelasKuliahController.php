<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class KelasKuliahController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/feeder/kelas_kuliah/ajax_feed", name="feeder_kelas_kuliah_ajax_feed")
     * @Method({"POST"})
     */
    public function feedAction(Request $request) 
    {
        $response = new JsonResponse();
        $results = array();

        if ( !empty( $request->get('kode_prodi') ) ) {
          $kode_prodi = $request->get('kode_prodi');
          $results = $this->getFeedData( $kode_prodi );
        }

        $response->setData($results);
        return $response;
    }

    private function getFeedData($kode_prodi, $results = array()) 
    {
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi($kode_prodi);
        $dataKelasKuliah = $this->feeder->ws( 'GetListKelasKuliah', array(
          'limit'   => 10000
        ) );
        if ( count($dataKelasKuliah['result']) > 0 ) {
          
          $data = $dataKelasKuliah['result'];
          $kelas_listed = array();
          for ( $i = 0; $i < count($data); $i++ ) {

            if ( $data[$i]['id_sms'] == $prodi->getUuid() ) {

              $nm_kls = trim($data[$i]['nm_kls']);
              if ( !in_array($nm_kls, $kelas_listed) ) {
                array_push($kelas_listed, $nm_kls);
                $results[] = $data[$i];
              }
              
            }

          }

        }
        return $results;
    }

    /**
     * @Route("/feeder/kelas_kuliah/ajax_import", name="feeder_kelas_kuliah_ajax_import")
     * @Method({"POST"})
     */
    public function importAction(Request $request) 
    { 
        $response = new JsonResponse();
        $data = $request->get('data');
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $request->get('kode_prodi') );
        $this->insertOrUpdateAction($data['nm_kls'], $prodi, $data);
        $response->setData(array(
          'success'   => 1,
          'message'   => 'Kelas ' . $data['nm_kls'] . ' berhasil diimport'
        ));
        sleep(1);
        return $response;
    }


    private function insertOrUpdateAction($nm_kls, $prodi, $data = array()) 
    {
      $em = $this->getDoctrine()->getManager();
      foreach ($data as $key => $value) {
        $data[$key] = trim($value);
      }
      $now = new\DateTime();
      $kelas = $em->getRepository('AppBundle:Kelas')
        ->findOneBy( array(
          'prodi' => $prodi,
          'nama'  => $nm_kls,
        ) );

      if ( !$kelas ) {
        $kelas = new \AppBundle\Entity\Kelas();
        $kelas->setNama($nm_kls);
        $kelas->setProdi($prodi);
        $kelas->setStatus('publish');
      }
      $kelas->setUuid( $data['id_kls'] );
      $em->persist($kelas);
      $em->flush();


      // set dosen pengampu
      if ( !empty($data['dosen']) ) {
        $user = $em->getRepository('AppBundle:User')
          ->findOneByNama( rtrim($data['dosen'], '#') );
        if ( $user ) {
          if ( null !== $user->getDataDosen() ) {
            // cek makul dl apakah ada atau tidak
            $makul = $em->getRepository('AppBundle:Makul')
              ->findOneBy(array(
                'kode'    => $data['kode_mk'],
                'nama'    => $data['nm_mk']
              ));
            if ( $makul ) { 

              // jika makul ada, coba untuk update kurikulum
              // untuk mendapatkan kurikulum, panggil tahun akademik terlebih dahulu
              $id_smt = $data['id_smt'];
              $tahun = substr($id_smt, 0, 4);
              $kodeSemester = ( substr($id_smt, -1) == 1 ) ? 1 : 2;
              $ta = $em->getRepository('AppBundle:TahunAkademik')
                ->findOneByKode($id_smt);
              if ( !$ta ) {
                $ta = new \AppBundle\Entity\TahunAkademik();
                $ta->setKode($id_smt);
                $nm_ta = $tahun;
                $nm_ta .= '/';
                $nm_ta .= $tahun+1;
                $ta->setNama($nm_ta);
                $ta->setTahun( $tahun );
                $ta->setKodeSemester( $kodeSemester );
                $ta->setAktif(0);
                $ta->setStatus('active');
                $em->persist($ta);
                $em->flush();
              }
              $periode = $em->getRepository('AppBundle:PmbPeriode')
                ->findOneBy(array(
                  'tahun' => $tahun,
                  'prodi' => $prodi
                ));
              if ( !$periode ) {
                $periode = new \AppBundle\Entity\PmbPeriode();
                $periode->setTahun($tahun);
                $periode->setStatus(0);
                $periode->setProdi($prodi);
                $em->persist($periode);
                $em->flush();
              }
              $kurikulum = $em->getRepository('AppBundle:kurikulum')
                ->findOneBy(array(
                  'ta'      => $ta,
                  'prodi'   => $prodi, 
                  'kode'    => $id_smt
                ));
              if ( !$kurikulum ) {
                $kurikulum = new \AppBundle\Entity\kurikulum();
                $kurikulum->setNama( $tahun . "-" . $kodeSemester );
                // $kurikulum->setSksWajib( 0 );
                // $kurikulum->setSksPilihan( 0 );
                // $kurikulum->setJumlahSksLulus( 0 );
                // $kurikulum->setJumlahSksWajib( 0 );
                // $kurikulum->setJumlahSksPilihan( 0 );
                // $kurikulum->setJumlahSemesterNormal( 0 );
                $kurikulum->setProdi( $prodi );
                $kurikulum->setTa( $ta );
                $kurikulum->setKode( $id_smt );
                $em->persist($kurikulum);
                $em->flush();
              }

              $makul->setKurikulum($kurikulum);
              $em->persist($makul);
              $em->flush();

              $dosen = $user->getDataDosen();
              $dosen_pengampu = $em->getRepository('AppBundle:DosenPengampu')
                ->findOneBy(array(
                  'makul'     => $makul,
                  'kelas'     => $nm_kls,
                  'dosen'     => $dosen
                ));
              if ( !$dosen_pengampu ) {
                $dosen_pengampu = new \AppBundle\Entity\DosenPengampu();
                $dosen_pengampu->setMakul($makul);
                $dosen_pengampu->setKelas($nm_kls);
                $dosen_pengampu->setDosen($dosen);
                $dosen_pengampu->setStatus('publish');
                $em->persist($dosen_pengampu);
                $em->flush();
              }
            }

          }
        }
      }
      return $kelas;
    }

    /**
     * @Route("/feeder/kelas_kuliah/tester", name="tester_kelas_kuliah_get_list")
     */
    public function feederGetListKelasKuliahSandbox()
    {
        $kode_prodi = 63201;
        $results = $this->getFeedData($kode_prodi);
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        exit;
    }

    /**
     * @Route("/feeder/kelas_kuliah/tester2", name="tester_kelas_kuliah2_get_list")
     */
    public function feederGetListKelasKuliah2Sandbox()
    {
        $kode_prodi = 63201;
        $results = $this->getFeedData($kode_prodi);
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi( $kode_prodi );
        $ret = array();
        foreach ( $results as $data) {
            $this->insertOrUpdateAction($data['nm_kls'], $prodi, $data);
            $ret['success'] = 1;
            $ret['message'] = 'Kelas ' . $data['nm_kls'] . ' berhasil diimport';
        }
        echo "<pre>";
        print_r($ret);
        echo "</pre>";
        exit;
    }


}
