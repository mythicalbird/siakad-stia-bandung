<?php

namespace FeederBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Semester;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\Setting;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends Controller
{
    protected $appService;
    protected $feeder;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );
    public function __construct(AppService $appService, FeederService $feeder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
    }

    /**
     * @Route("/feeder/pddikti", name="feeder_index")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Setting::class)
          ->findOneByName('server_feeder_dikti');
        if( ! $data ) {
          $data = new Setting();
          $data->setName('server_feeder_dikti');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => true))
              ->add('server', ChoiceType::class, array(
                  'label'   => 'Server Feeder',
                  'choices' => array(
                      'Live'  => 'live',
                      'Sandbox' => 'sandbox',
                  )
              ))
              ->add('url', null, array(
                  'required'  => false,
                  'label'   => 'URL',
              ))
              ->add('username', null, array(
                  'required'  => false,
                  'label'   => 'Username',
              ))
              ->add('password', PasswordType::class, array(
                  'required'  => false,
                  'label'   => 'Password'
              ))
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $dataProdi = $em
              ->getRepository('AppBundle:ProgramStudi')
              ->findAll();

            if ( $dataProdi ) {

              foreach ($dataProdi as $prodi) {

                $dataSms = $this->feeder->ws( 'GetRecord', array(
                  'table'   => "sms",
                  'filter'  => "kode_prodi='".$prodi->getKodeProdi()."'"
                ) );

                if ( isset($dataSms['result']) && count($dataSms['result']) > 0 ) {
                  
                  $sms = $dataSms['result'];
                  $jenjang = $this->appService->getMasterTermObject( 'jenjang_pendidikan', (int)trim($sms['id_jenj_didik']) );
                  if ( null !== $jenjang ) {
                    $prodi->setJenjang( $jenjang->getNama() );
                  }
                  $prodi->setNamaProdi( $sms['nm_lemb'] );
                  $prodi->setUuid( $sms['id_sms'] );
                  $em->persist($prodi);
                  $em->flush();

                }

              }

            }

            $em->persist($data);
            $em->flush(); 
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');

            // update id_sp
            $dataSp = $this->feeder->ws( 'GetRecord', array(
              'table'   => "satuan_pendidikan",
              'filter'  => "npsn='".$data->getValue()['username']."'"
            ) );
            if ( count($dataSp['result']) > 0 ) {
              $id_sp = trim($dataSp['result']['id_sp']);
              $this->appService->updateSetting('id_sp', array($id_sp));
            }
        }
        $prodiList = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')->findAll();
        $taList = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')->findAll();
        return $this->appService->load('FeederBundle:Default:index.html.twig', array(
          'form'  => $form->createView(),
          'prodiList' => $prodiList,
          'taList'  => $taList
        ));
    }

    /**
     * @Route("/feeder/siakad", name="feeder_import_index")
     */
    public function siakadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sp = $this->feeder->ws( 'GetRecord', array(
          'table'   => "satuan_pendidikan",
          'filter'  => "npsn='043141'"
        ) );
        if ( isset($sp['result']) && count($sp['result']) > 0 ) {
          $sp = $sp['result'];
          $this->response['result']['sp'] = $sp;
        }

        // update otomatis data prodi
        $dataProdi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findAll();
        foreach ($dataProdi as $dp) {

          $sms = $this->feeder->ws( 'GetRecord', array(
            'table'   => "sms",
            'filter'  => "kode_prodi='".$dp->getKodeProdi()."' AND id_sp='".$sp['id_sp']."'"
          ) );

          if ( isset($sms['result']) && count($sms['result']) > 0 ) {
            
            $sms = $sms['result'];
            $jenjang = $this->appService->getMasterTermObject( 'jenjang_pendidikan', (int)trim($sms['id_jenj_didik']) );
            if ( null !== $jenjang ) {
              $dp->setJenjang( $jenjang->getNama() );
            }
            // $dp->setNamaProdi( $sms['nm_lemb'] );
            $dp->setUuid( $sms['id_sms'] );
            $dp->setRaw($sms);
            $em->persist($dp);
            $em->flush();

            $this->response['result']['prodi_list'][] = $sms;

          }

        }


        $refData = $this->feeder->ws( 'ListTable', array('jenis' => 'ref') ); 
        $refs = ( isset($refData['error']) ) ? array() : $refData['result'];

        $prodiList = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')->findAll();
        $taList = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')->findAll();
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('FeederBundle:Default:feeder_to_siakad.html.twig', array(
              'refs'  => $refs,
              'prodiList' => $prodiList,
              'taList'  => $taList
            ));
        }
    }
    
}
