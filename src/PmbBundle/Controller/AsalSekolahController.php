<?php

namespace PmbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;
use AppBundle\Entity\Wilayah;

class AsalSekolahController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/pmb/sp/sekolah/{aksi}", name="asal_sekolah" )
     */
    public function sekolahIndexAction(Request $request, $aksi = 'index')
    {
        if ( $aksi == 'edit' ) {
            if ( !empty($request->get('id')) ) {
                $data = $this->getDoctrine()->getRepository('AppBundle:SatuanPendidikan')
                    ->find($request->get('id'));
            } else {
                $data = new \AppBundle\Entity\SatuanPendidikan();
                $data->setJenis('sekolah');
            }
            $form = $this->createFormBuilder($data)
                ->add('kode', null, array(
                    'label'   => 'ID Sekolah',
                ))
                ->add('nama', null, array(
                    'label'   => 'Nama Sekolah',
                ))
                ->add('wilayah', EntityType::class, array(
                  'label' => 'Alamat',
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level= :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
                  },
                  'choice_label' => function( Wilayah $entity = null ){
                    $choice_label = $entity->getNama();
                    if ( null !== $entity->getParent() ) {
                      $parent = $entity->getParent();
                      $choice_label .= ', ' . $parent->getNama();
                      if ( null !== $parent->getParent() ) {
                        $choice_label .= ', ' . $parent->getParent()->getNama();
                      }
                    }
                    return $choice_label;
                  },
                  'attr'  => array(
                    'class' => 'select2',
                    'style' => 'width:100%'
                  ),
                ))
                ->add('pos', null, array(
                    'label'   => 'Kodepos',
                ))
                ->add('telp', null, array(
                    'label'   => 'Telepon',
                ))
                ->add('fax', null, array(
                    'label'   => 'Fax',
                ))
                ->add('email', null, array(
                    'label'   => 'Email'
                ))
                ->add('website', null, array(
                    'label'   => 'Website'
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Simpan',
                    'attr'  => array(
                        'class'   => 'btn btn-primary'
                    )
                ))
                ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                $this->addFlash('success', 'Data berhasil disimpan.');
                return $this->redirectToRoute('asal_sekolah');
            }
        } else {
            $data = $this->getDoctrine()->getRepository('AppBundle:SatuanPendidikan')
                ->findByJenis('sekolah');
        }
        return $this->appService->load('PmbBundle:Default:asal_sekolah_'.$aksi.'.html.twig', array(
        	'data'	=> $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }

    /**
     * @Route("/pmb/sp/pt/{aksi}", name="asal_perguruan_tinggi" )
     */
    public function ptIndexAction(Request $request, $aksi = 'index')
    {
        if ( $aksi == 'edit' ) {
            if ( !empty($request->get('id')) ) {
                $data = $this->getDoctrine()->getRepository('AppBundle:SatuanPendidikan')
                    ->find($request->get('id'));
            } else {
                $data = new \AppBundle\Entity\SatuanPendidikan();
                $data->setJenis('perguruan_tinggi');
            }
            $form = $this->createFormBuilder($data)
                ->add('kode', null, array(
                    'label'   => 'ID Perguruan Tinggi',
                ))
                ->add('nama', null, array(
                    'label'   => 'Nama Perguruan Tinggi',
                ))
                ->add('wilayah', EntityType::class, array(
                  'label' => 'Alamat',
                  'class' => 'AppBundle:Wilayah',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('w')
                          ->where('w.level= :level')
                          ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
                  },
                  'choice_label' => function( Wilayah $entity = null ){
                    $choice_label = $entity->getNama();
                    if ( null !== $entity->getParent() ) {
                      $parent = $entity->getParent();
                      $choice_label .= ', ' . $parent->getNama();
                      if ( null !== $parent->getParent() ) {
                        $choice_label .= ', ' . $parent->getParent()->getNama();
                      }
                    }
                    return $choice_label;
                  },
                  'attr'  => array(
                    'class' => 'select2',
                    'style' => 'width:100%'
                  ),
                ))
                ->add('pos', null, array(
                    'label'   => 'Kodepos',
                ))
                ->add('telp', null, array(
                    'label'   => 'Telepon',
                ))
                ->add('fax', null, array(
                    'label'   => 'Fax',
                ))
                ->add('email', null, array(
                    'label'   => 'Email'
                ))
                ->add('website', null, array(
                    'label'   => 'Website'
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Simpan',
                    'attr'  => array(
                        'class'   => 'btn btn-primary'
                    )
                ))
                ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                $this->addFlash('success', 'Data berhasil disimpan.');
                return $this->redirectToRoute('asal_perguruan_tinggi');
            }
        } else {
            $data = $this->getDoctrine()->getRepository('AppBundle:SatuanPendidikan')
                ->findByJenis('perguruan_tinggi');
        }
        return $this->appService->load('PmbBundle:Default:asal_pt_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }
}
