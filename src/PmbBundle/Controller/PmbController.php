<?php

namespace PmbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Master;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\User;
use AppBundle\Entity\Wilayah;
use AppBundle\Entity\SatuanPendidikan;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\MahasiswaFormType;
use AppBundle\Service\AppService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Form\Type\BerkasType;
use AppBundle\Form\Type\PmbOrangTuaType;
use AppBundle\Form\Type\PmbFormType;

class PmbController extends Controller
{
    protected $appService;
    protected $encoder;
  
    public function __construct(AppService $appService, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/pmb/daftar", name="pmb_index")
     */
    public function indexAction(Request $request, AppService $appService)
    {
        $em = $this->getDoctrine()->getManager();
        // $periode = $em->createQueryBuilder()
        //   ->select('p')
        //   ->from('AppBundle:PmbPeriode', 'p')
        //   ->where('p.status=:status AND ')
        $periode = $this->getDoctrine()
            ->getRepository('AppBundle:PmbPeriode')
            ->findOneBy(array(
                'status'  => 'active',
                'tahun'   => date('Y')
            ));

        if (!$periode) {
            throw $this->createNotFoundException(
                'Tidak ada periode PMB yang terbuka saat ini!'
            );
        }

        $asalNamaPtChoices = array();
        $asalJenjangChoices = array();
        $asalProdiChoices = array();
        $asalNamaPtObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('asal_perguruan_tinggi');
        if ( $asalNamaPtObjects ) {
          foreach ($asalNamaPtObjects as $asalNamaPt) {
            $asalNamaPtChoices[$asalNamaPt->getNama()] = $asalNamaPt->getNama();
          }
        }
        $asalJenjangObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('jenjang_pendidikan');
        if ( $asalJenjangObjects ) {
          foreach ($asalJenjangObjects as $asalJenjang) {
            $asalJenjangChoices[$asalJenjang->getNama()] = $asalJenjang->getNama();
          }
        }
        $asalProdiObjects = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findByType('asal_prodi');
        if ( $asalProdiObjects ) {
          foreach ($asalProdiObjects as $asalProdi) {
            $asalProdiChoices[$asalProdi->getNama()] = $asalProdi->getNama();
          }
        }
        $option = $this->appService->getGlobalOption();
        $ta = $option['ta'];

        $builder = $this->createFormBuilder();
        $builder
          ->add('periode', EntityType::class, array(
              'label' => 'Program Studi',
              'class' => 'AppBundle:PmbPeriode',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('p')
                    ->where('p.status=:status')
                    ->setParameter('status', 'active');
              },
              'choice_label' => function(PmbPeriode $entity = null) {
                  return $entity->getProdi()->getJenjang() . ' ' . $entity->getProdi()->getNamaProdi();
              },
              'placeholder' => '-- Pilih --',
          ))
          ->add('jalur', EntityType::class, array(
            'required'  => false,
            'label'     => 'Jalur PMB',
            'class'     => Master::class,
            'query_builder' => function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'jalur_pmb');
            },
            'choice_label' => 'nama',
            'placeholder' => '-- Pilih --',
          ))
          ->add('nama', null, array(
              'required'  => true,
              'label' => 'Nama Mahasiswa'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'label' => 'Tanggal Lahir',
              'widget'    => 'single_text',
              'html5'     => false, 
              'format'    => 'dd-MM-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('ktp', null, array(
              'label' => 'No KTP'
          ))
          ->add('noKk', null, array(
              'label' => 'No KK'
          ))
          ->add('npwp', null, array(
              'label' => 'NPWP'
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --' 
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder' => '-- Pilih --',
          ))
          ->add('wargaNegara', ChoiceType::class, array(
            'label'   => 'Warga Negara',
            'choices' => array(
              'WNI' => 'ID',
              'WNA' => 'WNA',
            ),
          ))
          ->add('statusSipil', ChoiceType::class, array(
            'label'         => 'Status Sipil',
            'choices'       => array(
              'Bujangan'  => 'bujangan',
              'Duda'      => 'duda',
              'Janda'     => 'janda',
              'Menikah'   => 'menikah',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('telp', null, array(
            'required'  => true,
            'label'   => 'No. Telp/HP',
          ))
          ->add('email', EmailType::class, array(
            'required'  => true, 
          ))
          ->add('alamat', TextareaType::class, array(
            'required'  => true,
          ))
          ->add('rt', null, array(
              'label' => 'RT',
          ))
          ->add('rw', null, array(
              'label' => 'RW',
          ))
          ->add('dusun', null, array(
              'required'  => false,
              'attr'      => array(
                'placeholder' => '(jika ada)'
              )
          ))
          ->add('desa', null, array(
              'required'  => false,
              'attr'      => array(
                'placeholder' => '(jika ada)'
              )
          ))
          ->add('wilayah', EntityType::class, array(
              'label' => 'Kec/Kab/Pro',
              'class' => 'AppBundle:Wilayah',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('w')
                      ->where('w.level= :level')
                      ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
              },
              'choice_label' => function( Wilayah $entity = null ){
                $choice_label = $entity->getNama();
                if ( null !== $entity->getParent() ) {
                  $parent = $entity->getParent();
                  $choice_label .= ', ' . $parent->getNama();
                  if ( null !== $parent->getParent() ) {
                    $choice_label .= ', ' . $parent->getParent()->getNama();
                  }
                }
                return $choice_label;
              },
              'attr'  => array(
                'class' => 'select2',
                'style' => 'width:100%'
              ),
          ))           
          ->add('pos', null, array(
              'label' => 'Kodepos',
          ))
          ->add('statusMasuk', ChoiceType::class, array(
            'required'      => false,
            'label'        => 'Status Masuk',
            'choices' => array(
              'BARU'  => 'BARU',
              'PINDAHAN'  => 'PINDAHAN',
            ),
            'placeholder'   => '-- Pilih --'
          ))
          ->add('riwayatPendidikan', CollectionType::class, array(
              'mapped'  => false 
          ))
          // ->add('asalSekolah', EntityType::class, array(
          //     'label' => 'Asal Sekolah',
          //     'class' => 'AppBundle:SatuanPendidikan',
          //     'query_builder' => function (EntityRepository $er) {
          //         return $er->createQueryBuilder('s');
          //     },
          //     'choice_label' => function( SatuanPendidikan $entity = null ){
          //       $choice_label = $entity->getNama();
          //       return $choice_label;
          //     },
          //     'attr'  => array(
          //       'class' => 'select2',
          //       'style' => 'width:100%'
          //     ),
          // ))  
          ->add('asalPt', EntityType::class, array(
              'label' => 'Asal Perguruan Tinggi',
              'class' => 'AppBundle:SatuanPendidikan',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('s')
                    ->where('s.jenis=:jenis')
                    ->setParameter('jenis', 'perguruan_tinggi');
              },
              'choice_label' => function( SatuanPendidikan $entity = null ){
                $choice_label = $entity->getNama();
                return $choice_label;
              },
              'attr'  => array(
                'class' => 'select2',
                'style' => 'width:100%'
              ),
          ))  
          // ->add('kodeSekolah', null, array(
          //     'required'      => false,
          //     'label'         => 'Kode Sekolah',
          // ))
          ->add('namaSekolah', null, array(
              'required'        => false,
              'label'          => 'Nama Sekolah',
          ))
          ->add('nis', null, array(
              'required'      => false,
              'label' => 'NIS',
          ))
          ->add('nilaiUn', null, array(
              'required'      => false,
              'label' => 'Nilai UN',
          ))
          ->add('asalKodePt', null, array(
              'required'      => false,
              'label' => 'Kode Perguruan Tinggi',
          ))
          ->add('asalNamaPt', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Nama Perguruan Tinggi',
              'choices'       => $asalNamaPtChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalJenjang', ChoiceType::class, array(
              'required'      => false,
              'label' => 'Jenjang',
              'choices'       => $asalJenjangChoices,
              'placeholder' => '-- Pilih --',
          ))
          ->add('asalProdi', ChoiceType::class, array(
              'required'      => false,
              'label'         => 'Program Studi',
              'choices'       => $asalProdiChoices,
              'placeholder'   => '-- Pilih --',
          ))
          ->add('asalNim', null, array(
              'required'      => false,
              'label' => 'NIM Asal',
          ))
          ->add('asalSksDiakui', null, array(
              'required'      => false,
              'label' => 'SKS Diakui',
          )) 
          ->add('maba', HiddenType::class, array(
              'data'  => 1
          ))
          ->add('angkatan', HiddenType::class, array(
              'data'  => date('Y')
          ))
          ->add(
              $builder->create('orangtua', FormType::class, array('by_reference' => true))
                  ->add('ayah', PmbOrangTuaType::class)
                  ->add('ibu', PmbOrangTuaType::class)
          )
        ;
        $masterBerkas = $this->getDoctrine()->getManager()->createQueryBuilder()
                ->select('m')
                ->from('AppBundle:Master', 'm')
                ->where('m.type= :type')
                ->setParameter('type', 'berkas_pmb')
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $berkasField = $builder->create('berkas', FormType::class, array('by_reference' => true
              ));
        foreach( $masterBerkas as $berkas ) {
          $berkasField
              ->add($berkas['kode'], BerkasType::class);
        }
        $builder
          ->add($berkasField) 
          ->add('submit', SubmitType::class, array(
              'label' => 'Selesai',
              'attr'  => array(
                  'class' => 'btn btn-primary'
              )
          ))
        ;

        $form = $builder->getForm();
    	  $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            $data['riwayatPendidikan'] = $_POST['riwayatPendidikan'];
            $data['kodeSekolah'] = '';
            $maba = $this->appService->newMahasiswa($data, $ta);

            return $this->redirectToRoute('pmb_formulir_view', array('id'=>$maba->getId()));
        }
        return $this->render('PmbBundle:Default:index.html.twig', array(
            'maba'    => ( isset($maba) ) ? $maba : null,
            'periode' => $periode,
            'form'	  => $form->createView()
        ));
    }
  
    /**
     * @Route("/pmb/daftar/formulir/cetak", name="pmb_formulir_cetak")
     */
    public function cetakAction(Request $request)
    {
        if(!empty($request->get('id'))) {
          $data = $this->getDoctrine()->getRepository('AppBundle:User')
              ->find($request->get('id'));
        } 
        if (!$data) {
            throw $this->createNotFoundException(
                'Terjadi kesalahan!'
            );
        }
        return $this->render('PmbBundle:Default:formulir_cetak.html.twig', array(
          'user' => $data
        ));
    }
  
    /**
     * @Route("/pmb/daftar/formulir/preview/{id}", name="pmb_formulir_view")
     */
    public function previewAction(Request $request, $id)
    {
        $pmb = $this->getDoctrine()->getRepository('AppBundle:User')
            ->find($id);
        if (!$pmb) {
            throw $this->createNotFoundException(
                'Terjadi kesalahan!'
            );
        }
        return $this->render('PmbBundle:Default:pmb_preview.html.twig', array(
            'data' => $pmb
        ));
    }
  
}
