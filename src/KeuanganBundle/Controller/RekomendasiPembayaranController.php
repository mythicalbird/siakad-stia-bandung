<?php

namespace KeuanganBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\RekomendasiPembayaran;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class RekomendasiPembayaranController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/keuangan/rekomendasi_pembayaran/{aksi}", name="rekomendasi_pembayaran")
     */
    public function rekomendasiPembayaranAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:RekomendasiPembayaran')
                ->find($request->get('id'));
          } else {
              $data = new RekomendasiPembayaran();
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }

          $form = $this->createFormBuilder($data)
            ->add('nim', null, array(
              'attr'  => array(
                'readonly'  => 'readonly'
              ),
            ))
            ->add('nama', null, array(
              'attr'  => array(
                'readonly'  => 'readonly'
              ),
            ))
            ->add('diskon', EntityType::class, array(
                'class' => 'AppBundle:Master',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                      ->where('m.type=:type')
                      ->setParameter('type', 'dispensasi_pembayaran');
                },
                'choice_label' => 'nama',
            ))
            ->add('prodi', HiddenType::class)
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted()) {

              $post = $form->getData();
              $em = $this->getDoctrine()->getManager();
              $diskon = $post->getDiskon()->getNama();
              $data->setDiskon($diskon);
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('rekomendasi_pembayaran');
          }

        } else {
           $data = $this->getDoctrine()->getRepository('AppBundle:RekomendasiPembayaran')->findAll();
        }

        return $this->appService->load('KeuanganBundle:Default:rekomendasi_pembayaran_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ));
    }

    /**
     * @Route("/keuangan/cari/mahasiswa", name="rekomendasi_pembayaran_cari")
     * @Method({"POST"})
     */
    public function cariMahasiswaAction(Request $request)
    {
        $response = new JsonResponse();
        $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ($mahasiswa) {
          if( null !== $mahasiswa->getProdi() ) {
            $prodi = $mahasiswa->getProdi()->getNamaProdi();
          } else {
            $prodi = 'Tidak ditemukan';
          }
          $html = '<div class="row"><div class="col-md-offset-3 col-md-6"><table class="table no-border"><tr><td>Nama Mahasiswa</td>';
          $html .= '<td><strong>'.$mahasiswa->getNama().'</strong></td>';
          $html .= '</tr><tr><td>NIM</td>';
          $html .= '<td><em>'.$mahasiswa->getUsername().'</em></td>';
          $html .= '</tr><tr><td>Tahun Angkatan</td>';
          $html .= '<td>'.$mahasiswa->getNama().'</td>';
          $html .= '</tr><tr><td>Kode Prodi</td>';
          $html .= '<td>'.$prodi.'</td>';
          $html .= '</tr><tr><td>Proses</td>';

          // cek table rekomendasi
          $rekomendasi = $this->getDoctrine()->getRepository('AppBundle:RekomendasiPembayaran')->findOneByNim($mahasiswa->getUsername());
          if (!$rekomendasi) {
            $html .= '<td><a href="#" data-toggle="modal" data-target="#rekomendasiModal" data-nim="'.$mahasiswa->getUsername().'" data-nama="'.$mahasiswa->getNama().'" data-prodi="'.$prodi.'">Tambah rekomendasi</a>';
            $html .= '</td>';
          } else {
            $html .= '<td>Sudah direkomendasikan</td>';
          }

          $html .= '</tr>';
          $html .= '</table></div></div>';
          $html .= '<div class="clearfix"></div>';
          $response->setData($html);
        } else {
          $response->setData('<div class="alert alert-error">Mahasiswa tidak ditemukan!</div>');
        }
        return $response;
    }

}
