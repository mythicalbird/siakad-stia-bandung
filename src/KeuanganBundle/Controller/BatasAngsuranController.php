<?php

namespace KeuanganBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Inventaris;
use AppBundle\Entity\Master;
use AppBundle\Entity\Lokasi;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class BatasAngsuranController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/keuangan/batas_angsuran", name="batas_angsuran_index")
     */
    public function indexAction()
    {

      return $this->appService->load('KeuanganBundle:Default:batas_angsuran_index.html.twig');
    }

    /**
     * @Route("/keuangan/batas_angsuran/{aksi}", name="batas_angsuran_edit")
     */
    public function predikatIndexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->find($request->get('id'));
          } else {
              $data = new Master();
              $data->setType('dispensasi_pembayaran');
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
	        $form = $this->createFormBuilder($data)
	            ->add('nama', null, array(
	            	'label'	=> 'Nama Rekomendasi'
	            ))
	            ->add('custom1', null, array(
	            	'label'	=> 'Sampai Semester'
	            ))
	            ->add('custom2', IntegerType::class, array(
	            	'label'	=> 'Diskon'
	            ))
	            ->add('submit', SubmitType::class, array(
	                'label' => 'Simpan',
	                'attr'  => array(
	                    'class' => 'btn btn-primary'
	                )
	            ))
	            ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('dispensasi_pembayaran');
          }
        } else {
           $data = $this->getDoctrine()->getRepository(Master::class)->findByType('dispensasi_pembayaran');
        }
        return $this->appService->load('KeuanganBundle:Default:dispensasi_pembayaran_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }

}
