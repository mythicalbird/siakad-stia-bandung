<?php

namespace KeuanganBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Inventaris;
use AppBundle\Entity\Master;
use AppBundle\Entity\Lokasi;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class DefaultController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/keuangan/master_biaya/{aksi}", name="biaya_kuliah_index")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit' && !empty($request->get('id'))) {
          $formData = $this->getDoctrine()->getRepository('AppBundle:PembayaranBiayaKuliah')
            ->find($request->get('id'));
        } else {
          $formData = new \AppBundle\Entity\PembayaranBiayaKuliah();
        }
        if ( null !== $this->getUser()->getProdi() ) {
          $formData->setProdi($this->getUser()->getProdi());
        }
        $semesterChoices = array();
        for ($i=1; $i <= 14; $i++) { 
          $semesterChoices['Semester ' . $i] = $i;
        }
        $form = $this->createFormBuilder($formData)
            // ->add('namaMahasiswa', null, array(
            //   'label' => 'Nama',
            //   'mapped'  => false,
            //   'attr'  => array(
            //       'readonly'  => 'readonly'
            //   )
            // ))
            // ->add('nimMahasiswa', null, array(
            //   'label' => 'NIM',
            //   'mapped'  => false,
            //   'attr'  => array(
            //       'readonly'  => 'readonly'
            //   )
            // ))
            ->add('kode', null, array(
              'label' => 'Kode Tagihan',
            ))
            ->add('nama', null, array(
              'label' => 'Nama Tagihan'
            ))
            ->add('gelombang1', null, array(
              'label' => 'Jumlah (Gel. 1)'
            ))
            ->add('gelombang2', null, array(
              'label' => 'Jumlah (Gel. 2)'
            ))
            ->add('gelombang3', null, array(
              'label' => 'Jumlah (Gel. 3)'
            ))
            ->add('semester', ChoiceType::class, array(
              'choices' => $semesterChoices
            ))
            ->add('batasBayar', DateType::class, array(
                'required'  => false,
                'label'     => 'Batas Pembayaran',
                'widget'    => 'single_text',
                'html5'     => false,
                'format'    => 'dd-MM-yyyy',
                'attr'      => ['class' => 'js-datepicker'],
            ))
            ->add('syaratKrs', ChoiceType::class, array(
              'label' => 'Syarat KRS',
              'choices' => array(
                  'YA'  => 'YA',
                  'TIDAK'  => 'TIDAK',
              )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formData);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('biaya_kuliah_index');
        }
        $data = $this->getDoctrine()->getRepository('AppBundle:PembayaranBiayaKuliah')
          ->findAll();
        return $this->appService->load('KeuanganBundle:Default:biaya_kuliah_'.$aksi.'.html.twig', array(
          'data' => $data,
          'form'  => $form->createView()
        ));
    }

    /**
     * @Route("/keuangan/dispensasi/{aksi}", name="dispensasi_pembayaran")
     */
    public function predikatIndexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                ->find($request->get('id'));
          } else {
              $data = new Master();
              $data->setType('dispensasi_pembayaran');
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
	        $form = $this->createFormBuilder($data)
	            ->add('nama', null, array(
	            	'label'	=> 'Nama Rekomendasi'
	            ))
	            ->add('custom1', null, array(
	            	'label'	=> 'Sampai Semester'
	            ))
	            ->add('custom2', IntegerType::class, array(
	            	'label'	=> 'Diskon'
	            ))
	            ->add('submit', SubmitType::class, array(
	                'label' => 'Simpan',
	                'attr'  => array(
	                    'class' => 'btn btn-primary'
	                )
	            ))
	            ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('dispensasi_pembayaran');
          }
        } else {
           $data = $this->getDoctrine()->getRepository(Master::class)->findByType('dispensasi_pembayaran');
        }
        return $this->appService->load('KeuanganBundle:Default:dispensasi_pembayaran_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }

}
