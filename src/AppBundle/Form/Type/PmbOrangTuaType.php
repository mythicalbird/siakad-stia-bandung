<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\Pmb;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\PmbPeriode;
use AppBundle\Entity\Wilayah;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PmbOrangTuaType extends AbstractType
{
    protected $em;
  
    public function __construct(EntityManager $em) {
      $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $levelProvinsi = $this->em->getRepository('AppBundle:Master')
          ->findOneBy(array('type' => 'level_wilayah', 'kode' => 1));
        $levelKota = $this->em->getRepository('AppBundle:Master')
          ->findOneBy(array('type' => 'level_wilayah', 'kode' => 2));
        $builder
          ->add('nama', null, array(
              'required'  => false,
          ))
          ->add('nik', null, array(
              'required'  => false,
              'label'	    => 'NIK',
              'required'  => false,
          ))
          ->add('agama', EntityType::class, array(
            'required'  => false,
            'class'			=> Master::class,
            'query_builder'	=> function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'agama');
            },
            'choice_value' => function (Master $entity = null) {
                return $entity ? $entity->getNama() : '';
            },
            'choice_label' => 'nama',
            'placeholder'	=> '-- Pilih --',
          ))
          ->add('pendidikan', ChoiceType::class, array(
            'required'  => false,
            'choices'	=> array(
              'TAMAT SMA'	      => 'TAMAT SMA',
              'TAMAT SMP'	      => 'TAMAT SMP',
              'TAMAT SD'	      => 'TAMAT SD',
              'TIDAK TAMAT SD'	=> 'TIDAK TAMAT SD',
            ),
            'placeholder' 	=> '-- Pilih --'
          ))
          ->add('pekerjaan', EntityType::class, array(
            'required'  => false,
            'class'     => Master::class,
            'query_builder' => function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'pekerjaan');
            },
            'choice_value' => function (Master $entity = null) {
                return $entity ? $entity->getNama() : '';
            },
            'choice_label' => 'nama'
          ))
          ->add('penghasilan', EntityType::class, array(
            'required'  => false,
            'class'     => Master::class,
            'query_builder' => function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'penghasilan');
            },
                  'choice_value' => function (Master $entity = null) {
                      return $entity ? $entity->getNama() : '';
                  },
            'choice_label' => 'nama'
          ))
          ->add('status', ChoiceType::class, array(
            'required'  => false,
            'choices'	=> array(
              'HIDUP'	      => 'HIDUP',
              'MENINGGAL'	  => 'MENINGGAL',
            ),
            'placeholder' 	=> '-- Pilih --'
          ))
          ->add('alamat', TextareaType::class, array(
              'required'  => false,
          ))
          ->add('provinsi', EntityType::class, array(
            'required'  => false,
            'class' => 'AppBundle:Wilayah',
            'query_builder' => function (EntityRepository $er) use ($levelProvinsi) {
                return $er->createQueryBuilder('w')
                    ->where('w.level = :level')
                    ->setParameter('level', $levelProvinsi);
            },
            'attr'          => array( 'class' => 'form-control select2', 'style' => 'width:100%' ),
            'choice_label'  => 'nama',
            'choice_value' => function (Wilayah $entity = null) {
                return $entity ? $entity->getId() : '';
            },
            'placeholder'	=> '-- Pilih --',
          ))
          ->add('kota', EntityType::class, array(
            'required'  => false,
            'label'     => 'Kota/Kabupaten',
            'class' => 'AppBundle:Wilayah',
            'query_builder' => function (EntityRepository $er) use ($levelKota) {
                return $er->createQueryBuilder('w')
                    ->where('w.level = :level')
                    ->setParameter('level', $levelKota);
            },
            'attr'          => array( 'class' => 'form-control select2', 'style' => 'width:100%' ),
            'choice_label'  => 'nama',
            'choice_value' => function (Wilayah $entity = null) {
                return $entity ? $entity->getId() : '';
            },
            'placeholder'	=> '-- Pilih --',
          ))
          ->add('pos', null, array(
              'required'  => false,
              'label'   => 'Kodepos'
          ))
          ->add('telp', null, array(
              'required'  => false,
              'label'   => 'Telepon'
          ))
          ->add('hp', null, array(
              'required'  => false,
              'label'   => 'Nomor HP'
          ))
          ->add('email', null, array(
              'required'  => false,
          ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}