<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\Pmb;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\MahasiswaOrangtua;
use AppBundle\Entity\PmbPeriode;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\BerkasType; 
use AppBundle\Form\Type\PmbPeriodeProdiType; 

class PmbFormType extends AbstractType
{
    protected $em;
  
    public function __construct(EntityManager $em) {
      $this->em = $em;
    }
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('jalur', EntityType::class, array(
            'required'  => false,
            'label'			=> 'Jalur PMB',
            'class'			=> Master::class,
            'query_builder'	=> function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'jalur_pmb');
            },
            'choice_label' => 'nama',
            'placeholder'	=> '-- Pilih --',
          ))
          ->add('periode', EntityType::class, array(
              'required'  => false,
              'label' => 'Program Studi',
              'class' => 'AppBundle:PmbPeriode',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.status= :status')
                      ->setParameter('status', 1);
              },
              'choice_label' => function(PmbPeriode $entity = null) {
                  return $entity->getProdi()->getJenjang()->getNama() . ' ' . $entity->getProdi()->getNamaProdi();
              },
              'placeholder' => '-- Pilih --',
          )) // prodi
          // ->add('periode', EntityType::class, array(
          //     'label' => 'Program Studi',
          //     'class' => 'AppBundle:PmbPeriode',
          //     'query_builder' => function (EntityRepository $er) {
          //         return $er->createQueryBuilder('m')
          //             ->where('m.aktif= :aktif')
          //             ->setParameter('aktif', 1);
          //     },
          //     'choice_label' => 'prodi',
          //     'placeholder' => '-- Pilih --',
          // )) // prodi
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pmb::class,
        ]);
    }
}