<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Entity\Dosen;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\BerkasType;
use AppBundle\Form\Type\PmbOrangTuaType;
use AppBundle\Service\AppService;

class DosenFormType extends AbstractType
{
    protected $em;
    protected $appService;

    public function __construct(EntityManager $em, AppService $appService) {
      $this->appService = $appService;
      $this->em = $em;
    }
  
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('nip', null, array(
              'label' => 'NIP',
              'required'  => false,
          ))
          ->add('nidn', null, array(
              'label' => 'NIDN'
          ))
          ->add('tglMulaiKerja', DateType::class, array(
              'required'  => false,
              'label' => 'Tanggal Mulai Kerja',
              'widget'=> 'single_text',
              // prevents rendering it as type="date", to avoid HTML5 date pickers
              'html5' => false,
              'format' => 'dd-MM-yyyy',
              // adds a class that can be selected in JavaScript
              'attr' => ['class' => 'js-datepicker'],
          ))
          ->add('semesterMulaiKerja', EntityType::class, array(
              'required'  => false,
              'class' => 'AppBundle:TahunAkademik',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('t');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('statusKerja', EntityType::class, array(
              'required'  => false,
              'label' => 'Status Ikatan Kerja',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'ikatan_kerja_sdm');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('pendidikanTertinggi', EntityType::class, array(
              'required'  => false,
              'label' => 'Pendidikan Tertinggi',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'bentuk_pendidikan');
              },
              'choice_label' => 'nama',
          ))
          ->add('institusiInduk', EntityType::class, array(
              'required'  => false,
              'label' => 'Institusi Induk',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'institusi_induk');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          // ->add('status', ChoiceType::class, array(
          //     'required'  => false,
          //     'label' => 'Status',
          //     'choices'   => array(
          //         'Cuti'                  => 'Cuti',
          //         'Keluar'                => 'Keluar',
          //         'Almarhum'              => 'Almarhum',
          //         'Pensiun'               => 'Pensiun',
          //         'Studi Lanjut'          => 'Studi Lanjut',
          //         'Tugas di Instansi Lain' => 'Tugas di Instansi Lain',
          //         'Aktif Mengajar'        => 'Aktif Mengajar',
          //         'Ganti MIDN'            => 'Ganti MIDN',
          //     ),
          //     'placeholder'   => '-- Pilih --',
          // ))
          ->add('jabatanAkademik', ChoiceType::class, array(
              'required'  => false,
              'label' => 'Jabatan Akademik',
              'choices'   => $this->appService->getMasterChoices('jabfung', 'nama')
          ))
          ->add('jabatanFungsional', ChoiceType::class, array(
              'required'  => false,
              'label' => 'Jabatan Fungsional',
              'choices'   => $this->appService->getMasterChoices('jabfung', 'nama')
          ))
          ->add('jabatanStruktural', null, array(
              'label'   => 'Jabatan Struktural',
              'required'  => false,
          ))
          ->add('pangkatGolongan', EntityType::class, array(
              'required'  => false,
              'label' => 'Pangkat Golongan',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'pangkat_golongan');
              },
              'choice_label' => 'nama',
          ))
          ->add('aktif', null, array(
              // 'label' => false
          ))
          ->add('namaIbuKandung', null, array(
              'label' => 'Nama Ibu Kandung'
          ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dosen::class
        ]);
    }
}