<?php

namespace AppBundle\Controller\Perpustakaan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class PustakaController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/perpustakaan/pustaka/{aksi}", name="pustaka")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
      if ( $aksi == 'edit' ) {

        if ( !empty($request->get('id')) ) {
          $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPustaka')
            ->find($request->get('id'));
        } else {
          $data = new \AppBundle\Entity\PerpusPustaka();
        }

        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add('klasifikasi', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'rak_klasifikasi');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('kode')
          ->add('isbn', null, array( 
              'label' => 'No. ISBN'
          ))
          ->add('judul')
          ->add('format', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'format_pustaka');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('lokasi', EntityType::class, array(
              'label' => 'Lokasi/RAK',
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'lokasi_rak');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('pengarang')
          ->add('penerbit')
          ->add('tptTerbit', null, array(
              'label' => 'Tempat Terbit'
          ))
          ->add('tahunTerbit', null, array(
              'label' => 'Tahun Terbit'
          ))
          ->add('tglTerima', null, array(
              'label' => 'Tanggal Terima'
          ))
          ->add('edisi')
          ->add('harga')
          ->add('jumlah')
          ->add('sumberDari')
          ->add('ket')
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
            return $this->redirectToRoute('pustaka');
        }

      } else {

        $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPustaka')
          ->findAll();

      }
      $dataKlasifikasi = $this->getDoctrine()->getRepository('AppBundle:Master')
        ->findByType('rak_klasifikasi');
      return $this->appService->load('perpustakaan/pustaka/pustaka_'.$aksi.'.html.twig', array(
          'data'            => $data,
          'dataKlasifikasi' => $dataKlasifikasi,
          'form'            => (isset($form)) ? $form->createView() : ''
      ));
    }
  
    /**
     * @Route("/perpustakaan/lokasi_rak/{aksi}", name="rak_lokasi_index")
     */
    public function rakIndexAction(Request $request, $aksi = 'index')
    {
        if ( $aksi == "edit" ) {
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:Master')
              ->find($request->get('id'));
          } else {
            $data = new \AppBundle\Entity\Master();
            $data->setType('lokasi_rak');
          }
          $builder = $this->createFormBuilder($data);
          $form = $builder
            ->add('kode')
            ->add('nama')
            ->add('ket')
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()) {
              $post = $form->getData();
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Pengaturan berhasil disimpan.');
              return $this->redirectToRoute('rak_lokasi_index');
          }
        } else {
          $data = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findByType('lokasi_rak');
        }
        return $this->appService->load('perpustakaan/pustaka/rak_lokasi_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }
    
    /**
     * @Route("/perpustakaan/klasifikasi/{aksi}", name="rak_klasifikasi_index")
     */
    public function klasifikasiIndexAction(Request $request, $aksi = 'index')
    {
        if ( $aksi == "edit" ) {
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:Master')
              ->find($request->get('id')); 
          } else {
            $data = new \AppBundle\Entity\Master();
            $data->setType('rak_klasifikasi');
          }
          $builder = $this->createFormBuilder($data);
          $form = $builder
            ->add('kode', null, array(
                'required'  => true,
            ))
            ->add('nama', null, array(
                'label' => 'Klasifikasi',
            ))
            ->add('ket', null, array(
                'label' => 'Keterangan',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Pengaturan berhasil disimpan.');
              return $this->redirectToRoute('rak_klasifikasi_index');
          }
        } else {
          $data = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findByType('rak_klasifikasi');
        }
        return $this->appService->load('perpustakaan/pustaka/rak_klasifikasi_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }

    /**
     * @Route("/perpustakaan/format_pustaka/{aksi}", name="format_pustaka_index")
     */
    public function formatPustakaIndexAction(Request $request, $aksi = 'index')
    {
        if ( $aksi == "edit" ) {
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:Master')
              ->find($request->get('id')); 
          } else {
            $data = new \AppBundle\Entity\Master();
            $data->setType('format_pustaka');
          }
            $builder = $this->createFormBuilder($data);
            $form = $builder
              ->add('kode', null, array(
                  'required'  => true,
              ))
              ->add('nama', null, array(
                  'label' => 'Format Pustaka',
              ))
              ->add('ket', null, array(
                  'label' => 'Keterangan',
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class'   => 'btn btn-primary'
                  )
              ))
              ->getForm();
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($data);
                $em->flush();
                $this->addFlash('success', 'Pengaturan berhasil disimpan.');
                return $this->redirectToRoute('format_pustaka_index');
            }
        } else {
          $data = $this->getDoctrine()->getRepository('AppBundle:Master')
            ->findByType('format_pustaka');
        }
        return $this->appService->load('perpustakaan/pustaka/format_pustaka_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }

}
