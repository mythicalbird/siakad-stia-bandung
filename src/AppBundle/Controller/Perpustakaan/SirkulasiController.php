<?php

namespace AppBundle\Controller\Perpustakaan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;

class SirkulasiController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/perpustakaan/sirkulasi/kunjungan/{aksi}", name="sirkulasi_kunjungan")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        $em = $this->getDoctrine()->getManager();
        $now = new\DateTime();
        if ($aksi == 'edit') { 
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:PerpusKunjungan')
              ->find($request->get('id'));
          } else {
            $data = new \AppBundle\Entity\PerpusKunjungan();
            $data->setTanggal($now);
          }
          $form = $this->createFormBuilder($data)
            ->add('barcode', null, array(
                'label' => 'ID Barcode Anggota'
            ))
            ->add('noIdentitas', null, array(
                'label' => 'No. Identitas'
            ))
            ->add('nama')
            ->add('jk', ChoiceType::class, array(
                'label' => 'Jenis Kelamin',
                'choices' => array(
                    'Laki-laki' => 'l',
                    'Perempuan' => 'w'
                ),
                'placeholder' => '-- PILIH --'
            ))
            ->add('jenis', ChoiceType::class, array(
                'label' => 'Jenis Pengunjung',
                'choices' => array(
                    'Dosen' => 'Dosen',
                    'Pegawai' => 'Pegawai',
                    'Mahasiswa' => 'Mahasiswa',
                    'Umum' => 'Umum'
                ),
                'placeholder' => '-- PILIH --'
            ))
            ->add('alamat', null, array(
                'label' => 'Alamat'
            ))
            ->add('telp', null, array(
                'label' => 'No. Telp/HP'
            ))
            ->add('tujuan')
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
          $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Pengaturan berhasil disimpan.');
              return $this->redirectToRoute('sirkulasi_kunjungan');
          }
        } else {
          if ( !empty($request->get('tanggal')) ) {
            $tanggal = explode('-', $request->get('tanggal'));
            $tanggal = $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0];
          } else {
            $tanggal = $now->format('Y-m-d');
          }
          $dataPerpusKunjungan = $em->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:PerpusKunjungan', 'p')
            ->where('p.tanggal between :start and :akhir')
            ->setParameters(array(
              'start' => $tanggal,
              'akhir' => $tanggal . ' 23:59:59'
            ))
            ->getQuery()
            ->getResult();
          $no = 1;
          foreach ($dataPerpusKunjungan as $data) {
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $this->response['result'][] = array(
                $no,
                $data->getTanggal()->format('d/m/Y'),
                $data->getTanggal()->format('H:i'),
                $data->getBarcode(),
                $data->getNama(),
                $data->getTujuan(),
                ''
              );
              $no++;
            } else {
              $this->response['result'][] = array(
                'id'          => $data->getId(),
                'tanggal'     => $data->getTanggal()->format('d/m/Y'),
                'jam'         => $data->getTanggal()->format('H:i'),
                'id_anggota'  => $data->getBarcode(),
                'nama_anggota'=> $data->getTujuan()
              );
            }
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $response->setData(array('data'=> $this->response['result']));
            } else {
              $response->setData($this->response);
            }
            return $response;
        } else {
            return $this->appService->load('perpustakaan/sirkulasi/kunjungan_'.$aksi.'.html.twig', array(
                'data'  => $this->response,
                'form'  => ( isset($form) ) ? $form->createView() : ''
            ));
        }
    }

    /**
     * @Route("/perpustakaan/sirkulasi/peminjaman/{aksi}", name="sirkulasi_peminjaman")
     */
    public function peminjamanIndexAction(Request $request, $aksi = 'index')
    {
        $now = new\DateTime();
        $em = $this->getDoctrine()->getManager();
        if ( $aksi == 'edit' && !empty($request->get('id')) ) {
          $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
            ->find($request->get('id'));
        } elseif ( $aksi == 'tambah' ) {
          $data = '';          
        } else {
          if ( !empty($request->get('tanggal')) ) {
            $tanggal = explode('-', $request->get('tanggal'));
            $tanggal = $tanggal[2] . "-" . $tanggal[1] . "-" . $tanggal[0];
          } else {
            $tanggal = $now->format('Y-m-d');
          }
          $dataPerpusPeminjaman = $em->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:PerpusPeminjaman', 'p')
            ->where('p.tanggal between :start and :akhir')
            ->setParameters(array(
              'start' => $tanggal,
              'akhir' => $tanggal . ' 23:59:59'
            ))
            ->getQuery()
            ->getResult();
          $no = 1;
          foreach ($dataPerpusPeminjaman as $data) {
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $this->response['result'][] = array(
                $no,
                $data->getTanggal()->format('d/m/Y'),
                $data->getTanggal()->format('H:i'),
                $data->getAnggota()->getUsername(),
                $data->getAnggota()->getNama(),
                1,
                ''
              );
              $no++;
            } else {
              $this->response['result'][] = array(
                'id'          => $data->getId(),
                'tanggal'     => $data->getTanggal()->format('d/m/Y'),
                'jam'         => $data->getTanggal()->format('H:i'),
                'id_anggota'  => $data->getAnggota()->getUsername(),
                'nama_anggota'=> $data->getAnggota()->getNama()
              );
            }
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            if ( !empty($request->get('dt')) && $request->get('dt') == "true" ) {
              $response->setData(array('data'=> $this->response['result']));
            } else {
              $response->setData($this->response);
            }
            return $response;
        } else {
            return $this->appService->load('perpustakaan/sirkulasi/peminjaman_'.$aksi.'.html.twig', array(
                // 'data'  => $data,
                'form'  => ( isset($form) ) ? $form->createView() : ''
            ));
        }
    }


    /**
     * @Route("/perpustakaan/sirkulasi/pengembalian/{aksi}", name="sirkulasi_pengembalian")
     */
    public function pengembalianIndexAction(Request $request, $aksi = 'index')
    {
        if ($aksi == 'edit') {
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
              ->find($request->get('id'));
          } else {
            $data = new \AppBundle\Entity\PerpusPeminjaman();
            $now = new\DateTime();
            $data->setTanggal($now);
          }
        } else {
          $data = $this->getDoctrine()->getRepository('AppBundle:PerpusPeminjaman')
            ->findBySudahKembali(1);
        }
        return $this->appService->load('perpustakaan/sirkulasi/pengembalian_'.$aksi.'.html.twig', array(
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : ''
        ));
    }
}
