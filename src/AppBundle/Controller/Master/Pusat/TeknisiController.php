<?php

namespace AppBundle\Controller\Master\Pusat;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Dosen;
use AppBundle\Entity\BobotNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class TeknisiController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/teknisi", name="teknisi_index")
     */
    public function indexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(BobotNilai::class)
            ->findAll();
        return $this->appService->load('MasterBundle:Default:bobot_nilai_index.html.twig', [
            'data' => $data
        ]);
    }

    /**
     * @Route("/master/teknisi/edit", name="teknisi_edit")
     */
    public function newAction(Request $request)
    {
        if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new BobotNilai();
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "hapus") {
        } elseif (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository(BobotNilai::class)->find($request->get('id'));
        } else {
            exit;
        }

        $form = $this->createFormBuilder($data)
            ->add('kode', null, array(
                'label'     => 'Kode Mata Kuliah',
            ))
            ->add('namaMakul', null, array(
                'label'     => 'Nama Mata Kuliah (ind)',
            ))
            ->add('namaMakulEng', null, array(
                'label'     => 'Nama Mata Kuliah (eng)',
            ))
            ->add('sksTeori', null, array(
                'label'     => 'SKS Teori',
            ))
            ->add('sksPraktek', null, array(
                'label'     => 'SKS Praktek',
            ))
            ->add('sksLapangan', null, array(
                'label'     => 'SKS Lapangan',
            ))
            ->add('semester', EntityType::class, array(
                'class' => Master::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.type = :type')
                        ->setParameter('type', 'semester');
                },
                'choice_label' => 'nama',
            ))
            ->add('kurikulum', EntityType::class, array(
                'class' => Kurikulum::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('k');
                },
                'choice_label' => 'nama',
            ))
            ->add('kelompok', EntityType::class, array(
                'label' => 'Kelompok Mata Kuliah',
                'class' => Master::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.type = :type')
                        ->setParameter('type', 'kelompok_makul');
                },
                'choice_label' => 'nama',
                'placeholder' => '-- PILIH --',
            ))
            ->add('jenis', EntityType::class, array(
                'label' => 'Jenis Mata Kuliah',
                'class' => Master::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.type = :type')
                        ->setParameter('type', 'jenis_makul');
                },
                'choice_label' => 'nama',
                'placeholder' => '-- PILIH --',
            ))
            ->add('kelompok', EntityType::class, array(
                'label' => 'Jenis Kelompok',
                'class' => Master::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->where('m.type = :type')
                        ->setParameter('type', 'jenis_kelompok_makul');
                },
                'choice_label' => 'nama',
                'placeholder' => '-- PILIH --',
            ))
            ->add('status', ChoiceType::class, array(
                'label'     => 'Status Mata Kuliah',
                'choices'   => array(
                    'AKTIF' => 'AKTIF',
                    'TIDAK AKTIF' => 'TIDAK AKTIF'
                ),
                'placeholder' => '-- PILIH --',
            ))
            ->add('silabus', null, array(
                'label'     => 'Ada Silabus',
            ))
            ->add('satuanAcara', null, array(
                'label'     => 'Satuan Acara Perkuliahan',
            ))
            ->add('bahanAjar', null, array(
                'label'     => 'Ada Bahan Ajar',
            ))
            ->add('diktat', null, array(
                'label'     => 'Diktat',
            ))
            ->add('nilaiMinimal', null, array(
                'label'     => 'Nilai Minimal Lulus',
            ))
            ->add('dosenPengampu', HiddenType::class)
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $dp = ltrim( rtrim($post->getDosenPengampu(), "|"), "|" );
            $data->setDosenPengampu($dp);
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('makul_index');
        }
        $dataKelas = $this->getDoctrine()->getRepository(Kelas::class)
          ->findAll();
        $dataDosen = $this->getDoctrine()->getRepository(Dosen::class)
          ->findAll();
        return $this->appService->load('MasterBundle:Default:makul_edit.html.twig', [
            'form'  => $form->createView(),
            'kelas' => $dataKelas,
            'dataDosen' => $dataDosen,
            'dosenPengampu' => $this->listDosenPengampu($data->getDosenPengampu())
        ]);
    }
}
