<?php

namespace AppBundle\Controller\Master\Civitas;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\ProgramStudi;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\User;
use AppBundle\Entity\DosenPendidikan;
use AppBundle\Entity\DosenPelatihan;
use AppBundle\Entity\Wilayah;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Type\DosenFormType;
use AppBundle\Form\Type\DosenPendidikanFormType;
use AppBundle\Form\Type\DosenPelatihanFormType;
use AppBundle\Service\AppService;
use Port\Excel\ExcelWriter;

class DosenController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/master/civitas/dosen", name="dosen_index")
     */
    public function indexAction(Request $request)
    {
        $params = array();
        if ( !empty($request->get('jenis')) ) {
          $params['statusKerja'] = $this->appService->getMasterTermObject('ikatan_kerja_sdm', $request->get('jenis'));
        }
        $dataDosen = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findBy($params);
        foreach ($dataDosen as $dosen) {
          if ( null !== $dosen->getUser() && $dosen->getUser()->getStatus() != 'trash' ) {
            $this->response['result'][] = array(
              'id'              => $dosen->getId(),
              'id_user'         => $dosen->getUser()->getId(),
              'username'        => $dosen->getUser()->getUsername(),
              'nidn'            => ( !empty($dosen->getNidn()) ) ? $dosen->getNidn() : $dosen->getUser()->getUsername(),
              'nama'            => $dosen->getUser()->getNama(),
              'email'           => $dosen->getUser()->getEmail(),
              'telp'            => $dosen->getUser()->getTelp(),
              'hp'              => $dosen->getUser()->getHp(),
              'uuid'            => $dosen->getUuid()
            ); 
          }
        }
        $dataMasterStatusIkatanKerja = $this->getDoctrine()->getRepository('AppBundle:Master')->findByType('ikatan_kerja_sdm');
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('master/civitas/dosen_index.html.twig', array(
              'data'          => $this->response,
              'jenis_dosen'   => $dataMasterStatusIkatanKerja
            ));
        }
    }

    /**
     * @Route("/master/civitas/dosen/export", name="dosen_export")
     * @Method({"POST"})
     */
    public function exportAction()
    {

        $response = new JsonResponse();


        $role = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'DOSEN']);
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
              'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 3)
          ));

        $namaFile = 'data_dosen.xlsx';
        $filePath = 'uploads/' . $namaFile;
        $path = $this->get('kernel')->getProjectDir() . '/web/' . $filePath;
        // var_dump($path); exit;
        $file = new \SplFileObject( $path, 'w');
        $writer = new ExcelWriter($file, date('Y-m-d') . 'Data Dosen');

        $writer->prepare();
        $writer->writeItem(['No', 'NIDN', 'Nama', 'Pendidikan', 'Tempat, Tanggal Lahir', 'Agama', 'Jenis Kelamin', 'Asal Daerah', 'Tanggal Masuk']);
        
        $no = 1;
        foreach ($data as $user) {
          $ttl = $user->getTptLahir();
          $tglLahir = ( null !== $user->getTglLahir() ) ? $user->getTglLahir() : '';
          $ttl .= ( $tglLahir != '' ) ? ', ' . $tglLahir->format('d F Y') : '';
          $jk = ( $user->getJk() != '' ) ? ( $user->getJk() == 'l' ) ? 'LAKI-LAKI' : 'PEREMPUAN' : '';
          $writer->writeItem([
            'No'                        => $no, 
            'NIDN'                      => $user->getUsername(),
            'Nama'                      => $user->getNama(),
            'Pendidikan'                => '',
            'Tempat, Tanggal Lahir'     => $ttl,
            'Agama'                     => ( null !== $user->getAgama() ) ? $user->getAgama()->getNama() : '',
            'Jenis Kelamin'             => $jk,
            'Asal Daerah'               => $user->getTptLahir(),
            'Tanggal Masuk'             => ( null !== $user->getTglMasuk() ) ? $user->getTglMasuk()->format('d F Y') : '',
          ]);
          $no++;
        }

        $writer->finish();

        $manager = $this->get('assets.packages');
        $url = $manager->getUrl($filePath);
        $response->setData($url);
        return $response;
    }

    /**
     * @Route("/master/civitas/dosen/tambah", name="dosen_new")
     */
    public function newAction(Request $request)
    {
        if( $request->get('aksi') == 'tambah' ) {
          $data = new User();
        } else {
          $data = $this->getDoctrine()->getRepository(User::class)->find($request->get('id'));
        }
        $role = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'DOSEN']);
        $data->setHakAkses($role);
        $data->setProdi($this->getUser()->getProdi());
        $builder = $this->createFormBuilder($data);
        $builder
          ->add('username', null, array(
              'required'  => false,
              'label'     => 'NIDN'
          ))
          ->add('nama', null, array(
              'required'  => true,
              'label' => 'Nama Dosen',
              'attr'  => array(
                'readonly'  => ( !empty($request->get('aksi')) && $request->get('aksi') == 'tambah' ) ? false : true
              )
          ))
          ->add('gelarDepan', null, array(
              'label' => 'Gelar Depan'
          ))
          ->add('gelarBelakang', null, array(
              'label' => 'Gelar Belakang'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'required'    => false,
              'label' => 'Tanggal Lahir',
              'widget'=> 'single_text',
              // prevents rendering it as type="date", to avoid HTML5 date pickers
              'html5' => false,
              'format' => 'dd-MM-yyyy',
              // adds a class that can be selected in JavaScript
              'attr' => ['class' => 'js-datepicker'],
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --',
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('ktp', null, array(
              'required'  => false, 
              'label'     => 'Nomor KTP',
          ))
          ->add('telp', null, array(
              'label' => 'No. Telpon'
          ))
          ->add('hp', null, array(
              'label' => 'Nomor HP'
          ))
          ->add('email', EmailType::class)
          // ->add('pendidikan')
          ->add('statusSipil', ChoiceType::class, array(
            'label'   => 'Status',
            'choices' => array(
              'MENIKAH' => 1,
              'BELUM MENIKAH' => 0,
            ),
            'placeholder'   => '-- Pilih --'
          ))
          // ->add('prodi', EntityType::class, array(
          //     'label'     => 'Program Studi',
          //     'class'     => 'AppBundle:ProgramStudi',
          //     'query_builder' => function (EntityRepository $er) {
          //         return $er->createQueryBuilder('p');
          //     },
          //     'attr'        => array( 'class' => 'form-control select2' ),
          //     'choice_label' => 'namaProdi',
          //     'placeholder' => '-- Pilih Prodi --',
          // ))
        ;

        if ( !empty($request->get('id')) ) {
            $builder
                  ->add('alamat', null, array(
                      'required'  => false,
                  ))
                  ->add('dusun')
                  ->add('desa', null, array(
                    'required'  => true,
                    'label'     => 'Desa/Kel.'
                  ))
                  ->add('wilayah', EntityType::class, array(
                      'label' => 'Kec/Kab/Pro',
                      'class' => 'AppBundle:Wilayah',
                      'query_builder' => function (EntityRepository $er) {
                          return $er->createQueryBuilder('w')
                              ->where('w.level= :level')
                              ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
                      },
                      'choice_label' => function( Wilayah $entity = null ){
                        $choice_label = $entity->getNama();
                        if ( null !== $entity->getParent() ) {
                          $parent = $entity->getParent();
                          $choice_label .= ', ' . $parent->getNama();
                          if ( null !== $parent->getParent() ) {
                            $choice_label .= ', ' . $parent->getParent()->getNama();
                          }
                        }
                        return $choice_label;
                      },
                      'choice_value' => function( Wilayah $entity = null ){
                        return ( null !== $entity ) ? $entity->getId() : null;
                      },
                      'attr'  => array(
                        'style' => 'width:100%'
                      ),
                      'placeholder' => '-- Pilih --',
                  )) 
                  ->add('pos', null, array(
                      'required'  => false,
                      'label'     => 'Kodepos'
                  ))
                  ->add('dataDosen', DosenFormType::class)
                ;
        }
        $builder->add('submit', SubmitType::class, array(
            'label' => 'Simpan',
            'attr'  => array(
                'class' => 'btn btn-primary'
            )
        ));

        $form = $builder->getForm();
        $form->handleRequest($request);
        if( $request->get('aksi') == 'tambah' ) {
          $template = 'master/civitas/dosen_tambah.html.twig';
          $target = 'alamat';
        } else {
          $template = 'master/civitas/dosen_edit.html.twig';
          $target = 'identitas';
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ( $request->get('aksi') == "tambah" ) {
              if ( !empty($data->getUsername()) ) {
                $nidn = $data->getUsername();
                $username = $data->getUsername();
              } else {
                $nidn = '';
                $user = true;
                while ( $user ) {
                  $username = random_int(10000, 999999);
                  $user = $em->getRepository('AppBundle:User')
                    ->findOneByUsername($nidn);
                }
                $username = 'D' . $username;
                $data->setUsername($username);
              }
              $data->setPassword($username);
              $dosen = new \AppBundle\Entity\Dosen();
              $dosen->setNidn($nidn);
              $data->setDataDosen($dosen);
            }
            $dosen = $data->getDataDosen();
            $em->persist($dosen);

            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('dosen_edit', array('id' => $data->getId(), 'target' => $target));
        }


        $_pendidikan = new DosenPendidikan();
        $f_pendidikan = $this->createForm(DosenPendidikanFormType::class, $_pendidikan);
        $f_pendidikan->handleRequest($request);

        if ($f_pendidikan->isSubmitted()) {
          $post = $f_pendidikan->getData();
          $em = $this->getDoctrine()->getManager();
          $dosen = $em->getRepository('AppBundle:User')->find($post->getDosen());
          $_pendidikan->setDosen($dosen);
          $em->persist($_pendidikan);
          $em->flush();
          $response = new JsonResponse();
          $response->setData('success');
          return $response;
        }

        $_pelatihan = new DosenPelatihan();
        $f_pelatihan = $this->createForm(DosenPelatihanFormType::class, $_pelatihan);
        $f_pelatihan->handleRequest($request);
        
        if ($f_pelatihan->isSubmitted()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($_pelatihan);
          $em->flush();
          $response = new JsonResponse();
          $response->setData('success');
          return $response;
        }

        return $this->appService->load($template, array(
            'data' => $data,
            'form' => $form->createView(),
            'formPendidikan' => $f_pendidikan->createView(),
            'formPelatihan' => $f_pelatihan->createView(),
        ));
    }

    /**
     * @Route("/master/civitas/dosen/edit/{id}", name="dosen_edit")
     */
    public function editAction(Request $request, $id)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($id);
        if ( null === $data->getProdi() ) {
          $data->setProdi($this->getUser()->getProdi());
        }
        $builder = $this->createFormBuilder($data);
        $builder
          // ->add('username', null, array(
          //     'required'  => false,
          //     'label'     => 'NIDN'
          // ))
          ->add('gelarDepan', null, array(
              'label' => 'Gelar Depan'
          ))
          ->add('gelarBelakang', null, array(
              'label' => 'Gelar Belakang'
          ))
          ->add('tptLahir', null, array(
              'label' => 'Tempat Lahir'
          ))
          ->add('tglLahir', DateType::class, array(
              'required'    => false,
              'label' => 'Tanggal Lahir',
              'widget'=> 'single_text',
              // prevents rendering it as type="date", to avoid HTML5 date pickers
              'html5' => false,
              'format' => 'dd-MM-yyyy',
              // adds a class that can be selected in JavaScript
              'attr' => ['class' => 'js-datepicker'],
          ))
          ->add('jk', ChoiceType::class, array(
              'label' => 'Jenis Kelamin',
              'choices'   => array(
                  'Laki-laki' => 'L',
                  'Perempuan' => 'P'
              ),
              'placeholder'   => '-- Pilih --',
          ))
          ->add('agama', EntityType::class, array(
              'class' => 'AppBundle:Master',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('m')
                      ->where('m.type= :type')
                      ->setParameter('type', 'agama');
              },
              'choice_label' => 'nama',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('ktp', null, array(
              'required'  => false, 
              'label'     => 'Nomor KTP',
          ))
          ->add('telp', null, array(
              'label' => 'No. Telpon'
          ))
          ->add('hp', null, array(
              'label' => 'Nomor HP'
          ))
          ->add('email', EmailType::class)
          // ->add('pendidikan')
          ->add('statusSipil', ChoiceType::class, array(
            'label'   => 'Status',
            'choices' => array(
              'MENIKAH' => 1,
              'BELUM MENIKAH' => 0,
            ),
            'placeholder'   => '-- Pilih --'
          ))
          // ->add('prodi', EntityType::class, array(
          //     'label'     => 'Program Studi',
          //     'class'     => 'AppBundle:ProgramStudi',
          //     'query_builder' => function (EntityRepository $er) {
          //         return $er->createQueryBuilder('p');
          //     },
          //     'attr'        => array( 'class' => 'form-control select2' ),
          //     'choice_label' => 'namaProdi',
          //     'placeholder' => '-- Pilih Prodi --',
          // ))
        ;

        if ( !empty($request->get('id')) ) {
            $builder
                  ->add('alamat', null, array(
                      'required'  => false,
                  ))
                  ->add('dusun')
                  ->add('desa', null, array(
                    'required'  => true,
                    'label'     => 'Desa/Kel.'
                  ))
                  ->add('wilayah', EntityType::class, array(
                      'label' => 'Kec/Kab/Pro',
                      'class' => 'AppBundle:Wilayah',
                      'query_builder' => function (EntityRepository $er) {
                          return $er->createQueryBuilder('w')
                              ->where('w.level= :level')
                              ->setParameter('level', $this->appService->getMasterTermObject('level_wilayah', 3));
                      },
                      'choice_label' => function( Wilayah $entity = null ){
                        $choice_label = $entity->getNama();
                        if ( null !== $entity->getParent() ) {
                          $parent = $entity->getParent();
                          $choice_label .= ', ' . $parent->getNama();
                          if ( null !== $parent->getParent() ) {
                            $choice_label .= ', ' . $parent->getParent()->getNama();
                          }
                        }
                        return $choice_label;
                      },
                      'choice_value' => function( Wilayah $entity = null ){
                        return ( null !== $entity ) ? $entity->getId() : null;
                      },
                      'attr'  => array(
                        'style' => 'width:100%'
                      ),
                      'placeholder' => '-- Pilih --',
                  )) 
                  ->add('pos', null, array(
                      'required'  => false,
                      'label'     => 'Kodepos'
                  ))
                  ->add('dataDosen', DosenFormType::class)
                ;
        }
        $builder->add('submit', SubmitType::class, array(
            'label' => 'Simpan',
            'attr'  => array(
                'class' => 'btn btn-primary'
            )
        ));

        $form = $builder->getForm();
        $form->handleRequest($request);
        if( $request->get('aksi') == 'tambah' ) {
          $template = 'master/civitas/dosen_tambah.html.twig';
          $target = 'alamat';
        } else {
          $template = 'master/civitas/dosen_edit.html.twig';
          $target = 'identitas';
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ( $request->get('aksi') == "tambah" ) {
              if ( !empty($data->getUsername()) ) {
                $nidn = $data->getUsername();
                $username = $data->getUsername();
              } else {
                $nidn = '';
                $user = true;
                while ( $user ) {
                  $username = random_int(10000, 999999);
                  $user = $em->getRepository('AppBundle:User')
                    ->findOneByUsername($nidn);
                }
                $username = 'D' . $username;
                $data->setUsername($username);
              }
              $data->setPassword($username);
              $dosen = new \AppBundle\Entity\Dosen();
              $dosen->setNidn($nidn);
              $data->setDataDosen($dosen);
            }
            $dosen = $data->getDataDosen();
            $em->persist($dosen);

            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('dosen_edit', array('id' => $data->getId(), 'target' => $target));
        }


        $_pendidikan = new DosenPendidikan();
        $f_pendidikan = $this->createForm(DosenPendidikanFormType::class, $_pendidikan);
        $f_pendidikan->handleRequest($request);

        if ($f_pendidikan->isSubmitted()) {
          $post = $f_pendidikan->getData();
          $em = $this->getDoctrine()->getManager();
          $dosen = $em->getRepository('AppBundle:User')->find($post->getDosen());
          $_pendidikan->setDosen($dosen);
          $em->persist($_pendidikan);
          $em->flush();
          $response = new JsonResponse();
          $response->setData('success');
          return $response;
        }

        $_pelatihan = new DosenPelatihan();
        $f_pelatihan = $this->createForm(DosenPelatihanFormType::class, $_pelatihan);
        $f_pelatihan->handleRequest($request);
        
        if ($f_pelatihan->isSubmitted()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($_pelatihan);
          $em->flush();
          $response = new JsonResponse();
          $response->setData('success');
          return $response;
        }

        return $this->appService->load($template, array(
            'data' => $data,
            'form' => $form->createView(),
            'formPendidikan' => $f_pendidikan->createView(),
            'formPelatihan' => $f_pelatihan->createView(),
        ));
    }
  
    /**
     * @Route("/master/civitas/dosen/cetak", name="dosen_cetak")
     */
    public function cetakAction(Request $request)
    { 
        $data = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        return $this->appService->load('cetak/dosen_data.html.twig', array(
            'data' => $data
        ));
    }

    /**
     * @Route("/master/civitas/dosen/hapus/{nidn}", name="dosen_hapus")
     */
    public function hapusAction(Request $request, $nidn)
    { 
        $this->appService->hapusDosen($nidn);
        return $this->redirectToRoute('dosen_index');
    }
}
