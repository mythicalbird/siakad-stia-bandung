<?php

namespace AppBundle\Controller\Master\Akademik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Inventaris;
use AppBundle\Entity\Master;
use AppBundle\Entity\Lokasi;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class InventarisController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/inventaris", name="inventaris_index")
     */
    public function indexAction(Request $request)
    {
        $lokasiList = $this->getDoctrine()->getRepository(Lokasi::class)->findAll();

        // $data = $this->getDoctrine()->getRepository(Inventaris::class)->findAll();
        return $this->appService->load('MasterBundle:Default:inventaris_index.html.twig', array(
        	'lokasiList' => $lokasiList,
        ));
    }

    /**
     * @Route("/master/inventaris/list", name="inventaris_list")
     */
    public function listAction(Request $request)
    {
        $lokasi = $this->getDoctrine()->getRepository(Lokasi::class)->find($request->get('lokasi'));
        $data = $this->getDoctrine()->getRepository(Inventaris::class)->findByLokasi($lokasi);
        return $this->appService->load('MasterBundle:Default:inventaris_list.html.twig', array(
            'data' => $data,
            'lokasi'    => $lokasi
        ));
    }

    /**
     * @Route("/master/inventaris/edit", name="inventaris_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new Inventaris();
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "hapus") {
        } elseif (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository(Inventaris::class)->find($request->get('id'));
        } else {
            exit;
        }
        $form = $this->createFormBuilder($data)
            ->add('kodeBarang', null, array(
                'label' => 'No. Kode',
            ))
            ->add('namaBarang')
            ->add('asal')
            ->add('kepemilikan')
            ->add('type')
            ->add('merk')
            ->add('hargaBeli')
            ->add('tglBeli', null, array(
                'label' => 'Tanggal Pembelian'
            ))
            ->add('jadwalRawat', null, array(
                'label' => 'Jadwal Perawatan'
            ))
            ->add('umurPakai')
            ->add('lokasi', EntityType::class, array(
                'class' => Lokasi::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m');
                },
                'choice_label' => 'namaLokasi',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('inventaris_index');
        }
        return $this->appService->load('MasterBundle:Default:inventaris_edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/master/inventaris/lokasi", name="inventaris_lokasi_index")
     */
    public function lokasiIndexAction(Request $request)
    {
        $lokasiList = $this->getDoctrine()->getRepository(Lokasi::class)->findAll();
        $lokasi = new Lokasi();
        $form = $this->createFormBuilder($lokasi)
            ->add('namaLokasi', TextType::class, array(
                'label' => 'Tambah Lokasi',
                'attr'  => array(
                    'placeholder' => 'Nama Lokasi',
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan'
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($lokasi);
            $em->flush();
            $this->addFlash('success', 'Lokasi berhasil ditambahkan.');
            return $this->redirectToRoute('inventaris_lokasi_index');
        }
        return $this->appService->load('MasterBundle:Default:inventaris_lokasi.html.twig', [
            'lokasiList'    => $lokasiList,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * @Route("/master/inventaris/jadwal", name="inventaris_jadwal")
     */
    public function jadwalAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Inventaris::class)->findAll();
        return $this->appService->load('MasterBundle:Default:inventaris_jadwal.html.twig', array(
            'data' => $data,
        ));
    }
}
