<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class JamKuliahController extends Controller
{
    private $appService;
  
    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/master/jam_kuliah", name="jam_kuliah_index")
     */
    public function indexAction(Request $request, AppService $appService)
    {
        $currentProdiObj = $appService->getCurrentProdi($this->getUser()->getId());
        $data = $this->getDoctrine()->getRepository('AppBundle:JamKuliah')
            ->findByProdi($this->getUser()->getProdi());
        return $this->appService->load('master/akademik/jam_kuliah_index.html.twig', [
            'data' => $data
        ]);
    }

    /**
     * @Route("/master/jam_kuliah/edit", name="jam_kuliah_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new \AppBundle\Entity\JamKuliah();
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "hapus") {
        } elseif (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository('AppBundle:JamKuliah')->find($request->get('id'));
        } else {
            exit;
        }
        if ( null !== $this->getUser()->getProdi() ) {
            $data->setProdi($this->getUser()->getProdi());
        }
        $jam = array();
        for($i = 1; $i < 25; $i++) {
          $jam[$i] = $i;
        }
        $form = $this->createFormBuilder($data)
            ->add('prodi', EntityType::class, array(
                'class' => 'AppBundle:ProgramStudi',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p');
                },
                'choice_label' => 'namaProdi',
            ))
            ->add('jam', ChoiceType::class, array(
                'label'     => 'Jam ke',
                'choices'   => $jam,
                'placeholder' => '-- Jam --'
            ))
            ->add('dari')
            ->add('sampai')
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array('class' => 'btn btn-primary')
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('jam_kuliah_index');
        }
        return $this->appService->load('master/akademik/jam_kuliah_edit.html.twig', [
            'form'  => $form->createView()
        ]);
    }

}
