<?php

namespace AppBundle\Controller\Master\Akademik;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use AppBundle\Entity\BobotNilai;
use AppBundle\Entity\BatasSks;
use AppBundle\Entity\Predikat;
use AppBundle\Entity\ProdiKurikulum;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class BobotNilaiController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/master/akademik/bobot_nilai/{aksi}", name="bobot_nilai")
     */
    public function indexAction(Request $request, $aksi = 'index')
    {
        if($aksi == 'edit') {
          if (!empty($request->get('id'))) {
              $data = $this->getDoctrine()->getRepository('AppBundle:BobotNilai')
                ->find($request->get('id'));
          } else {
              $data = new BobotNilai();
          }
          if (!$data) {
              throw $this->createNotFoundException();
          }
          if ( null !== $this->getUser()->getProdi() ) {
            $data->setProdi($this->getUser()->getProdi());
          }
          $builder = $this->createFormBuilder($data); 
          if ( !empty($request->get('prodi')) && $request->get('prodi') == "true" ) {
            $builder
                ->add('prodi', EntityType::class, array(
                  'label'     => 'Program Studi',
                  'class'     => 'AppBundle:ProgramStudi',
                  'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('p');
                  },
                  'choice_label' => 'namaProdi',
                  'placeholder' => '-- Pilih --',
                ))
            ;
          }
          $builder
              ->add('predikat', null, array(
                  'label'     => 'Nilai Huruf',
              ))
              ->add('bobot', null, array(
                  'label'     => 'Bobot Nilai',
              ))
              ->add('min', null, array(
                  'label'     => 'Nilai Minimal',
              ))
              ->add('max', null, array(
                  'label'     => 'Nilai Maximal',
              ))
              ->add('lulus', ChoiceType::class, array(
                  'label'     => 'Lulus',
                  'choices'   => array(
                      'YA' => 1,
                      'TIDAK' => 0
                  ),
                  'placeholder' => '-- PILIH --',
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
          ;
          $form = $builder->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $post = $form->getData();
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('bobot_nilai');
          }
        } else {
           $data = $this->getDoctrine()->getRepository('AppBundle:BobotNilai')
              ->findByGlobal(1);
        }
        return $this->appService->load('master/akademik/bobot_nilai_'.$aksi.'.html.twig', [
            'data'  => $data,
            'form'  => ( isset($form) ) ? $form->createView() : null
        ]);
    }

}
