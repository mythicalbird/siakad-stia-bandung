<?php

namespace AppBundle\Controller\Grafik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class IpController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/grafik/ip", name="grafik_ip")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $chartData = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Presensi Dosen',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );
        $presensiLabels = ['alpa', 'sakit', 'ijin', 'absen'];
        for ( $i = 0; $i < count($presensiLabels); $i++ ) {
            $count = 0;
            $presensi = $em->getRepository('AppBundle:Presensi')
                ->findByKet($presensiLabels[$i]);
            $count += count($presensi);

            $chartData['rows'][] = array( ucfirst($presensiLabels[$i]), $count );
        }
        return $this->appService->load('GrafikBundle:Default:grafik_presensi_index.html.twig', array(
        	'chartData'	=> $chartData,
        ));
    }
}
