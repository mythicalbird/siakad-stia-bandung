<?php

namespace AppBundle\Controller\Grafik;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PresensiController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/grafik/presensi/mahasiswa", name="grafik_presensi")
     */
    public function indexAction(Request $request, $role = 'dosen')
    {
        $em = $this->getDoctrine()->getManager();
        $this->response['result'] = array(
            'rows'      => array(),
            'options'   => array(
                    'title' => 'Program Studi Mahasiswa',
                    // 'width' => 400,
                    // 'height'=> 300
            )
        );
        $presensiLabels = ['alpa', 'sakit', 'ijin', 'absen'];
        for ( $i = 0; $i < count($presensiLabels); $i++ ) {
            $count = 0;
            $presensi = $em->getRepository('AppBundle:PresensiMahasiswa')
                ->findByKet($presensiLabels[$i]);
            $count += count($presensi);

            $this->response['result']['rows'][] = array( ucfirst($presensiLabels[$i]), $count );
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('grafik/presensi/mahasiswa.html.twig', array(
                'data'  => $this->response
            ));
        }
    }
}
