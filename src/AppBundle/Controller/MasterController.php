<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Service\AppService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MasterController extends Controller
{

    private $router;

    public function __construct(UrlGeneratorInterface $router, AppService $appService)
    {
        $this->router = $router;
        $this->appService = $appService;
    }

    /**
     * @Route("/master/term/{type}", name="term_index")
     */
    public function indexAction(Request $request, $type = "semester")
    {
        $em = $this->getDoctrine()->getManager();
        $masterType = ucwords(str_replace('_', ' ', $type));
        $master = new \AppBundle\Entity\Master();
        $master->setType($type);
        $master->setLocked(0);
        $master->setStatus('publish');
        $em = $this->getDoctrine()->getManager();
        if ($type == 'provinsi') {
            // cek provinsi sudah diinput apa belum
            $dataProvinsi = $em->getRepository(Master::class)->findByType('provinsi');
            if (count($dataProvinsi) < 1) {
                $provinsiUrl = $this->generateUrl('dashboard', array(), UrlGeneratorInterface::ABSOLUTE_URL) . 'provinsi.txt';
                $provinsiList = file_get_contents($provinsiUrl);
                $provinsiList = json_decode($provinsiList, true);
                foreach ($provinsiList['provinsi']['records'] as $provinsi) {
                    $masterProv = new Master();
                    $masterProv->setType($type);
                    $masterProv->setNama($provinsi[1]);
                    $masterProv->setSlug($provinsi[0]);
                    $em->persist($masterProv);
                    $em->flush();
                }
            }
        } elseif ($type == 'kota') {
            // cek kota sudah diinput apa belum
            $dataKota = $em->getRepository(Master::class)->findByType('kota');
            if (count($dataKota) < 1) {
                $kotaUrl = $this->generateUrl('dashboard', array(), UrlGeneratorInterface::ABSOLUTE_URL) . 'kota.txt';
                $kotaList = file_get_contents($kotaUrl);
                $kotaList = json_decode($kotaList, true);
                foreach ($kotaList['kota']['records'] as $kota) {
                    $masterKota = new Master();
                    $masterKota->setType($type);
                    $masterKota->setNama($kota[2].' '.$kota[3]);
                    $masterKota->setSlug($kota[0].'_'.$kota[1]);
                    $em->persist($masterKota);
                    $em->flush();
                }
            }
        }

        $form = $this->createFormBuilder($master)
            ->add('kode', TextType::class, array(
                'required'  => false,
                'label'     => 'Kode',
                'attr'      => array(
                      'placeholder' => '(Optional)',
                )
            ))
            ->add('nama', TextType::class, array(
                'label' => 'Tambah ' . $masterType,
                'attr'  => array(
                    'placeholder' => 'Nama ' . $masterType,
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em->persist($master);
            $em->flush();
            $this->addFlash('success', $masterType . ' berhasil ditambahkan.');
            return $this->redirectToRoute('term_index', array('type' => $type));
        }
        $masterList = $em->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Master', 'm')
            ->where('m.type=:type AND m.status!=:status')
            ->setParameters(array(
              'type'      => $type,
              'status'    => 'trash'
            ))
            ->getQuery()
            ->getResult();
        return $this->appService->load('default/term_index.html.twig', [
            'masterList'    => $masterList,
            'form'          => $form->createView(),
            'masterType'    => $masterType
        ]);
    }

    /**
     * @Route("/master/{type}/hapus", name="master_delete")
     * @Method({"POST"})
     */
    public function deleteAction(Request $request, $type = "semester")
    {
		$em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Master::class)->find($request->get('id'));
        if ( $data ) {
            $data->setStatus('trash');
            $em->persist($data);
			$em->flush();
			$this->addFlash("success", "Data berhasil dihapus");
        }
        return $this->redirectToRoute("term_index", array("type" => $type));
    }

    /**
     * @Route("/master/term/update/{field}", name="term_update")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $field = '')
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Master')->find($request->get('pk'));
        if ( $data && $field != '' ) {
            if ($field == 'kode') {
                $data->setKode($request->get('value'));
            } elseif ($field == 'nama') {
                $data->setNama($request->get('value'));
            }
            $em->persist($data);
            $em->flush();
            $response->setData(array(
                'id' => $request->get('pk'),
                'success' => 1
            ));
        }
        return $response;
    }
}
