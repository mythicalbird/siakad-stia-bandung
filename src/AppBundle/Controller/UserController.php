<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\User;
use AppBundle\Entity\Master;
use AppBundle\Service\AppService;
use AppBundle\Form\GantiPasswordType;
use AppBundle\Form\Model\GantiPassword;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UserController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/admin/user/{aksi}", name="admin_user")
     */
    public function indexAction(Request $request, $aksi = 'index', UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        if ( $aksi == 'edit' ) {
          
          if ( !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:User')
                ->find($request->get('id'));
          } else {
            $data = new \AppBundle\Entity\User();
          }

        } else {
          $data = new \AppBundle\Entity\User();
          $dataUser = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.hakAkses!=:role_dosen AND u.hakAkses!=:role_mahasiswa AND u.hakAkses!=:role_superadmin AND u.status=:status')
            ->setParameters(array(
              'role_dosen'        => $this->appService->getMasterTermObject('hak_akses', 3),
              'role_mahasiswa'    => $this->appService->getMasterTermObject('hak_akses', 4),
              'role_superadmin'   => $this->appService->getMasterTermObject('hak_akses', 1),
              'status'            => 'active'
            ))
            ->getQuery()
            ->getResult();
          foreach ($dataUser as $u) {
            $this->response['result'][] = array(
              'id'        => $u->getId(),
              'nama'      => $u->getNama(),
              'username'  => $u->getUsername(),
              'email'     => $u->getEmail(),
              'hak_akses' => $u->getHakAkses()->getNama()
            );
          }
        }

        $builder = $this->createFormBuilder($data);
        $builder 
          ->add('username')
          ->add('hakAkses', EntityType::class, array(
            'label'     => 'Hak Akses',
            'class'     => 'AppBundle:Master',
            'query_builder' => function(EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                          ->where('m.type=:type')
                          ->setParameter('type', 'hak_akses');
            },
            'choice_label' => 'nama',
            'placeholder' => '-- Pilih --',
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
               'attr' => array(
                  'class'   => 'btn btn-primary'
               )
          ))
        ;

        if ( $aksi == 'index' ) {
            $builder
              ->add('password', RepeatedType::class, array(
                  'type' => PasswordType::class,
                  'invalid_message' => 'The password fields must match.',
                  'options' => array('attr' => array('class' => 'password-field')),
                  'required' => true,
                  'first_options'  => array('label' => 'Kata Sandi Baru'),
                  'second_options' => array('label' => 'Ulangi Kata Sandi'),
              ))
            ;
        }
          
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
          $post = $form->getData();
          $em = $this->getDoctrine()->getManager();

          if ( $aksi == 'index' ) {
            $plainPassword = $data->getPassword();
            $encoded = $encoder->encodePassword($data, $plainPassword);

            $data->setPassword($encoded);
          }

          $em->persist($data);
          $em->flush();
          $this->addFlash('success', 'Data berhasil disimpan');
          return $this->redirectToRoute('admin_user');
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
          $params = array(
            'form'      => $form->createView(),
            'data'      => $data,
            'dataUser'  => isset($dataUser) ? $dataUser : null
          );
          if ( $aksi == 'index' ) {
            $params['data'] = $this->response;
          }
          return $this->appService->load('default/admin_user_'.$aksi.'.html.twig', $params);
        }
    }

    /**
     * @Route("/admin/level_user/{aksi}", name="level_index")
     */
    public function levelIndexAction(Request $request, $aksi = 'index', UserPasswordEncoderInterface $encoder)
    {
        $type = 'hak_akses';
        $em = $this->getDoctrine()->getManager();
        $masterType = ucwords(str_replace('_', ' ', $type));
        $master = new Master();
        $master->setType($type);
        $em = $this->getDoctrine()->getManager();


        $form = $this->createFormBuilder($master)
            ->add('kode', null, array(
                'required'  => false,
                'label'     => 'Kode',
                'attr'      => array(
                      'placeholder' => '(Optional)',
                )
            ))
            ->add('nama', null, array(
                'label' => 'Tambah ' . $masterType,
                'attr'  => array(
                    'placeholder' => 'Nama ' . $masterType,
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em->persist($master);
            $em->flush();
            $this->addFlash('success', $masterType . ' berhasil ditambahkan.');
            return $this->redirectToRoute('level_index');
        }
        $masterList = $em->getRepository(Master::class)
            ->findByType($type);
        return $this->appService->load('default/admin_level_user_index.html.twig', [
            'masterList'    => $masterList,
            'form'          => $form->createView(),
            'masterType'    => $masterType
        ]);
    }

    /**
     * @Route("/admin/hapus_user/{id}", name="admin_user_hapus")
     */
    public function hapusAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')
          ->find($id);
        if ( $user ) {
          if ( in_array('ROLE_DOSEN', $user->getRoles()) ) {
            // remove presensi
            $presensi = $em->getRepository('AppBundle:PresensiDosen')
              ->findByUser($user);
            if ($presensi) {
              foreach ($presensi as $data) {
                $em->remove($data);
              }
            }
            if ( $user->getDataDosen() ) {
              // remove dosen pengampu
              $dosenPengampu = $em->getRepository('AppBundle:DosenPengampu')
                ->findByDosen($user->getDataDosen());
              if ($dosenPengampu) {
                foreach ($dosenPengampu as $dp) {
                  $em->remove($dp);
                }
              }
              // remove dosen pa
              $dosenPa = $em->getRepository('AppBundle:Mahasiswa')
                ->findByPa($user->getDataDosen());
              if ($dosenPa) {
                foreach ($dosenPa as $dpa) {
                  $dpa->setPa(null);
                  $em->persist($dpa);
                }
              }
              $em->remove($user->getDataDosen());
            }
          } elseif ( in_array('ROLE_MAHASISWA', $user->getRoles()) ) {
            // remove presensi
            $presensi = $em->getRepository('AppBundle:PresensiMahasiswa')
              ->findByUser($user);
            if ($presensi) {
              foreach ($presensi as $data) {
                $em->remove($data);
              }
            }
            if ( $user->getDataMahasiswa() ) {
              $em->remove($user->getDataMahasiswa());
            }
          }
          $em->remove($user);
          $em->flush();
        }
        $this->addFlash('success', 'User berhasil dihapus.');
        return $this->redirectToRoute('admin_user');
    }

    /**
     * @Route("/profil", name="profil_index")
     */
    public function profileAction(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $data = $this->getUser();

        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add('imageFile', VichFileType::class, [
              'label' => false,
              'required' => false,
          ])
          ->add('username', null, array(
              'disabled'  => true
          ))
          ->add('nama', null, array(
              'attr'  => array(
                  'readonly'  => 'readonly'
              )
          ))
          ->add('email')
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Profil berhasil diperbarui.');
            return $this->redirectToRoute('profil_index');
        }


        return $this->render('default/profil_index.html.twig', [
            'form' => $form->createView(),
            // 'passForm'  => $passForm->createView()
        ]);
    }

    /**
     * @Route("/profil/password", name="password_index")
     */
    public function passFormIndexAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $data = $this->getUser();

        $builder = $this->createFormBuilder($data);
      
        // if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
        //   $form = $this->adminForm($data);
        // } else {
        //   $form = $this->mahasiswaForm($data);
        // }
        $builder
          ->add('password', RepeatedType::class, array(
              'type' => PasswordType::class,
              'invalid_message' => 'The password fields must match.',
              'options' => array('attr' => array('class' => 'password-field')),
              'required' => true,
              'first_options'  => array('label' => 'Kata Sandi Baru'),
              'second_options' => array('label' => 'Ulangi Kata Sandi'),
          ))
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ));

        $form = $builder->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $plainPassword = $data->getPassword();
            $encoded = $encoder->encodePassword($data, $plainPassword);

            $data->setPassword($encoded);
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Profil berhasil diperbarui.');
        }

        return $this->render('default/profil_password.html.twig', [
            'form' => $form->createView(),
            // 'passForm'  => $passForm->createView()
        ]);
    }


    /**
     * @Route("/resetpass", name="resetpass")
     */
  public function resetPassAction(Request $request, \Swift_Mailer $mailer)
  {
      $form = $this->createFormBuilder()
        ->add('email', null, array(
            'label' => false,
        ))
        ->add('submit', SubmitType::class, array(
            'label' => 'Kirim',
            'attr'  => array(
                'class'   => 'btn btn-primary'
            )
        ))
        ->getForm();
      $form->handleRequest($request);
      if ( $form->isSubmitted() ) {
        $data = $form->getData();

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo(trim($data['email']))
            ->setBody(
                $this->renderView(
                    // templates/emails/registration.html.twig
                    'emails/reset_password.html.twig',
                    array('name' => trim($data['email']))
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $mailer->send($message);

        $this->addFlash('success', 'Email berhasil dikirim...');
        return $this->redirectToRoute('resetpass');
      }
      return $this->render('default/reset_password.html.twig', array(
          'form' => $form->createView()
      ));
  }

}
