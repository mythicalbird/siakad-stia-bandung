<?php

namespace AppBundle\Controller\Akademika\HasilStudi;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class SkkController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/akademika/hasil_studi/skk/{aksi}", name="hs_skk")
     */
    public function indexAction(Request $request, $aksi = "index")
    {
        if($aksi == "edit" && !empty($request->get('id'))) {

          if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
              $formData = $this->getDoctrine()->getRepository('AppBundle:MahasiswaSkk')
                  ->find($request->get('id'));   
          }

          if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
              $formData = $this->getDoctrine()->getRepository('AppBundle:Master')
                  ->find($request->get('id'));   
          }   

        } else {

          if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
              $data = $this->getDoctrine()->getRepository('AppBundle:MahasiswaSkk')
                  ->findByMahasiswa($this->getUser()->getDataMahasiswa());      
              $formData = new \AppBundle\Entity\MahasiswaSkk();
              $formData->setMahasiswa($this->getUser()->getDataMahasiswa());
              $semester = ( null !== $this->getUser()->getDataMahasiswa()->getSemester() ) ? $this->getUser()->getDataMahasiswa()->getSemester()->getSemester() : null;
              $formData->setSemester($semester);
          }

          if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
              $data = $this->getDoctrine()->getRepository('AppBundle:Master')
                  ->findByType('kategori_kegiatan');      
              $formData = new Master();
              $formData->setType('kategori_kegiatan');
          }

        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_MAHASISWA')) {
            $form = $this->createFormBuilder($formData)
              ->add('kegiatan', null, array(
                  'label' => 'Nama Kegiatan'
              ))
              ->add('kategori', EntityType::class, array(
                  'label' => 'Kategori Kegiatan',
                  'class' => 'AppBundle:Master',
                  'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('m')
                        ->where('m.type=:type')
                        ->setParameter('type', 'kategori_kegiatan');
                  },
                  'choice_label' => 'nama',
              ))
              ->add('uraian')
              ->add('tanggal', DateType::class, array(
                  'required'  => false,
                  'label'     => 'Tanggal',
                  'widget'    => 'single_text',
                  'html5'     => false,
                  'format'    => 'dd-MM-yyyy',
                  'attr'      => ['class' => 'js-datepicker'],
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($formData);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('hs_skk');
            }
        }

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $form = $this->createFormBuilder($formData)
              ->add('nama', null, array(
                  'label' => 'Kategori Kegiatan'
              ))
              ->add('custom1', null, array(
                  'label' => 'Nilai'
              ))
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($formData);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('hs_skk');
            }
        }

        return $this->appService->load('akademika/hasil_studi/skk_'.$aksi.'.html.twig', array(
        	'data'	=> ( isset($data) ) ? $data : null,
          'formData'  => ( isset($formData) ) ? $formData : null,
          'form'  => ( isset($form) ) ? $form->createView() : null
        ));
    }

}
