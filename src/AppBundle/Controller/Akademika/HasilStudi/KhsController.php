<?php

namespace AppBundle\Controller\Akademika\HasilStudi;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class KhsController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/hasil_studi/khs", name="hs_khs_index")
     */
    public function indexAction(Request $request)
    {
       
        $em = $this->getDoctrine()->getManager();
        $params = array();


        $dataMahasiswa = array();
        $ta = $this->appService->getTahunAkademik();
        // $periode = $this->getDoctrine()->getRepository('AppBundle:PmbPeriode')
        //   ->findOneByTa($ta);
        $dataMahasiswaUserList = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findBy(array(
            'prodi'     => $this->getUser()->getProdi(),
            'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
          ));
          
        foreach ($dataMahasiswaUserList as $user) {

          $mhs = $user->getDataMahasiswa();

          if ( !empty($request->get('semester')) ) {

            if ( $mhs->getSemester() == $request->get('semester') ) {

              if ( !empty($request->get('kelas')) ) {

                $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
                  ->find($request->get('kelas'));

                if ( $kelas ) {
                  if ( $mhs->getKelas() == $kelas ) {
                    $dataMahasiswa[] = $mhs;
                  }
                }

              } else {
                $dataMahasiswa[] = $mhs;
              }

            }
            
          } else {
            $dataMahasiswa[] = $mhs;
          }
        }

        foreach ($dataMahasiswa as $data) {
          if ( null !== $data->getUser() ) {
            $user = $data->getUser();
            $result = array(
              'id'        => $data->getId(),
              'id_user'   => $user->getId(),
              'nama'      => $user->getNama(),
              'nim'       => $user->getUsername(),
              'angkatan'  => $data->getAngkatan(),
              'semester'  => $data->getSemester(),
              'krs'       => array(),
            );
            $this->response['result'][] = $result;
          }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/hasil_studi/khs_index.html.twig', array(
                'data'      => $this->response
            ));
        }
    }

    /**
     * @Route("/akademika/hasil_studi/khs/cetak/{id}", name="hs_khs_cetak")
     */
    public function cetakAction(Request $request, $id)
    {
        $params = array();
        $em = $this->getDoctrine()->getManager();
        $mhs = $em->getRepository('AppBundle:Mahasiswa')
            ->find($id);
        $data = array(
          'id'        => $mhs->getId(),
          'id_user'   => $mhs->getUser()->getId(),
          'nim'       => $mhs->getUser()->getUsername(),
          'nama'      => $mhs->getUser()->getNama(),
          'semester'  => $mhs->getSemester(),
          'angkatan'  => $mhs->getAngkatan(),
          'dosen'     => ( null !== $mhs->getPa() ) ? $mhs->getPa()->getUser()->getNama() : "",
          'prodi'     => ( null !== $mhs->getUser()->getProdi() ) ? $mhs->getUser()->getProdi()->getJenjang() . " " . $mhs->getUser()->getProdi()->getNamaProdi() : '',
          'krs'       => array()
        );
        $aspekNilai = $this->getDoctrine()->getRepository('AppBundle:AspekNilai')
            ->findAll();
        $this->response['result'] = $data;
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
            return $this->appService->load('akademika/hasil_studi/khs_cetak.html.twig', $params);
        }
    }
  
  }
