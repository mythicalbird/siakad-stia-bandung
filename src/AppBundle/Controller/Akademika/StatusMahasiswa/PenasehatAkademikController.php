<?php

namespace AppBundle\Controller\Akademika\StatusMahasiswa;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class PenasehatAkademikController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/status_mahasiswa/dosen_pembimbing", name="penasehat_akademik_index")
     */
    public function indexAction(Request $request)
    {
        $dataDosen = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        $params = array();
        if ( !empty($request->get('angkatan')) ) {
          $params['angkatan'] = $request->get('angkatan');
        }
        $dataMahasiswa = $this->appService->getListMahasiswa($params, false);
        foreach ($dataMahasiswa as $mhs) {
          if ( null !== $mhs->getUser() ) {
            $user = $mhs->getUser();
            if ( null !== $user->getProdi() && $user->getProdi() == $this->getUser()->getProdi() ) {
              $this->response['result'][] = array(
                'id'            => $mhs->getId(),
                'id_user'       => $user->getId(),
                'uuid'          => $mhs->getUuid(),
                'reg_pd'        => $mhs->getRegPd(),
                'nim'           => $user->getUsername(),
                'nama'          => $user->getNama(),
                'pa'            => ( null !== $mhs->getPa() ) ? $mhs->getPa()->getUser()->getNama() : ''
              );
            }
          }
        }
        if ( !empty($request->get('ids')) ) {
            $em = $this->getDoctrine()->getManager();
            $nim = $request->get('ids');
            if ( is_array($nim) ) {
                for ($i=0; $i < count($nim); $i++) { 
                    $user = $em->getRepository('AppBundle:User')
                      ->findOneByUsername($nim[$i]);
                    if ( $user ) {
                        $dosen = $em->getRepository('AppBundle:Dosen')
                          ->find($request->get('dosen'));
                        if ( $dosen ) {
                          $mhs = $user->getDataMahasiswa();
                          $mhs->setPa($dosen);
                          $em->persist($mhs);
                          $em->persist($user);
                          $em->flush();
                        }
                    }
                }
            }
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('penasehat_akademik_index');
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/status_mahasiswa/dosen_pembimbing_index.html.twig', array(
                'dataDosen'   => $dataDosen,
                'data'        => $this->response
            ));
        }
    }
}
