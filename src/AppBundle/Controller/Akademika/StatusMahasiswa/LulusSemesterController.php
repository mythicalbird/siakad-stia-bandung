<?php

namespace AppBundle\Controller\Akademika\StatusMahasiswa;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class LulusSemesterController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/status_mahasiswa/lulus_semester", name="lulus_semester_index")
     */
    public function indexAction(Request $request)
    {
        $args = array(
          'prodi' => $this->getUser()->getProdi()
        );
        if ( !empty($request->get('semester')) ) {
          $args['semester'] = $request->get('semester');
        }
        if ( !empty($request->get('kelas')) ) {
          $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
            ->find($request->get('kelas'));
          if ( $kelas ) {
            $args['kelas'] = $kelas;
          }
        }
        $dataMahasiswa = $this->appService
          ->getListMahasiswa($args, false);
        foreach ($dataMahasiswa as $mhs) {
          if ( null !== $mhs->getUser() ) {
            $user = $mhs->getUser();
            $this->response['result'][] = array(
              'id'        => $mhs->getId(),
              'id_user'   => $user->getId(),
              'nim'       => $user->getUsername(),
              'nama'      => $user->getNama(),
              'semester'  => $mhs->getSemester(),
              'angkatan'  => $mhs->getAngkatan()
            );
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/status_mahasiswa/lulus_semester_index.html.twig', array(
                'data'      => $this->response
            ));
        }
    }

    /**
     * @Route("/akademika/lulus_semester/proses", name="lulus_semester_proses")
     * @Method({"POST"})
     */
    public function prosesAction(Request $request)
    {
        $post_ids = $request->get('id');
        $post_semester = $request->get('semester');

        if ( is_array($post_ids) ) {

          $tahunAkademik = $this->appService->getGlobalOption()['ta'];

          for ($i=0; $i < count($post_ids); $i++) { 

            $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
                ->find($post_ids[$i]);
            if ( $mahasiswa ) {
                $em = $this->getDoctrine()->getManager();
                $mahasiswa->setSemester($post_semester[$i]);
                $em->persist($mahasiswa);
                $em->flush();
            }
          
          }

          $this->addFlash('success', 'Data berhasil disimpan.');
          return $this->redirectToRoute('lulus_semester_index');

        }
    }

    /**
     * @Route("/akademika/lulus_akademik", name="lulus_akademik_index")
     */
    public function lulusAkademikIndexAction(Request $request)
    {
        $tahunAkademik = $this->appService->getGlobalOption()['ta'];
        $param = array();
        // if( $request->get('semester') != '' ) {
        //     $semester = $this->getDoctrine()->getRepository('AppBundle:Semester')
        //       ->find($request->get('semester'));
        //     if ( $semester ) {
        //       $param['semester'] = $semester;
        //     }
        // }
        if( $request->get('kelas') != '' ) {
            $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
              ->find($request->get('kelas'));
            if ( $kelas ) {
              $param['kelas'] = $kelas;
            }
        }

        if ( count($param) > 0 ) {
          $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
            ->findBy($param);
        }

        $dataKelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')->findAll();
        return $this->appService->load('akademika/status_mahasiswa/lulus_akademik_index.html.twig', array(
            'data' => ( isset($data) ) ? $data : array(), 
            'dataKelas' => $dataKelas,
            'semester'  => (isset($semester)) ? $semester : null,
        ));
    }

    /**
     * @Route("/akademika/lulus_akademik/proses", name="lulus_akademik_proses")
     * @Method({"POST"})
     */
    public function lulusAkademikProsesAction(Request $request)
    {
        $post_ids = $request->get('id');
        $tgl_ijazah = $request->get('tgl_ijazah');
        $no_ijazah = $request->get('no_ijazah');
        $tgl_sk_yudisium = $request->get('tgl_sk_yudisium');
        $no_sk_yudisium = $request->get('no_sk_yudisium');

        if ( is_array($post_ids) ) {

          // set lulus
          $status = $this->appService->getMasterTermObject('status_mahasiswa', 'L');
          // tgl keluar & ijazah
          $tglKeluar = explode('-', $tgl_ijazah);
          $tglKeluar = date('Y-m-d', strtotime($tgl_ijazah[2] . '-' . $tgl_ijazah[1] . '-' . $tgl_ijazah[0] . ' 00:00:00'));
          $tglKeluar = new\DateTime($tglKeluar);
          // tgl sk yudisium
          $tglSkYudisium = explode('-', $tgl_sk_yudisium);
          $tglSkYudisium = date('Y-m-d', strtotime($tgl_sk_yudisium[2] . '-' . $tgl_sk_yudisium[1] . '-' . $tgl_sk_yudisium[0] . ' 00:00:00'));
          $tglSkYudisium = new\DateTime($tglSkYudisium);

          for ($i=0; $i < count($post_ids); $i++) { 

            $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
                ->find($post_ids[$i]);
            if ( $mahasiswa ) {
                $em = $this->getDoctrine()->getManager();
                $mahasiswa->setStatus($status);
                $mahasiswa->getUser()->setTglKeluar($tglKeluar);
                $mahasiswa->setTglIjazah($tglKeluar);
                $mahasiswa->setTglSkYudisium($tglSkYudisium);
                $mahasiswa->setNoSkYudisium($no_sk_yudisium);
                $mahasiswa->setNoSeriIjazah($no_ijazah);
                $em->persist($mahasiswa);
                $em->flush();
            }
          
          }

          $this->addFlash('success', 'Data berhasil disimpan.');
          return $this->redirectToRoute('lulus_akademik_index');

        }
    }
}
