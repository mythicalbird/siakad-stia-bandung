<?php

namespace AppBundle\Controller\Akademika\StatusMahasiswa;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\Kelas;
use AppBundle\Entity\Mahasiswa;
use AppBundle\Entity\Semester;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class CutiController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/akademika/status_mahasiswa/cuti", name="mahasiswa_cuti_index")
     */
    public function indexAction(Request $request)
    {
        // if( $request->get('semester') != '' ) {
        //     $semester = $this->getDoctrine()->getRepository(Semester::class)
        //       ->find($request->get('semester'));
        // }
        // $semesterList = $this->getDoctrine()->getRepository(Semester::class)->findAll();
        $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')->findAll();
        $tahun = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')->findAll();
        $data = $this->getDoctrine()->getRepository('AppBundle:MahasiswaCuti')
          ->findAll();
        return $this->appService->load('akademika/status_mahasiswa/cuti_index.html.twig', array(
            'data' => $data,
            'kelas' => $kelas,
            'semester'  => (isset($semester)) ? $semester : null,
            // 'semesterList'  => $semesterList,
        ));
    }

    /**
     * @Route("/akademika/status_mahasiswa/pengajuan_cuti", name="pengajuan_cuti_index")
     */
    public function pengajuanAction(Request $request, $aksi = 'index')
    {
        return $this->appService->load('akademika/status_mahasiswa/cuti_edit.html.twig');
    }

    /**
     * @Route("/akademika/_ajax/status_mahasiswa/cuti/cari_mahasiswa", name="cuti_cari_mahasiswa")
     * @Method({"POST"})
     */
    public function cariMahasiswaAction(Request $request)
    {
        $response = new JsonResponse();
        $mahasiswa = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ($mahasiswa) {
          if( null !== $mahasiswa->getProdi() ) {
            $prodi = $mahasiswa->getProdi()->getNamaProdi();
          } else {
            $prodi = 'Tidak ditemukan';
          }
          $cuti = $this->getDoctrine()->getRepository('AppBundle:MahasiswaCuti')
            ->findOneByMahasiswa($mahasiswa);
          $tglMulai = ( $cuti ) ? $cuti->getTglMulai() : '';
          $tglAkhir = ( $cuti ) ? $cuti->getTglAkhir() : '';
          $statusCuti = ( $cuti ) ? $cuti->getStatus() : '';
          $actionUrl = $this->generateUrl( 'mahasiswa_cuti_submit' );
          $html = '<div class="row"><div class="col-md-12"><table class="table table-bordered table-responsive"><tr><td>Nama Mahasiswa</td>';
          $html .= '<td><strong>'.$mahasiswa->getNama().'</strong></td>';
          $html .= '</tr><tr><td>NIM</td>';
          $html .= '<td><em>'.$mahasiswa->getUsername().'</em></td>';
          $html .= '</tr><tr><td>Tahun Angkatan</td>';
          $html .= '<td>'.$mahasiswa->getNama().'</td>';
          $html .= '</tr><tr><td>Kode Prodi</td>';
          $html .= '<td>'.$prodi.'</td>';
          $html .= '</tr>';
          $html .= '</table></div></div>';
          $html .= '<div class="clearfix"></div>';

          $html .= '<div class="row"><div class="col-md-12"><form method="post" action="'.$actionUrl.'">';
          $html .= '<table class="table table-bordered table-responsive"><tr>';
          $html .= '<th colspan="2">Pengajuan Cuti</th>';
          $html .= '</tr><tr><td>Mulai Tanggal</td>';
          $html .= '<td><input type="text" value="'.$tglMulai.'" class="form-control datepicker" name="mulai" style="width:200px"></td>';
          $html .= '</tr><tr><td>Sampai Tanggal</td>';
          $html .= '<td><input type="text" value="'.$tglAkhir.'" class="form-control datepicker" name="sampai" style="width:200px"></td>';
          $html .= '</tr>';
          if ( $statusCuti != 'PENGAJUAN' ) {
            $html .= '<tr><td>Status Cuti</td>';
            $html .= '<td>'.$statusCuti.'</td>';
            $html .= '</tr>';            
          }
          if ( $statusCuti == 'PENGAJUAN' || $statusCuti == '' ) {
            $html .= '<tr><td></td>';
            $html .= '<td><input type="hidden" name="nim" value="'.$mahasiswa->getId().'"><button type="submit" class="btn btn-primary">Simpan</button></td>';
            $html .= '</tr>';
          }

          $html .= '</table>';
          $html .= '</form></div></div>';
          $html .= '<div class="clearfix"></div>';

          $response->setData($html);
        } else {
          $response->setData('<div class="alert alert-error">Mahasiswa tidak ditemukan!</div>');
        }
        return $response;
    }

    /**
     * @Route("/akademika/cuti/submit", name="mahasiswa_cuti_submit")
     * @Method({"POST"})
     */
    public function submitAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
          ->find($request->get('nim'));
        if ( $user ) {

          if ( null !== $user->getDataMahasiswa() ) {
            $mhs = $user->getDataMahasiswa();

            $data = $this->getDoctrine()->getRepository('AppBundle:MahasiswaCuti')
              ->findOneByMahasiswa($mhs);

            if ( ! $data ) {
              $data = new \AppBundle\Entity\MahasiswaCuti();
              $data->setMahasiswa($mhs);
              $data->setStatus('PENGAJUAN');
            }

            $data->setTglMulai($request->get('mulai'));
            $data->setTglAkhir($request->get('sampai'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
          }

          $this->addFlash('success', 'Pengajuan cuti telah disimpan.');

        }
        return $this->redirectToRoute('mahasiswa_cuti_index');
    }

    /**
     * @Route("/_ajax/akademika/status_mahasiswa/cuti/update_status_cuti", name="status_cuti_update")
     * @Method({"POST"})
     */
    public function updateStatusAction(Request $request)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:MahasiswaCuti')
          ->find($request->get('pk'));
        if ( $data ) {

            $status = $request->get('value');
            $data->setStatus($status);
            $em->persist($data);

            $mhs = $data->getMahasiswa();
            if ( $status == 'CUTI' ) {
              $mhs->setStatus( $this->appService->getMasterTermObject( 'status_mahasiswa', 'C' ) );
              $em->persist($mhs);
            } elseif ( $status == 'AKTIF' ) {
              $mhs->setStatus( $this->appService->getMasterTermObject( 'status_mahasiswa', 'A' ) );
              $em->persist($mhs);
            }

            $em->flush();
            $response->setData(array(
              'id' => $request->get('pk'),
              'success' => 1
            ));
        }
        return $response;
    }

}
