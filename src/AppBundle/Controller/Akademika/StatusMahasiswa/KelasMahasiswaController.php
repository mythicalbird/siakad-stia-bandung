<?php

namespace AppBundle\Controller\Akademika\StatusMahasiswa;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class KelasMahasiswaController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/status_mahasiswa/kelas", name="kelas_mahasiswa_index")
     */
    public function indexAction(Request $request)
    {
        $isAjax = $request->isXmlHttpRequest();
        $params = array();

        if(!empty($request->get('semester')) || !empty($request->get('kelas'))) {
          if( !empty($request->get('semester')) ) {
            $params['semester'] = $request->get('semester');
          }
          if( !empty($request->get('kelas')) ) {
            $kelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
              ->find($request->get('kelas'));
            if( $kelas )
              $params['kelas'] = $kelas;
          }
        } 

        $dataMahasiswa = $this->appService->getListMahasiswa($params, false);
        $dt = array();
        foreach ($dataMahasiswa as $mhs) {
          if ( null !== $mhs->getUser() && null !== $mhs->getUser()->getProdi() && $mhs->getUser()->getProdi() == $this->getUser()->getProdi() ) {
            $kelas_mhs = '';
            if ( null !== $mhs->getKelas() ) {
              $kelas_mhs .= $mhs->getKelas()->getNama();
              if ( !empty($mhs->getKelas()->getRuangan()) ) {
                $kelas_mhs .= ' (' . $mhs->getKelas()->getRuangan() . ')';
              }
            }
            $this->response['result'][] = array(
              'id'          => $mhs->getId(),
              'id_user'     => $mhs->getUser()->getId(),
              'nim'         => $mhs->getUser()->getUsername(),
              'nama'        => $mhs->getUser()->getNama(),
              'angkatan'    => $mhs->getAngkatan(),
              'kelas'       => array(
                'id'    => ( null !== $mhs->getKelas() ) ? $mhs->getKelas()->getId() : '',
                'nama'    => $kelas_mhs
              )
            );          
          }
        }
        $dataKelas = $this->getDoctrine()->getRepository('AppBundle:Kelas')
          ->findBy(array(
            // 'ta'    => $this->appService->getTahunAkademik(),
            'prodi' => $this->getUser()->getProdi()
          ));

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/status_mahasiswa/kelas_mahasiswa_index.html.twig', array(
              'dataKelas'     => $dataKelas,
              'data'          => $this->response
            ));
        }
    }
}
