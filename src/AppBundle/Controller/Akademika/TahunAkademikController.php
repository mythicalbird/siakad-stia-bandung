<?php

namespace AppBundle\Controller\Akademika;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class TahunAkademikController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/periode_tahun_ajaran", name="tahun_akademik_index")
     */
    public function indexAction(Request $request)
    {
        for ($i=2000; $i < 2023; $i++) { 
            $this->appService->getTa($i . 1, true);
            $this->appService->getTa($i . 2, true);
            $this->appService->getTa($i . 3, true);
        }
        $dataTa = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')
            ->findByStatus("publish");
        foreach ($dataTa as $ta) {
            $nm_ta = $ta->getTahun() . "/" . ($ta->getTahun()+1);
            if ( $ta->getKodeSemester() == 1 ) {
                $nm_ta .= ' Ganjil';
            } elseif ( $ta->getKodeSemester() == 2 ) {
                $nm_ta .= ' Genap';
            } elseif ( $ta->getKodeSemester() == 3 ) {
                $nm_ta .= ' Pendek';
            }
            $this->response['result'][] = array(
                'id'            => $ta->getId(),
                'kode'          => $ta->getKode(),
                'nama'          => ( !empty($nm_ta) ) ? $nm_ta : $ta->getNama(),
                'tahun'         => $ta->getTahun(),
                'perkuliahan'   => $ta->getPerkuliahan(),
                'aktif'         => $ta->getAktif()
            );
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/tahun_akademik_index.html.twig', array(
                'data' => $this->response
            ));
        }
    }

    /**
     * @Route("/akademika/tahun/edit", name="tahun_akademik_edit")
     */
    public function editAction(Request $request)
    {
        if (!empty($request->get('aksi')) && $request->get('aksi') == "tambah") {
            $data = new \AppBundle\Entity\TahunAkademik();
            $data->setAktif(0);
        } elseif (!empty($request->get('aksi')) && $request->get('aksi') == "hapus") {
        } elseif (!empty($request->get('id'))) {
            $data = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')->find($request->get('id'));
        } else {
            exit;
        }
        $years = array();
        $date = new\DateTime();
        $max = $date->format('Y')+5;
        for($i = $max; $i > 1980; $i--) {
          $years[$i] = $i;
        }
        $form = $this->createFormBuilder($data)
            ->add('tahun', ChoiceType::class, array(
                'choices'       => $years,
                'placeholder'   => '-- Pilih Tahun --'
            ))
            ->add('kodeSemester', ChoiceType::class, array(
                'label'         => 'Semester',
                'choices'       => array(
                    'Semester Ganjil'   => 1,
                    'Semester Genap'    => 2,
                ),
                'placeholder'   => '-- Pilih --'
            ))
            ->add('nama', null, array(
                'label'     => 'Nama Th Akademik'
            ))
            ->add('batasKrs', null, array(
                'label'       => 'Batas KRS',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasKrsOnline', null, array(
                'label'       => 'KRS Online',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasUbahKrs', null, array(
                'label'       => 'Ubah KRS',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasCetakKss', null, array(
                'label'       => 'Cetak KSS',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasPembayaran', null, array(
                'label'       => 'Batas Pembayaran',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('perkuliahan', null, array(
                'label'       => 'Perkuliahan',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasUts', null, array(
                'label'       => 'UTS',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('batasUas', null, array(
                'label'       => 'UAS',
                'attr'        => [
                  'class' => 'drpick',
                  'placeholder' => 'Tanggal mulai - Tanggal berakhir',
                ]
            ))
            ->add('bolehCuti', DateType::class, array(
                'required'    => false,
                'label'       => 'Boleh Cuti',
                'widget'      => 'single_text',
                'html5'       => false,
                'format'      => 'dd-MM-yyyy',
                'attr'        => [
                  'class' => 'js-datepicker',
                  'style' => 'width:45%;display:inline',
                  'placeholder' => 'Tanggal Mulai',
                ],
            ))
            ->add('bolehMundur', DateType::class, array(
                'required'    => false,
                'label'       => 'Boleh Mundur',
                'widget'      => 'single_text',
                'html5'       => false,
                'format'      => 'dd-MM-yyyy',
                'attr'        => [
                  'class' => 'js-datepicker',
                  'style' => 'width:45%;display:inline',
                  'placeholder' => 'Tanggal Mulai',
                ],
            ))
            ->add('akhirKss', DateType::class, array(
                'required'    => false,
                'label'       => 'Akhir KSS',
                'widget'      => 'single_text',
                'html5'       => false,
                'format'      => 'dd-MM-yyyy',
                'attr'        => [
                  'class' => 'js-datepicker',
                  'style' => 'width:45%;display:inline',
                  'placeholder' => 'Tanggal Mulai',
                ],
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class' => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ( null !== $this->getUser()->getProdi() && null !== $this->getUser()->getProdi()->getJumlahSesi() && null !== $this->getUser()->getProdi()->getBatasSesi() ) {

                $prodi = $this->getUser()->getProdi();
                $jmlSesi = $prodi->getJumlahSesi();
                $btsSesi = $prodi->getBatasSesi();
                $semesterArray = array();

                for ($i=$data->getKodeSemester(); $i <= ($btsSesi/$jmlSesi)+1; $i++) { 
                    if ( $data->getKodeSemester() == 1 ) {
                        if($i % 2 != 0){
                            $semesterArray[] = $i;
                        }
                    } else {
                        if($i % 2 == 0){
                            $semesterArray[] = $i;
                        }
                    }
                }

            } else {
                if ( $data->getKodeSemester() == 1 ) {
                    $semesterArray = array(1,3,5,7,9,11,13);
                } else {
                    $semesterArray = array(2,4,6,8,10,12,14);
                }
            }

            $kodeSemester = $data->getTahun().$data->getKodeSemester();

            // foreach ($semesterArray as $s) {

            //     $semester = $em->getRepository('AppBundle:Semester')
            //         ->findOneBy(array(
            //             'kode'      => $kodeSemester,
            //             'semester'  => $s
            //         ));
            //     if ( ! $semester ) {
            //         $semester = new \AppBundle\Entity\Semester();
            //     }
            //     $semester->setKode($kodeSemester);
            //     $namaSemester = $data->getTahun() . '/';
            //     $namaSemester .= $namaSemester+1 . ' ';
            //     $namaSemester .= ( $data->getKodeSemester() == 1 ) ? 'Ganjil' : 'Genap';
            //     $semester->setNama($namaSemester);
            //     $semester->setSemester($s);
            //     $semester->setTahunAkademik($data);
            //     $em->persist($semester);
            //     $em->flush();
            // }
            $data->setKode($kodeSemester);
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Data berhasil disimpan.');
            return $this->redirectToRoute('tahun_akademik_index');
        }
        return $this->appService->load('AkademikaBundle:Default:tahun_akademik_edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/_ajax/edit/tahun_akademik", name="tahun_akademik_update")
         * @Method({"POST"})
     */
    public function ajaxUpdateAction(Request $request)
    {
        $response = new JsonResponse();
        $aktifId = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $thAkademik = $em->createQueryBuilder()
                  ->select('ta')
                  ->from('AppBundle:TahunAkademik', 'ta')
                  ->getQuery()
                  ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        foreach ($thAkademik as $ta) {
          $data = $em->getRepository('AppBundle:TahunAkademik')
            ->find($ta['id']);
          if ( $data ) {
            if ( $ta['id'] == $aktifId ) {
              $data->setAktif(1);
            } else {
              $data->setAktif(0);
            }
            $em->persist($data);
            $em->flush();
          }
        }
        return $response;
    }
}
