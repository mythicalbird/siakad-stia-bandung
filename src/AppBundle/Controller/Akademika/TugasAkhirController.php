<?php

namespace AppBundle\Controller\Akademika;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Dosen;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Service\AppService;

class TugasAkhirController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/tugas_akhir/pengajuan", name="tugas_akhir_index")
     */
    public function indexAction(Request $request)
    {
        return $this->appService->load('akademika/tugas_akhir/index.html.twig', array(
        	
        ));
    }

    /**
     * @Route("/akademika/tugas_akhir/persetujuan/{aksi}", name="persetujuan_tugas_akhir")
     */
    public function persetujuanIndexAction(Request $request, $aksi = 'list')
    {
        $params = array();
        if ( $aksi == 'edit' && !empty($request->get('id')) ) {
            $data = $this->getDoctrine()->getRepository('AppBundle:TugasAkhir')
                ->find($request->get('id'));
            $this->response['result'] = $data;
            $form = $this->createFormBuilder($data)
                ->add('judul')
                ->add('tglPengajuan', DateType::class, array(
                    'required'  => false,
                    'label'     => 'Tanggal Pengajuan',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-MM-yyyy',
                    'attr'      => ['class' => 'js-datepicker'],
                ))
                ->add('tglMulai', DateType::class, array(
                    'required'  => false,
                    'label'     => 'Tanggal Mulai',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-MM-yyyy',
                    'attr'      => ['class' => 'js-datepicker'],
                ))
                ->add('tglBerakhir', DateType::class, array(
                    'required'  => false,
                    'label'     => 'Tanggal Berakhir',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-MM-yyyy',
                    'attr'      => ['class' => 'js-datepicker'],
                ))
                ->add('tglSidang', DateType::class, array(
                    'required'  => false,
                    'label'     => 'Tanggal Sidang',
                    'widget'    => 'single_text',
                    'html5'     => false,
                    'format'    => 'dd-MM-yyyy',
                    'attr'      => ['class' => 'js-datepicker'],
                ))
                ->add('pembimbing1', EntityType::class, array(
                    'required'  => false,
                    'label'  => 'Pembimbing 1',
                    'class'     => 'AppBundle:Dosen',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m');
                    },
                    'attr'        => array( 'class' => 'form-control select2' ),
                    'choice_label' => function(Dosen $entity = null) {
                        return ( $entity->getUser() !== null ) ? $entity->getUser()->getNama() : $entity->getId();
                    },
                    'placeholder' => '-- Pilih Dosen --',
                ))
                ->add('pembimbing2', EntityType::class, array(
                    'required'  => false,
                    'label'  => 'Pembimbing 2',
                    'class'     => 'AppBundle:Dosen',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m');
                    },
                    'attr'        => array( 'class' => 'form-control select2' ),
                    'choice_label' => function(Dosen $entity = null) {
                        return ( $entity->getUser() !== null ) ? $entity->getUser()->getNama() : $entity->getId();
                    },
                    'placeholder' => '-- Pilih Dosen --',
                ))
                ->add('penguji1', EntityType::class, array(
                    'required'  => false,
                    'label'  => 'Penguji 1',
                    'class'     => 'AppBundle:Dosen',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m');
                    },
                    'attr'        => array( 'class' => 'form-control select2' ),
                    'choice_label' => function(Dosen $entity = null) {
                        return ( $entity->getUser() !== null ) ? $entity->getUser()->getNama() : $entity->getId();
                    },
                    'placeholder' => '-- Pilih Dosen --',
                ))
                ->add('penguji2', EntityType::class, array(
                    'required'  => false,
                    'label'  => 'Penguji 2',
                    'class'     => 'AppBundle:Dosen',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m');
                    },
                    'attr'        => array( 'class' => 'form-control select2' ),
                    'choice_label' => function(Dosen $entity = null) {
                        return ( $entity->getUser() !== null ) ? $entity->getUser()->getNama() : $entity->getId();
                    },
                    'placeholder' => '-- Pilih Dosen --',
                ))
                ->add('diterima')
                ->add('submit', SubmitType::class, array(
                    'label' => 'Simpan',
                    'attr'  => array(
                        'class' => 'btn btn-primary'
                    )
                ))
                ->getForm();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
              $em = $this->getDoctrine()->getManager();
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('persetujuan_tugas_akhir');
            }
            $params['form'] = $form->createView();
        } else {
            $dataTugasAkhir = $this->getDoctrine()->getRepository('AppBundle:TugasAkhir')
                ->findAll();
            foreach ($dataTugasAkhir as $tugas) {
                if ( null !== $tugas->getMahasiswa() && 'trash' != $tugas->getStatus() ) {
                    $mhs = $tugas->getMahasiswa();
                    $this->response['result'][] = array(
                        'id'                => $tugas->getId(),
                        'id_mhs'            => $mhs->getId(),
                        'id_user'           => $mhs->getUser()->getId(),
                        'nim'               => $mhs->getUser()->getUsername(),
                        'nama'              => $mhs->getUser()->getNama(),
                        'semester'          => $mhs->getSemester(),
                        'angkatan'          => $mhs->getAngkatan(),
                        'judul'             => $tugas->getJudul(),
                        'status'            => ( $tugas->getDiterima() ) ? 'Diterima' : 'Proses'
                    );
                }
            }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
            return $this->appService->load('akademika/tugas_akhir/'.$aksi.'.html.twig', $params);
        }
    }

    /**
     * @Route("/akademika/_ajax/tugas_akhir/cari_mahasiswa", name="tugas_akhir_cari_mahasiswa")
     * @Method({"POST"})
     */
    public function cariMahasiswaAction(Request $request)
    {
        $response = new JsonResponse();
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));
        if ($user) {
            $prodi = ( null !== $user->getProdi() ) ? $user->getProdi()->getNamaProdi() : 'Tidak ditemukan';
            $mhs = $user->getDataMahasiswa();

            $html = '<div class="row"><div class="col-md-12"><table class="table table-bordered table-responsive"><tr><td>Nama Mahasiswa</td>';
            $html .= '<td><strong>'.$user->getNama().'</strong></td>';
            $html .= '</tr><tr><td>NIM</td>';
            $html .= '<td><em>'.$user->getUsername().'</em></td>';
            $html .= '</tr><tr><td>Tahun Angkatan</td>';
            $html .= '<td>'.$mhs->getAngkatan().'</td>';
            $html .= '</tr><tr><td>Kode Prodi</td>';
            $html .= '<td>'.$prodi.'</td>';
            $html .= '</tr><tr><td>Judul Tugas Akhir</td>';

          // cek table rekomendasi
          // $rekomendasi = $this->getDoctrine()->getRepository('AppBundle:RekomendasiPembayaran')->findOneByNim($mahasiswa->getUsername());
          // if (!$rekomendasi) {
          //   $html .= '<td><a href="#" data-toggle="modal" data-target="#rekomendasiModal" data-nim="'.$mahasiswa->getUsername().'" data-nama="'.$mahasiswa->getNama().'" data-prodi="'.$prodi.'">Tambah rekomendasi</a>';
          //   $html .= '</td>';
          // } else {
          //   $html .= '<td>Sudah direkomendasikan</td>';
            // }

            $url = $this->generateUrl( 'pengajuan_judul_action', array( 'nim' => $user->getUsername() ) );

            $tugasAkhir = $this->getDoctrine()->getRepository('AppBundle:TugasAkhir')
            ->findOneByMahasiswa($mhs);
            if ( !$tugasAkhir ) {
                $html .= '<td><form action="'.$url.'" method="post" id="pengajuanForm"><div class="form-group"><textarea name="judul" class="form-control" required></textarea></div><input type="hidden" name="nim" value="'.$user->getUsername().'"><button class="btn btn-primary">Simpan</button></form></td>';
            } else {
                $html .= '<td><form action="'.$url.'" method="post" id="pengajuanForm"><div class="form-group"><textarea name="judul" class="form-control" required>'.$tugasAkhir->getJudul().'</textarea></div><input type="hidden" name="nim" value="'.$user->getUsername().'"><button class="btn btn-primary">Simpan</button></form></td>';
            }
            $html .= '</tr>';

            if ( $tugasAkhir ) {    
                $html .= '<tr>';
                $html .= '<td>Tanggal Pengajuan</td>';
                $html .= '<td>'.$tugasAkhir->getTglPengajuan()->format('d-m-Y').'</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Tanggal Mulai</td>';
                $html .= '<td>'.$tugasAkhir->getTglMulai().'</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Tanggal Berakhir</td>';
                $html .= '<td>'.$tugasAkhir->getTglBerakhir().'</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Tanggal Sidang</td>';
                $html .= '<td>'.$tugasAkhir->getTglSidang().'</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Dosen Pembimbing 1</td>';
                $html .= '<td>';
                $html .= ( null !== $tugasAkhir->getPembimbing1() ) ? $tugasAkhir->getPembimbing1()->getUser()->getNama() : '';
                $html .= '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Dosen Pembimbing 2</td>';
                $html .= '<td>';
                $html .= ( null !== $tugasAkhir->getPembimbing2() ) ? $tugasAkhir->getPembimbing2()->getUser()->getNama() : '';
                $html .= '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Dosen Penguji 1</td>';
                $html .= '<td>';
                $html .= ( null !== $tugasAkhir->getPenguji1() ) ? $tugasAkhir->getPenguji1()->getUser()->getNama() : '';
                $html .= '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Dosen Penguji 2</td>';
                $html .= '<td>';
                $html .= ( null !== $tugasAkhir->getPenguji2() ) ? $tugasAkhir->getPenguji2()->getUser()->getNama() : '';
                $html .= '</td>';
                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td>Diterima</td>';
                $html .= '<td>';
                $html .= ( $tugasAkhir->getDiterima() == 1 ) ? 'Ya' : 'Tidak';
                $html .= '</td>';
                $html .= '</tr>';
            }

            $html .= '</table></div></div>';
            $html .= '<div class="clearfix"></div>';
          $response->setData($html);
        } else {
          $response->setData('<div class="alert alert-error">Mahasiswa tidak ditemukan!</div>');
        }
        return $response;
    }


    /**
     * @Route("/akademika/tugas_akhir/action/submit_judul", name="pengajuan_judul_action")
     * @Method({"POST"})
     */
    public function submitAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('nim'));

        if ( null !== $user->getDataMahasiswa() ) {
            $mhs = $user->getDataMahasiswa();

            $tugasAkhir = $this->getDoctrine()->getRepository('AppBundle:TugasAkhir')
                ->findOneByMahasiswa($mhs);
            if ( ! $tugasAkhir ) {
                $tugasAkhir = new \AppBundle\Entity\TugasAkhir();
                $tugasAkhir->setMahasiswa($mhs);
                $tugasAkhir->setTglPengajuan(new\DateTime());
                $tugasAkhir->setDiterima(0);
            }

            $judul = $request->get('judul');
            $tugasAkhir->setJudul($judul);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tugasAkhir);
            $em->flush();
        }

        $this->addFlash('success', 'Judul telah disimpan.');
        return $this->redirectToRoute('tugas_akhir_index', array( 'nim' => $request->get('nim') ));
    }

}
