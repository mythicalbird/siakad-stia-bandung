<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Presensi;
use AppBundle\Service\AppService;

class PresensiDosenController extends Controller
{  
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }

    /**
     * @Route("/akademika/perkuliahan/presensi/dosen", name="presensi_index")
     */
    public function indexAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
          ->add('tanggal', DateType::class, array(
              'label'     => 'Tanggal',
              'widget'    => 'single_text',
              'html5'     => false,
              'format'    => 'dd-mm-yyyy',
              'attr'      => ['class' => 'js-datepicker'],
          ))
          ->add('jam', EntityType::class, array(
              'class'     => 'AppBundle:JamKuliah',
              'query_builder' => function (EntityRepository $er) {
                  return $er->createQueryBuilder('jk');
              },
              'attr'          => array( 'class' => 'form-control' ),
              'choice_label'  => 'jam',
              'placeholder'   => '-- Pilih --',
          ))
          ->add('dosen', CollectionType::class, array(
              'label' => false,
              'entry_type' => HiddenType::class,
          ))
          ->add('presensi', CollectionType::class, array(
              'label' => false,
              'entry_type' => HiddenType::class,
          ))
          ->getForm();
        $form->handleRequest($request);
        if ( $form->isSubmitted() ) {
          $post = $_POST['form'];
          // echo "<pre>";
          // var_dump($post);echo "</pre>";exit;
          
          $tanggal = new\DateTime($post['tanggal']);
          $jam = $em->getRepository('AppBundle:JamKuliah')->find($post['jam']);
          for($i = 0; $i < count($post['dosen']); $i++) {

            if ( isset($post['dosen'][$i]) ) {

              $dosen = $em->getRepository('AppBundle:Dosen')->find($post['dosen'][$i]);
              if ( $dosen ) {
                $ket = $post['presensi'][$i];
                if ( null !== $dosen->getUser() && !empty($ket) ) {
                  $presensi = $em->getRepository('AppBundle:PresensiDosen')
                    ->findOneBy(array(
                      'ta'      => $this->appService->getTahunAkademik(),
                      'dosen'   => $dosen,
                      'tanggal' => $tanggal, 
                      'jam'     => $jam
                    ));
                  if ($presensi) {
                    $presensi->setKet($ket);
                  } else {
                    $presensi = new \AppBundle\Entity\PresensiDosen();
                    $presensi->setTa($this->appService->getTahunAkademik());
                    $presensi->setDosen($dosen);
                    $presensi->setKet($ket);
                    $presensi->setTanggal($tanggal);
                    $presensi->setJam($jam);
                  }
                  $em->persist($presensi);
                  $em->flush();
                }
              }
            }
          }
          $this->addFlash('success', 'Data berhasil disimpan');
          return $this->redirectToRoute('presensi_index');
        }

        $params['form'] = $form->createView();
        $dataDosen = $em->getRepository('AppBundle:Dosen')
          ->findBy(array(
            'aktif' => 1
          ));
        foreach ($dataDosen as $dosen) {
          if ( null !== $dosen->getUser() ) {
            $this->response['result'][] = array(
              'id'            => $dosen->getId(),
              'id_user'       => $dosen->getUser()->getId(),
              'username'      => $dosen->getUser()->getUsername(),
              'nidn'          => $dosen->getNidn(),
              'nama'          => $dosen->getUser()->getNama(),
              'jabatan'       => $dosen->getJabatanAkademik(),
              'pangkat_golongan'=> ( null !== $dosen->getPangkatGolongan() ) ? $dosen->getPangkatGolongan()->getNama() : '',
              'presensi'      => array(
                'hadir' => $this->appService->getCountPresensiDosen($dosen, 'hadir'),
                'sakit' => $this->appService->getCountPresensiDosen($dosen, 'sakit'),
                'ijin'  => $this->appService->getCountPresensiDosen($dosen, 'ijin'),
                'alpa'  => $this->appService->getCountPresensiDosen($dosen, 'alpa')
              ),
            );
          }
        }

        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            $params['data'] = $this->response;
            return $this->appService->load('akademika/perkuliahan/presensi_dosen_index.html.twig', $params);
        }
    }

    /**
     * @Route("/akademika/perkuliahan/presensi/edit_ajax/dosen", name="presensi_dosen_edit_ajax")
     * @Method({"POST"})
     */
    public function presensiDosenEditAjaxAction(Request $request)
    {
        // AJAX
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('username'));

        $html = '';

        if ( $user && null !== $user->getDataDosen() ) {
            $dosen = $user->getDataDosen();
            $dataPresensi = $em->getRepository('AppBundle:PresensiDosen')
              ->findBy(array(
                'dosen'          => $dosen,
                'ta' => $this->appService->getTahunAkademik()
              ));
            if ( $dataPresensi ) {
              $no = 1;
              $count = 0;
              foreach ($dataPresensi as $ps) {
                $html .= '<tr>';
                $html .= '<td>'.$no.'</td>';
                $html .= '<td>'.$this->appService->hari($ps->getTanggal()).', '.$ps->getTanggal()->format('d-m-Y').'</td>';
                $html .= '<td>';

                $html .= '<select class="form-control" name="presensi['.$count.'][jam_id]">';
                $html .= '<option value="">-- Pilih Jam --</option>';
                $jamKuliah = $em->getRepository('AppBundle:JamKuliah')
                  ->findByProdi($this->getUser()->getProdi());
                if ( $jamKuliah ) {
                  foreach ($jamKuliah as $jam) {
                    $html .= '<option value="';
                    $html .= $jam->getId();
                    $html .= '"';
                    $html .= ( null !== $ps->getJam() && $jam == $ps->getJam() ) ? ' selected' : '';
                    $html .= '>';
                    $html .= $jam->getJam();
                    $html .= ( null !== $jam->getDari() ) ? ' >> ' . $jam->getDari()->format('H:i') : '';
                    $html .= ( null !== $jam->getSampai() ) ? ' - '.$jam->getSampai()->format('H:i') : '';
                    $html .= '</option>';
                  }
                }
                $html .= '</select>';
                $html .= '<input type="hidden" name="presensi['.$count.'][id]" value="'.$ps->getId().'">';
                $html .= '</td>';
                $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="hadir"';
                $html .= ($ps->getKet() == 'hadir') ? ' checked' : '';
                $html .= ' required></td>';
                $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="sakit"';
                $html .= ($ps->getKet() == 'sakit') ? ' checked' : '';
                $html .= ' required></td>';
                $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="ijin"';
                $html .= ($ps->getKet() == 'ijin') ? ' checked' : '';
                $html .= ' required></td>';
                $html .= '<td align="center"><input type="radio" name="presensi['.$count.'][ket]" value="alpa"';
                $html .= ($ps->getKet() == 'alpa') ? ' checked' : '';
                $html .= ' required></td>';
                $no++;
                $count++;
              }
            }
        }

        $response->setData($html);

        return $response;
    }

    /**
     * @Route("/akademika/perkuliahan/presensi/edit/dosen", name="presensi_dosen_edit")
     * @Method({"POST"})
     */
    public function presensiDosenEditAction(Request $request)
    {
        // NON AJAX
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')
          ->findOneByUsername($request->get('username'));

        if ( $user ) {

            $dataPresensi = $request->get('presensi');
            foreach ($dataPresensi as $ps) {

              $presensi = $em->getRepository('AppBundle:PresensiDosen')
                ->find($ps['id']);

              if ( $presensi ) {

                if ( isset($ps['jam_id']) ) {
                  $jam = $em->getRepository('AppBundle:JamKuliah')
                    ->find($ps['jam_id']);
                  if ( $jam ) {
                    $presensi->setJam($jam);
                  }
                }
                
                if ( isset($ps['ket']) ) {
                  $presensi->setKet($ps['ket']);
                }

                $presensi->setTahunAkademik($this->appService->getTahunAkademik());

                $em->persist($presensi);
                $em->flush();

              }

            }
        
        }

        $this->addFlash('success', 'Data berhasil diperbarui...');
        return $this->redirectToRoute('presensi_index');
    }

    /**
     * @Route("/cetak/presensi/{role}/{id}", name="cetak_presensi")
     */
    public function cetakPresensiAction(Request $request, $id, $role = 'mahasiswa')
    {

        if ($role == 'dosen') {
            $data = $this->getDoctrine()->getRepository('AppBundle:Dosen')->find($id);
        } else {
            $data = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')->find($id);
        }
        if ($data) {
            if ($role == 'dosen') {
              $presensi = $this->getDoctrine()->getRepository('AppBundle:PresensiDosen')
                  ->findBy(array(
                    'user'  => $data->getUser(),
                    'tahunAkademik' => $this->appService->getTahunAkademik()
                  ));
            } else {
              $params = array(
                'tahunAkademik' => $this->appService->getTahunAkademik(),
                'semester'      => $data->getSemester(),
                'kelas'         => $data->getKelas(),
                'user'          => $data->getUser()
              );
              if ( !empty($request->get('makul')) ) {
                $makul = $this->getDoctrine()->getRepository('AppBundle:Makul')->find($request->get('makul'));
                if ( $makul ) {
                  $params['makul'] = $makul;
                }
              }
              $presensi = $this->getDoctrine()->getRepository('AppBundle:PresensiMahasiswa')
                  ->findBy($params);
            }
        }
        return $this->appService->load('cetak/'.$role.'_presensi.html.twig', array(
            'data' => $data,
            'presensi'  => ( $presensi ) ? $presensi : []
        ));
    }
}
