<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class PerkuliahanController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/akademika/perkuliahan/materi", name="materi_makul_index")
     */
    public function materiKuliahIdexAction(Request $request)
    {
    	$data = $this->getDoctrine()->getRepository(TahunAkademik::class)->findAll();
        return $this->appService->load('AkademikaBundle:Default:index.html.twig', array(
        	'data'	=> $data
        ));
    }
  
    /**
     * @Route("/akademika/perkuliahan/dosen_pembimbing", name="mahasiswa_pa_index")
     */
    public function mahasiswaPaIndexAction(Request $request)
    {
        $ta = $this->appService->getTahunAkademik();
        $params = array(
          'prodi'     => $this->getUser()->getProdi(),
          // 'angkatan'  => $ta->getTahun()
        );
        $dataMahasiswa = $this->appService->getListMahasiswa($params);
        foreach ($dataMahasiswa as $mhs) {
          if ( null !== $mhs->getUser() && $mhs->getUser()->getProdi() == $this->getUser()->getProdi() ) {
            $user = $mhs->getUser();
            $this->response['result'][] = array(
              'id'        => $mhs->getId(),
              'id_user'   => $user->getId(),
              'nim'       => $user->getUsername(),
              'nama'      => $user->getNama(),
              'prodi'     => $user->getProdi()->getJenjang() . " " . $user->getProdi()->getNamaProdi(),
              'angkatan'  => $mhs->getAngkatan(),
              'dosen'     => ( null !== $mhs->getPa() ) ? $mhs->getPa()->getUser()->getNama() : ''
            );
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/perkuliahan/dosen_pembimbing_index.html.twig', array(
                'data'      => $this->response
            ));
        }
    }
}
