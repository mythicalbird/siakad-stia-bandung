<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class MataKuliahDosenController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }
  
    /**
     * @Route("/akademika/perkuliahan/mata_kuliah_diampu", name="makul_diampu_index")
     */
    public function indexAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_DOSEN')) {
      	  $dataMakulDiampu = $this->getDoctrine()->getRepository('AppBundle:DosenPengampu')
            ->findByDosen($this->getUser()->getDataDosen());
        } else {
          $dataMakulDiampu = $this->getDoctrine()->getRepository('AppBundle:DosenPengampu')
            ->findAll();
        }
        foreach ($dataMakulDiampu as $data) {
          if ( null !== $data->getMakul() && null !== $data->getMakul()->getMakul() ) {
            $makul = $data->getMakul()->getMakul();
            if ( $makul->getProdi() == $this->getUser()->getProdi() ) {
              $this->response['result'][] = array(
                'id'            => $data->getId(), // id tabel dosen pengampu
                'kode_mk'       => $makul->getKode(),
                'nama_mk'       => $makul->getNama(),
                'kelas'         => ( null !== $data->getMakul()->getKelas() ) ? $data->getMakul()->getKelas()->getNama() : '',
                'dosen'         => ( null !== $data->getDosen() && null !== $data->getDosen()->getUser() ) ? $data->getDosen()->getUser()->getNama() : '',
                'semester'      => $data->getMakul()->getSemester(),
                'sks_teori'     => $makul->getSksTeori(),
                'sks_praktek'   => $makul->getSksPraktek(),
                'sks_lapangan'  => $makul->getSksLapangan(),
              );
            }
          }
        }
        if ( !empty($request->get('json')) && $request->get('json') == "true" ) {
            $response = new JsonResponse();
            $response->setData($this->response);
            return $response;
        } else {
            return $this->appService->load('akademika/perkuliahan/makul_diampu_index.html.twig', array(
                'data'      => $this->response
            ));
        }
    }

}
