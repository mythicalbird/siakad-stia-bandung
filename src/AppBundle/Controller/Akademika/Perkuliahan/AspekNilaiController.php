<?php

namespace AppBundle\Controller\Akademika\Perkuliahan;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Master;
use AppBundle\Entity\TahunAkademik;
use AppBundle\Entity\AspekNilai;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Service\AppService;

class AspekNilaiController extends Controller
{
    protected $appService;
    protected $response = array(
      'error'   => null,
      'result'  => array()
    );

    public function __construct(AppService $appService) {
        $this->appService = $appService;
    }


    /**
     * @Route("/akademika/perkuliahan/aspek_nilai/{aksi}", name="aspek_nilai", defaults={"aksi": "index"})
     */
    public function indexAction(Request $request, $aksi)
    {
        $em = $this->getDoctrine()->getManager();
        if( $aksi == 'edit' ) {
          if( !empty($request->get('id')) ) {
              $data = $this->getDoctrine()->getRepository('AppBundle:AspekNilai')
                ->find($request->get('id')); 
          } else {
              $data = new \AppBundle\Entity\AspekNilai();
          }
          $form = $this->createFormBuilder($data)
              ->add('nama')
              ->add('persen')
              ->add('submit', SubmitType::class, array(
                  'label' => 'Simpan',
                  'attr'  => array(
                      'class' => 'btn btn-primary'
                  )
              ))
              ->getForm();
          $form->handleRequest($request);
          if ($form->isSubmitted() && $form->isValid()) {
              $em->persist($data);
              $em->flush();
              $this->addFlash('success', 'Data berhasil disimpan.');
              return $this->redirectToRoute('aspek_nilai', array('ta' => $data->getTahun()->getId()));
          }
        } 
        else {
          // throw $this->createAccessDeniedException();

          if ($this->get('security.authorization_checker')->isGranted('ROLE_DOSEN')) {
              $dataMakul = $this->getDoctrine()->getRepository('AppBundle:DosenPengampu')
                ->findByDosen($this->getUser()->getDataDosen());
          } else {
              $dataMakul = $this->getDoctrine()->getRepository('AppBundle:Makul')
                ->findAll();    
          }
          $dataTa = $em->createQueryBuilder()
            ->select('t')
            ->from('AppBundle:TahunAkademik', 't')
            ->where('t.status!=:trash')
            ->setParameter('trash', 'trash')
            ->orderBy('t.tahun', 'DESC')
            ->getQuery()
            ->getResult();

          if ( !empty($request->get('ta')) ) {
            $ta = $this->getDoctrine()->getRepository('AppBundle:TahunAkademik')
              ->find($request->get('ta'));
            if ( ! $ta ) {
              $ta = $this->appService->getTahunAkademik(); //tahun akedemik aktif
            }
          } else {
            $ta = $this->appService->getTahunAkademik(); //tahun akedemik aktif
          }

          $this->setDefaultIfEmpty($ta);

          $data = $this->getDoctrine()->getRepository('AppBundle:AspekNilai')
            ->findByTahun($ta); 

          $params = array(
            'data'        => $data,
            'dataMakul'   => $dataMakul,
            'dataTa'      => $dataTa,
            'ta'          => $ta
          );

        }
        if( isset($form) ) 
          $params['form'] = $form->createView();
        return $this->appService->load('akademika/perkuliahan/aspek_nilai_'.$aksi.'.html.twig', $params);
    }


    private function setDefaultIfEmpty($ta)
    {
      $defaults = array(
        array(
          'aspek'   => 'Absensi',
          'persen'  => 10
        ),
        array(
          'aspek'   => 'Penugasan',
          'persen'  => 20
        ),
        array(
          'aspek'   => 'UTS',
          'persen'  => 30
        ),
        array(
          'aspek'   => 'UAS',
          'persen'  => 40
        ),
      );

      $em = $this->getDoctrine()->getManager();

      foreach ($defaults as $default) {
        $aspekNilai = $em->getRepository('AppBundle:AspekNilai')
          ->findOneBy(array(
            'tahun' => $ta,
            'nama'  => $default['aspek']
          ));
        if ( !$aspekNilai ) {
          $aspekNilai = new \AppBundle\Entity\AspekNilai();
          $aspekNilai->setTahun($ta);
          $aspekNilai->setNama($default['aspek']);
          $aspekNilai->setPersen($default['persen']);
          $em->persist($aspekNilai);
          $em->flush();
        }
      }

    }

  }
