<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Setting;
use AppBundle\Entity\Master;
use AppBundle\Entity\Module;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Form\Type\VichImageType;
use AppBundle\Form\Type\BerkasType;
use AppBundle\Service\AppService;

class DefaultController extends Controller
{
    protected $appService;
  
    public function __construct(AppService $appService) {
      $this->appService = $appService;
    }

    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction(Request $request)
    {
        $data = array();
        $em = $this->getDoctrine()->getManager();
        $data['dosen'] = $this->getDoctrine()->getRepository('AppBundle:Dosen')
          ->findAll();
        $data['mahasiswa'] = $this->getDoctrine()->getRepository('AppBundle:Mahasiswa')
          ->findBy(array(
              'maba'       => 0
          )); 
        $role = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findOneBy(['type' => 'hak_akses', 'nama' => 'ADMIN']);
        $data['admin'] = $this->getDoctrine()->getRepository('AppBundle:User')
          ->findByHakAkses($role); 
        $data['prodi'] = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findAll(); 
        return $this->appService->load('default/index.html.twig', array(
          'data' => $data
        ));
    }
  
    /**
     * @Route("/pengaturan", name="pengaturan_index")
     */
    public function pengaturanIndexAction(Request $request)
    {
       $data = $this->getDoctrine()->getRepository(Setting::class)
          ->findOneByName('theme_option');
        if( ! $data ) {
          $data = new Setting();
          $data->setName('theme_option');
        }
        $now = new\DateTime();
        $data->setModifiedAt($now);
        $builder = $this->createFormBuilder($data);
        $form = $builder
          ->add(
              $builder->create('value', FormType::class, array('by_reference' => false))
                ->add('theme_skin', EntityType::class, array(
                    'label' => 'Skin Tema',
                    'class' => Master::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('m')
                            ->where('m.type = :type')
                            ->setParameter('type', 'theme_skin');
                    },
                    'choice_label'  => 'nama',
                    'choice_value' => function (Master $entity = null) {
                        return $entity ? $entity->getId() : '';
                    },
                    'placeholder'   => '-- PILIH --'
                ))
                ->add('logo', null, array(
                    'label' => 'URL Logo'
                ))
                ->add('avatar_max_size', IntegerType::class, array(
                    'label' => 'Ukuran Max. Avatar (Kb)',
                    'data'  => 2000
                ))
          )
          ->add('submit', SubmitType::class, array(
              'label' => 'Simpan',
              'attr'  => array(
                  'class'   => 'btn btn-primary'
              )
          ))
          ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();
            $this->addFlash('success', 'Pengaturan berhasil disimpan.');
        }
        return $this->appService->load('default/pengaturan_index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pengaturan/_prodi/update", name="pengaturan_prodi")
     * @Method({"POST"})
     */
    public function updateProdiAction(AppService $appService, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser()->getId();
        $prodi = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi($request->get('kode'));
        if( $prodi ) {
          $user = $this->getUser();
          $user->setProdi($prodi);
          $em->persist($user);
          $em->flush();
          $appService->updateSetting('__'.$userId.'_current_prodi', $prodi->getKodeProdi()); 
        }
        $retRoute = $request->get('_ret');
        $this->addFlash('success', 'Program studi berhasil dipilih.'); 
        return $this->redirectToRoute($retRoute);
    }

    /**
     * @Route("/pengaturan/backup_restore", name="backup_restore_index")
     */
    public function backupRestoreIndexAction(Request $request)
    {
        $path = $this->get('kernel')->getProjectDir() . '/web';
        $filePath = 'uploads/database.sql';
        $command = 'mysqldump';
        $command .= ' --user=';
        $command .= $this->getParameter('database_user');
        $command .= ' --password=';
        $command .= $this->getParameter('database_password');
        $command .= ' --host=localhost ';
        $command .= $this->getParameter('database_name');
        $command .= ' > ' . $path . '/' . $filePath;
        $exec = exec($command);
        return $this->appService->load('default/backup.html.twig', [
            'filePath' => $filePath 
        ]);
    }

    /**
     * @Route("/pengaturan/modul", name="modul_index")
     */
    public function modulIndexAction(Request $request)
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:Module')
          ->findAll();
        $roles = $this->getDoctrine()->getRepository('AppBundle:Master')
          ->findBy(array(
            'type'    => 'hak_akses',
            'status'  => 'publish'
          ));
        return $this->appService->load('default/modul_index.html.twig', [
            'modules' => $data,
            'roles'         => $roles
        ]);
    }

    /**
     * @Route("/pengaturan/modul/edit", name="modul_edit")
     */
    public function modulEditAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $master = new Module();
        $master->setActive(1);

        $form = $this->createFormBuilder($master)
            ->add('slug', null, array(
                'required'  => false,
                'label'     => 'Kode',
                'attr'      => array(
                      'placeholder' => '(Optional)',
                )
            ))
            ->add('nama', null, array(
                'label' => 'Nama Module',
            ))
            ->add('parent', EntityType::class, array(
                'class' => 'AppBundle:Module',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m');
                },
                'attr'  => array(
                  'class' => 'form-control select2'
                ),
                'choice_label'  => 'nama',
                'placeholder'   => '-- PILIH --'
            ))
            ->add('queryArgs', null, array(
                'label' => 'Query Args',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Simpan',
                'attr'  => array(
                    'class'   => 'btn btn-primary'
                )
            ))
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em->persist($master);
            $em->flush();
            $this->addFlash('success', 'Modul berhasil ditambahkan.');
            return $this->redirectToRoute('modul_edit');
        }
        $masterList = $em->getRepository(Module::class)
            ->findAll();
        $roles = $em->getRepository('AppBundle:Master')->findByType('hak_akses');
        return $this->appService->load('default/module_edit.html.twig', [
            'masterList'    => $masterList,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * @Route("/pengaturan/modul/submit", name="modul_submit")
     * @Method({"POST"})
     */
    public function modulSubmitAction(Request $request)
    {
        $modules = $request->get('modul');
        // echo "<pre>";
        // var_dump($modules);
        // echo "</pre>";exit;
        foreach ($modules as $key => $value) {
          $modul = $this->getDoctrine()->getRepository('AppBundle:Module')
            ->findOneBySlug($key);
          if ($modul) {
            if ( isset($value['roles']) ) {
              $em = $this->getDoctrine()->getManager();
              $modul->setRoles($value['roles']);
              if ( isset($value['aktif']) ) {
                $modul->setActive($value['aktif']);
              }
              $em->persist($modul);
              $em->flush();
            }
          }
        }
        $this->addFlash('success', 'Hak akses berhasil diperbaharui.');
        return $this->redirectToRoute('modul_index');
    }

    /**
     * @Route("/pengaturan/modul/update/{field}", name="modul_update")
     * @Method({"POST"})
     */
    public function updateAction(Request $request, $field = '')
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:Module')->find($request->get('pk'));
        if ( $data && $field != '' ) {
            if ($field == 'slug') {
                $data->setSlug($request->get('value'));
            } elseif ($field == 'nama') {
                $data->setNama($request->get('value'));
            }
            $em->persist($data);
            $em->flush();
            $response->setData(array(
                'id' => $request->get('pk'),
                'success' => 1
            ));
        }
        return $response;
    }

    /**
     * @Route("/404", name="not_found")
     */
    public function notFoundAction(Request $request)
    {
        return $this->appService->load('default/404.html.twig');
    }
}
