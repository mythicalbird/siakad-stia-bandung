<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class NilaiSemesterController extends Controller
{
		
		protected $appService;
    private $response = array(
      'error'   => null,
      'result'  => array()
    );

		public function __construct(AppService $appService) {
			$this->appService = $appService;
		}

    /**
     * @Route("/api/v1/nilai_semester", name="api_nilai_semester_index")
     */
    public function indexAction(Request $request)
    {
		    $response = new JsonResponse();
        if ( empty($request->get('semester')) ) {
          $this->response['error'] = 'Semester harus diisi';
        } 
        elseif ( empty($request->get('kurikulum')) ) {
          $this->response['error'] = 'Kurikulum harus diisi';
        }
        elseif ( empty($request->get('makul')) ) {
          $this->response['error'] = 'Mata kuliah harus diisi';
        }
        elseif ( empty($request->get('kelas')) ) {
          $this->response['error'] = 'Kelas harus diisi';
        }
        else {

          $em = $this->getDoctrine()->getManager();

          $kurikulum = $em->getRepository('AppBundle:Kurikulum')
            ->find( $request->get('kurikulum') );
          if ( !$kurikulum ) {
            $this->response['error'] = 'Kurikulum tidak ditemukan!';
            $response->setData($this->response);
            return $response;
          }

          $makul = $em->getRepository('AppBundle:Makul')
            ->find( $request->get('makul') );
          if ( !$makul ) {
            $this->response['error'] = 'Mata kuliah tidak ditemukan!';
            $response->setData($this->response);
            return $response;
          }

          $kelas = $em->getRepository('AppBundle:Kelas')
            ->find( $request->get('kelas') );
          if ( !$kelas ) {
            $this->response['error'] = 'Kelas tidak ditemukan!';
            $response->setData($this->response);
            return $response;
          }

          $dataAspekNilai = $em->getRepository('AppBundle:AspekNilai')->findAll();

          $dataUser = $em->getRepository('AppBundle:User')
            ->findBy(array(
              'prodi'     => $this->getUser()->getProdi(),
              'hakAkses'  => $this->appService->getMasterTermObject('hak_akses', 4)
            ));

          if ( !empty($request->get('kurikulum')) && !empty($request->get('makul')) && !empty($request->get('kelas')) ) {

            foreach ($dataUser as $user) {

              if ( null !== $user->getDataMahasiswa() ) {

                $mhs = $user->getDataMahasiswa();
                // ambil kelas -> dosen pengampu -> makul
                if ( null !== $mhs->getKelas() && null !== $mhs->getUser() ) {

                  if ( $mhs->getKelas() == $kelas ) {
                    $result = array(
                      'id_mahasiswa'  => $mhs->getId(),
                      'id_user'       => $user->getId(),
                      'npm'           => $user->getUsername(),
                      'jk'            => $user->getJk(),
                      'nilai'          => []
                    );

                    $nilai_ip = 0;

                    foreach ($dataAspekNilai as $aspekNilai) {

                      $nilai = 0;

                      $krs = $em->getRepository('AppBundle:Krs')
                          ->findOneBy(array(
                              'mahasiswa' => $mhs,
                              'makul'     => $makul
                          ));
                      if ( $krs ) {
                        if ( $aspekNilai->getNama() == 'Absensi' ) {

                            $presensi = $em->getRepository('AppBundle:PresensiMahasiswa')
                                ->findBy(array(
                                    // 'tahunAkademik' => $ta,
                                    'semester'  => $mhs->getSemester(),
                                    'kelas'     => $mhs->getKelas(),
                                    'makul'     => $makul,
                                    'user'      => $mhs->getUser(),
                                    'ket'       => 'hadir'
                                ));
                            $presensiCount = count($presensi);
                            $jmlPertemuan = 14;

                            $nilai_aspek = (( ( $presensiCount/$jmlPertemuan )*$aspekNilai->getPersen() )/100)*100;
                            // $np = array(
                            //     $presensiCount,
                            //     round($final_np, 2), // setelah di persen
                            //     0
                            // );
                            $nilai_real = $presensiCount;
                            $nilai += round($nilai_aspek, 2);

                        } else {

                            $np = $em->getRepository('AppBundle:NilaiPerkuliahan')
                                ->findOneBy(array(
                                    'krs'           => $krs,
                                    'aspekNilai'    => $aspekNilai
                                ));

                            if ( !$np ) {
                                $np = new \AppBundle\Entity\NilaiPerkuliahan();
                                $np->setKrs($krs);
                                $np->setAspekNilai($aspekNilai);
                                $np->setNilai(0);
                                $this->em->persist($np);
                                $this->em->flush();
                            }
                            $nilai_aspek = ($np->getNilai()*$aspekNilai->getPersen())/100;

                            // $np = array(
                            //     $nilai->getNilai(),
                            //     round($nilai_final, 2), // setelah di persen
                            //     $nilai->getId()
                            // );

                            $nilai_real = $np->getNilai();
                            $nilai += round($nilai_aspek, 2);

                        }

                        $nilai_ip += $nilai;
                      }

                      $nilai_huruf = $this->getBobotNilai($nilai, true);
                      $nilai_angka = $this->getBobotNilai($nilai);

                      $result['nilai'][] = array(
                        'id_aspek'          => $aspekNilai->getId(),
                        'nama_aspek'        => $aspekNilai->getNama(),
                        'persentase'        => $aspekNilai->getPersen(),
                        'nilai'             => $nilai_real . " (" . $nilai . ")",
                        'huruf'             => $nilai_huruf,
                        'angka'             => $nilai_angka,
                        'lulus'             => 1
                      );

                      // return $np; // id, nilai real, nilai persenstasi

                    }
                    $result['ips'] = array(
                      'huruf' => $this->getBobotNilai($nilai_ip, true),
                      'angka' => $this->getBobotNilai($nilai_ip)
                    );
                    $this->response['result'][] = $result;
                  }
                }

              }

            } 

          }

        }
        $response->setData($this->response);
		    return $response;
    }


    private function getBobotNilai($nilai, $huruf = false, $lulus = false)
    {
        return $this->appService->getBobotNilai($nilai, $huruf, $lulus);
    }

}
