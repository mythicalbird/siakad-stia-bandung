<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Service\AppService;
use AppBundle\Service\FeederService;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FeederKurikulumController extends Controller
{
		
    protected $result = array();
    protected $appService;
    protected $feeder;
    protected $encoder;

    public function __construct(AppService $appService, FeederService $feeder, UserPasswordEncoderInterface $encoder) {
      $this->appService = $appService;
      $this->feeder = $feeder;
      $this->encoder = $encoder;
    }

    /**
     * @Route("/api/v1/feeder/kurikulum", name="api_kurikulum_list")
     */
    public function indexAction(Request $request)
    {
		    $response = new JsonResponse();
        $kode_prodi = 63201;
        $dataLimit = $this->feeder->ws( 'GetCountRecordset', array(
          'table' => 'kurikulum'
        ) );
        $limit = (int) trim($dataLimit['result']);

        $sms = $this->getDoctrine()->getRepository('AppBundle:ProgramStudi')
          ->findOneByKodeProdi($kode_prodi);
        $id_sms = $sms->getUuid();

        $dataKurikulum = $this->feeder->ws( 'GetListKurikulum', array(
          'limit'   => $limit
        ) );

        if ( count($dataKurikulum['result']) > 0 ) {
          
          $data = $dataKurikulum['result'];
          
          for ( $i = 0; $i < count($data); $i++ ) {

            if ( $data[$i]['id_sms'] == $id_sms ) {

              $this->result[] = $sms;
              
            }

          }

        }
        $response->setData($this->result);
		    return $response;
    }

}
