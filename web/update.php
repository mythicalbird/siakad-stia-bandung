<?php

chdir('../');
$output = shell_exec('rm -rf var/cache/* 2>&1');
echo "<pre>$output</pre>";
$output = shell_exec('rm -rf var/logs/* 2>&1');
echo "<pre>$output</pre>";
$output = shell_exec('php bin/console doctrine:schema:update --force');
echo "<pre>$output</pre>";
$output = shell_exec('php bin/console cache:clear');
echo "<pre>$output</pre>";
$output = shell_exec('chown -R www-data:www-data web/uploads');